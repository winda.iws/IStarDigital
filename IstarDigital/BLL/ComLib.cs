﻿using System;
using System.Data;
using System.Globalization;
using System.Text;
using System.IO;
using IstarDigital.Models;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Collections.Generic;
using System.Web.Mvc;

namespace IstarDigital.BLL
{
    public class ComLib
    {
        #region "Public Enum"
        public enum InputType
        {
            Datetime,
            Numeric,
            Text
        }
        public enum TaskStatus
        {
            Handle,
            DirectAssign,
            Pending,
            ReturnTask,
            Reject,
            Complete,
            Expired
        }
        #endregion

        #region "Constants"
        public static System.DateTime NullDateTime = System.DateTime.MinValue;
        public static decimal NullDecimal = decimal.MinValue;
        public static double NullSingle = float.MinValue;
        public static double NullDouble = double.MinValue;
        public static int NullInt = int.MinValue;
        public static long NullLong = long.MinValue;
        public static string NullString = string.Empty;
        public static System.DateTime SqlMaxDate = new System.DateTime(9999, 1, 3, 23, 59, 59);
        public static System.DateTime SqlMinDate = new System.DateTime(1753, 1, 1, 0, 0, 0);
        #endregion
        public static bool NullBool = false;

        #region "Functions"

        public static byte[] Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return base64EncodedBytes;
        }

        public static AttachmentType GetMimeType(string value)
        {
            if (String.IsNullOrEmpty(value))
                return new AttachmentType
                {
                    FriendlyName = "Unknown",
                    MimeType = "application/octet-stream",
                    Extension = ""
                };

            var data = value.Substring(0, 5);

            switch (data.ToUpper())
            {
                case "IVBOR":
                    return new AttachmentType
                    {
                        FriendlyName = "Photo",
                        MimeType = "image/png",
                        Extension = ".png"
                    };

                case "AAAAF":
                    return new AttachmentType
                    {
                        FriendlyName = "Video",
                        MimeType = "video/mp4",
                        Extension = ".mp4"
                    };
                case "JVBER":
                    return new AttachmentType
                    {
                        FriendlyName = "Document",
                        MimeType = "application/pdf",
                        Extension = ".pdf"
                    };
                case "/9J/4":
                    return new AttachmentType
                    {
                        FriendlyName = "Photo",
                        MimeType = "image/jpeg",
                        Extension = ".png"
                    };

                default:
                    return new AttachmentType
                    {
                        FriendlyName = "Unknown",
                        MimeType = string.Empty,
                        Extension = ""
                    };
            }
        }


        public FileResults getFile(FileParam oParam)
        {
            FileResults oResult = null;

            if (!string.IsNullOrEmpty(oParam.Path))
            {
                if (!File.Exists(oParam.Path))
                {
                    oResult = new FileResults();
                    oResult.Result = "Failure";
                    oResult.Message = "Tidak ditemukan File/FilePath";
                }
                else
                {
                    oResult = new FileResults();
                    FileStream fs = new FileStream(oParam.Path,
                                                             FileMode.Open,
                                                             FileAccess.Read);
                    byte[] filebytes = new byte[fs.Length];
                    fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
                    oResult.FileString =
                         Convert.ToBase64String(filebytes,
                                                        Base64FormattingOptions.InsertLineBreaks);
                    //oResult.FileString = filebytes;
                    oResult.Result = "Success";
                    oResult.Message = "";
                }

            }
            else
            {
                oResult = new FileResults();
                oResult.Result = "Failure";
                oResult.Message = "Tidak ditemukan file";

            }

            return oResult;

        }

        public byte[] getFiletoByte(string filename)
        {


            if (!string.IsNullOrEmpty(filename))
            {
                if (!File.Exists(filename))
                {
                    return null;
                }
                else
                {
                    FileStream fs = new FileStream(filename,
                                                             FileMode.Open,
                                                             FileAccess.Read);
                    byte[] filebytes = new byte[fs.Length];
                    fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));

                    return filebytes;

                }

            }
            else
            {
                return null;

            }


        }

        public static string cToString64(byte[] pObj)
        {

            if ((pObj == null))
            {
                return NullString;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return NullString;
            }
            else
            {
                return Convert.ToBase64String(pObj);
            }

        }

        public static string CleanFileName(string sFileName)
        {
            return sFileName.Replace("_", "").Replace("/", "").Replace(":", "").Replace(" ", "").Replace("PM", "").Replace("AM", "");
        }
        public static bool CheckValidation(string TextInput, InputType TextType)
        {
            bool result = true;

            try
            {
                if (TextType == InputType.Datetime)
                {
                    DateTime outputDate;

                    //Check Date
                    if (!DateTime.TryParse(TextInput, out outputDate))
                    {
                        result = false;
                    }
                }
                else if (TextType == InputType.Numeric)
                {
                    double cResult = 0;

                    //Check Numeric
                    if (!Double.TryParse(TextInput, out cResult))
                    {
                        result = false;
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        //Get last day of the month
        public static System.DateTime LastDateofMonth(System.DateTime dateInput)
        {

            return dateInput.AddMonths(1).AddDays((dateInput.AddMonths(1).Day) * -1);

        }
        public static string CleanTextBox(string SourceStr)
        {

            string result = SourceStr.Replace("&nbsp;", "").Replace("&amp;", "");

            return result;
        }

        //Convert blank value to any return value
        public static object Nz(object value, object returnvalue)
        {
            return (IsTextBlank(value) ? returnvalue : value);
        }

        public static bool GetBool(object pObj)
        {
            if ((pObj == null))
            {
                return NullBool;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return NullBool;
            }
            else
            {
                return Convert.ToBoolean(pObj);
            }
        }

        public static double GetDouble(object pObj)
        {
            if ((pObj == null))
            {
                return 0;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(pObj);
            }
        }

        public static float GetSingle(object pObj)
        {
            if ((pObj == null))
            {
                return 0;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return 0;
            }
            else
            {
                return float.Parse(pObj.ToString());
            }
        }

        public static int GetInteger(object pObj)
        {
            if ((pObj == null))
            {
                return 0;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return 0;
            }
            else if (string.IsNullOrEmpty(pObj.ToString()))
            {
                return 0;
            }
            else
            {
                return int.Parse(pObj.ToString());
            }
        }

        public static string GetText(object pObj)
        {
            if ((pObj == null))
            {
                return NullString;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return NullString;
            }
            else if (pObj.ToString() == "&nbsp;")
            {
                return "";
            }
            else
            {
                return pObj.ToString();
            }
        }

        public static DateTime GetDate(object pObj)
        {
            if ((pObj == null))
            {
                return DateTime.MinValue;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return DateTime.MinValue;
            }
            else if ((pObj.ToString() == ""))
            {
                return DateTime.MinValue;
            }
            else if (pObj.ToString() == "&nbsp;")
            {
                return DateTime.MinValue;
            }
            else
            {
                return Convert.ToDateTime(pObj);
            }
        }

        public static System.DateTime CToDate(object pObj, bool pShort = false)
        {
            if ((pObj == null))
            {
                return SqlMinDate;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return SqlMinDate;
            }
            else if (IsTextBlank(pObj))
            {
                return SqlMinDate;
            }
            else
            {
                if (pShort)
                {
                    return Convert.ToDateTime(Convert.ToDateTime(pObj).ToShortDateString());
                }
                else
                {
                    return Convert.ToDateTime(pObj);
                }
            }
        }

        public static object MinDateToNull(object pobj)
        {
            if (DateTime.ParseExact(pobj.ToString(), "ddd MMM dd yyyy HH:mm:ss", CultureInfo.InvariantCulture) == DateTime.MinValue)//pobj == SqlMinDate
            {
                return DBNull.Value;
            }
            else if (pobj.ToString() == "&nbsp;")
            {
                return DBNull.Value;
            }
            else
            {
                return pobj;
            }
        }

        public static object BlankDateToNull(System.DateTime psource)
        {
            if (psource == DateTime.MinValue)
            {
                return null;
            }
            else
            {
                return psource;
            }
        }

        public static object BlankDateToMinValue(System.DateTime psource)
        {
            if (psource == DateTime.MinValue)
            {
                return SqlMinDate;
            }
            else
            {
                return psource;
            }
        }

        public static bool IsTextBlank(object pobj)
        {
            if ((pobj == null))
            {
                return true;
            }
            else
            {
                return Convert.IsDBNull(pobj) | string.IsNullOrEmpty(pobj.ToString());
            }
        }

        public static bool IsNumericBlank(object pobj)
        {
            if (IsTextBlank(pobj))
            {
                return true;
            }
            else if (Convert.ToInt16(pobj) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string CBlank(string pobj)
        {
            if (IsTextBlank(pobj))
            {
                return "";
            }
            else
            {
                return pobj;
            }
        }

        public static double CNumeric(string pobj)
        {
            if (IsTextBlank(pobj))
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(pobj);
            }
        }

        public static byte[] Cbytefrom64(string pobj)
        {
            if (IsTextBlank(pobj))
            {
                return null;
            }
            else
            {
                return Convert.FromBase64String(pobj);
            }
        }

        public static byte[] Getbyte(object pobj)
        {
            if (IsTextBlank(pobj))
            {
                return null;
            }
            else
            {
                return (byte[])pobj;
            }
        }

        public static System.DateTime SystemDate()
        {
            try
            {
                return System.DateTime.Today;
            }
            catch (Exception ex)
            {
                return SqlMinDate;
            }
        }

        public static SqlDbType GetSQLDataType(string pdatatype)
        {
            try
            {
                switch (pdatatype)
                {
                    case "I":
                        return SqlDbType.Int;
                    case "S":
                        return SqlDbType.VarChar;
                    case "D":
                        return SqlDbType.DateTime;
                    default:
                        return SqlDbType.VarChar;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static object GetSystemDataType(string pdatatype)
        {
            try
            {
                switch (pdatatype)
                {
                    case "I":
                        return typeof(int);
                    case "S":
                        return typeof(string);
                    case "D":
                        return typeof(System.DateTime);
                    default:
                        return typeof(string);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static object CastValue(string ptype, string pvalue)
        {
            try
            {
                switch (ptype)
                {
                    case "I":
                        return Convert.ToInt32(pvalue);
                    case "S":
                        return pvalue;
                    case "D":
                        return Convert.ToDateTime(pvalue);
                    default:
                        return pvalue;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static DateTime GetFirstDayOfMonth(DateTime dtDate)
        {
            DateTime dtFrom = dtDate;
            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));
            return dtFrom;
        }

        public static DateTime GetLastDayOfMonth(DateTime dtDate)
        {
            DateTime dtTo = new DateTime(dtDate.Year, dtDate.Month, 1);
            dtTo = dtTo.AddMonths(1);
            dtTo = dtTo.AddDays(-(dtTo.Day));
            return dtTo;
        }

        public static string CheckInputValidation(string inputName, string TextInput, InputType TextType, bool AllowNull = true)
        {
            string result = "";
            if (TextType == InputType.Datetime)
            {
                DateTime outputDate;

                //Check Date
                if (!DateTime.TryParse(TextInput, out outputDate))
                {
                    result = "*Please enter a valid Date value for " + inputName;
                }
            }
            else if (TextType == InputType.Numeric)
            {

                double cResult = 0;

                //Check Numeric
                if (!Double.TryParse(TextInput, out cResult))
                {
                    result = "*Please enter a valid numeric value for " + inputName;
                }
            }
            else
            {
            }
            return result;
        }

        public MemoryStream MergePdfForms(List<byte[]> files)
        {
            if (files.Count > 1)
            {
                PdfReader pdfFile;
                Document doc;
                PdfWriter pCopy;
                MemoryStream msOutput = new MemoryStream();

                pdfFile = new PdfReader(files[0]);

                doc = new Document();
                pCopy = new PdfSmartCopy(doc, msOutput);

                doc.Open();

                for (int k = 0; k < files.Count; k++)
                {
                    pdfFile = new PdfReader(files[k]);
                    for (int i = 1; i < pdfFile.NumberOfPages + 1; i++)
                    {
                        ((PdfSmartCopy)pCopy).AddPage(pCopy.GetImportedPage(pdfFile, i));
                    }
                    pCopy.FreeReader(pdfFile);
                }


                pdfFile.Close();
                pCopy.Close();
                doc.Close();

                return msOutput;
            }
            else if (files.Count == 1)
            {
                return new MemoryStream(files[0]);
            }

            return null;
        }

        //public FileResult PdfForms(byte[] files)
        //{
        //    //if (files.Count > 1)
        //    //{
        //    PdfReader pdfFile;
        //    Document doc;
        //    PdfWriter pCopy;
        //    //MemoryStream msOutput = new MemoryStream();

        //    pdfFile = new PdfReader(files);

        //    doc = new Document();
        //    //pCopy = new PdfSmartCopy(doc, msOutput);

        //    doc.Open();

        //    //for (int k = 0; k < files.Count; k++)
        //    //{
        //    pdfFile = new PdfReader(files);
        //    //for (int i = 1; i < pdfFile.NumberOfPages + 1; i++)
        //    //{
        //    //    ((PdfSmartCopy)pCopy).AddPage(pCopy.GetImportedPage(pdfFile, i));
        //    //}
        //    //pCopy.FreeReader(pdfFile);
        //    //}

        //    pdfFile.Close();
        //    //pCopy.Close();
        //    doc.Close();

        //    return File(files, "application/pdf");
        //    //}
        //    //else if (files.Count == 1)
        //    //{
        //    //    return new MemoryStream(files[0]);
        //    //}

        //    //return null;
        //}

        //public static cAnyParams ConvertStringToAnyParams(string pString)
        //{
        //    string[] arrfield = null;
        //    cAnyParams oany = new cAnyParams();
        //    try
        //    {
        //        arrfield = Strings.Split(pString, "|");
        //        for (i = 0; i <= Information.UBound(arrfield); i += 2)
        //        {
        //            if (arrfield[i] == "CompanyID")
        //            {
        //                oany.CompanyID = ComLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "PageNumber")
        //            {
        //                oany.PageNumber = ComLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "PageSize")
        //            {
        //                oany.PageSize = ComLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey1")
        //            {
        //                oany.skey1 = ComLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey2")
        //            {
        //                oany.skey2 = ComLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey3")
        //            {
        //                oany.skey3 = ComLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey4")
        //            {
        //                oany.skey4 = ComLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey5")
        //            {
        //                oany.skey5 = ComLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey6")
        //            {
        //                oany.skey6 = ComLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey7")
        //            {
        //                oany.skey7 = ComLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey8")
        //            {
        //                oany.skey8 = ComLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey9")
        //            {
        //                oany.skey9 = ComLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey10")
        //            {
        //                oany.skey10 = ComLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "ikey1")
        //            {
        //                oany.ikey1 = ComLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "ikey2")
        //            {
        //                oany.ikey2 = ComLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "ikey3")
        //            {
        //                oany.ikey3 = ComLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "ikey4")
        //            {
        //                oany.ikey4 = ComLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "ikey5")
        //            {
        //                oany.ikey5 = ComLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "fkey1")
        //            {
        //                oany.fkey1 = ComLib.GetDouble(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "fkey2")
        //            {
        //                oany.fkey2 = ComLib.GetDouble(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "fkey3")
        //            {
        //                oany.fkey3 = ComLib.GetDouble(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "fkey4")
        //            {
        //                oany.fkey4 = ComLib.GetDouble(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "fkey5")
        //            {
        //                oany.fkey5 = ComLib.GetDouble(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "bkey1")
        //            {
        //                oany.bkey1 = ComLib.GetBool(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "bkey2")
        //            {
        //                oany.bkey2 = ComLib.GetBool(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "bkey3")
        //            {
        //                oany.bkey3 = ComLib.GetBool(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "bkey4")
        //            {
        //                oany.bkey4 = ComLib.GetBool(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "bkey5")
        //            {
        //                oany.bkey5 = ComLib.GetBool(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "dkey1")
        //            {
        //                oany.dkey1 = ComLib.CToDate(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "dkey2")
        //            {
        //                oany.dkey2 = ComLib.CToDate(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "dkey3")
        //            {
        //                oany.dkey3 = ComLib.CToDate(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "dkey4")
        //            {
        //                oany.dkey4 = ComLib.CToDate(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "dkey5")
        //            {
        //                oany.dkey5 = ComLib.CToDate(arrfield[i + 1]);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return oany;
        //}

        public static bool IsTextBlankV(object pobj)
        {
            if ((pobj == null))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //#Region "JSON Conversion"
        //    Public Shared Function DataTableToJSON(dt As DataTable) As String
        //        Dim serializer As New JavaScriptSerializer()
        //        Dim rows As New List(Of Dictionary(Of String, Object))()
        //        Dim row As Dictionary(Of String, Object) = Nothing

        //        For Each dr As DataRow In dt.Rows
        //            row = New Dictionary(Of String, Object)()
        //            For Each col As DataColumn In dt.Columns
        //                row.Add(col.ColumnName, dr(col))
        //            Next
        //            rows.Add(row)
        //        Next
        //        Return serializer.Serialize(rows)
        //    End Function
        //    Public Shared Function DataSetToJSON(ds As DataSet) As String
        //        Dim dict As New Dictionary(Of String, Object)()

        //        For Each dt As DataTable In ds.Tables
        //            Dim arr As Object() = New Object(dt.Rows.Count) {}

        //            For i As Integer = 0 To dt.Rows.Count - 1
        //                arr(i) = dt.Rows(i).ItemArray
        //            Next

        //            dict.Add(dt.TableName, arr)
        //        Next

        //        Dim json As New JavaScriptSerializer()
        //        Return json.Serialize(dict)
        //    End Function
        //#End Region
        //#endregion

        public string getComString(string stype)
        {
            string m_file_name = null;
            StreamReader m_stream_reader = default(StreamReader);
            string getStr = "";
            string[] arrstring = null;
            bool bout = false;
            m_file_name = "comstring.ini";
            try
            {
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + m_file_name))
                {
                    m_stream_reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + m_file_name);
                    while (!bout)
                    {
                        getStr = m_stream_reader.ReadLine();
                        arrstring = getStr.Split('|');
                        if (arrstring[0].ToLower() == stype.ToLower())
                        {
                            getStr = arrstring[1];
                            bout = true;
                        }
                        else if (getStr == null)
                        {
                            bout = true;
                        }

                    }
                    m_stream_reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                m_stream_reader = null;
            }
            return getStr;
        }

        public string getSurveyTimeString(string stype)
        {
            string m_file_name = null;
            StreamReader m_stream_reader = default(StreamReader);
            string getStr = "";
            string[] arrstring = null;
            bool bout = false;
            m_file_name = "surveytime.ini";
            try
            {
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + m_file_name))
                {
                    m_stream_reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + m_file_name);
                    while (!bout)
                    {
                        getStr = m_stream_reader.ReadLine();
                        arrstring = getStr.Split('|');
                        if (arrstring[0].ToLower() == stype.ToLower())
                        {
                            getStr = arrstring[1];
                            bout = true;
                        }
                        else if (getStr == null)
                        {
                            bout = true;
                        }

                    }
                    m_stream_reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                m_stream_reader = null;
            }
            return getStr;
        }

        #endregion
    }
}