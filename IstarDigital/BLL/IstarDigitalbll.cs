﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IstarDigital.Models;
using IstarDigital.DAL;
using System.Data;
using IstarDigital.Helpers;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;
using IstarDigital.Models.Policy;
using static IstarDigital.Helpers.helperLib;
using System.Data.SqlClient;
using IstarDigital.Models.Customer;
using IstarDigital.Models.Payment;
using IstarDigital.Models.Report;
using IstarDigital.Models.Billing;
using IstarDigital.Models.Claim;

namespace IstarDigital.BLL
{
    public class IstarDigitalbll
    {

        helperCom objhelperCom = new helperCom();

        public loginResult IsValidAD(string pCompanyID, string username, string pass)
        {
            //DirectoryEntry theEntry = null/* TODO Change to default(_) if this is not a reference type */;
            string sresult = "";
            // Dim sdomainname As String = GetSingleValue(pCompanyID, "DomainName")

            SQLAccess odal = new SQLAccess("SPORTAL");
            IDataReader rdr;
            IDataReader rdr2;
            IDataReader rdr3;

            loginResult oTempResult = null/* TODO Change to default(_) if this is not a reference type */;
            string tempVal = "";
            bool hasDataFlag = false;

            try
            {
                //rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure, odal.CreateParameter("@companyid", SqlDbType.VarChar, comlib.GetText(pCompanyID)), odal.CreateParameter("@table", SqlDbType.VarChar, "key_login"), odal.CreateParameter("@userid", SqlDbType.VarChar, comlib.GetText(username)));
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                 odal.CreateParameter("@table", SqlDbType.VarChar, "key_login") as IDbDataParameter,
                 odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                 odal.CreateParameter("@skey1", SqlDbType.VarChar, username) as IDbDataParameter,
                 odal.CreateParameter("@skey2", SqlDbType.VarChar, pass) as IDbDataParameter);

                while (rdr.Read())
                {
                    oTempResult = new loginResult();
                    oTempResult.Result = "";
                    oTempResult.UserID = ComLib.GetText(rdr["UserID"]);
                    oTempResult.UserName = ComLib.GetText(rdr["UserName"]);
                    oTempResult.DeptID = ComLib.GetText(rdr["DepartmentID"]);
                    oTempResult.DomainName = ComLib.GetText(rdr["DomainName"]);
                    oTempResult.Roles = ComLib.GetText(rdr["RoleID"]);

                    tempVal = "";

                    //rdr2 = odal.ExecuteDataReader("usp_check_roles", CommandType.StoredProcedure, odal.CreateParameter("@companyid", SqlDbType.VarChar, ComLib.GetText(pCompanyID)), odal.CreateParameter("@userid", SqlDbType.VarChar, comlib.GetText(username)));
                    rdr2 = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                 odal.CreateParameter("@table", SqlDbType.VarChar, "key_user_role") as IDbDataParameter,
                 odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                 odal.CreateParameter("@skey1", SqlDbType.VarChar, username) as IDbDataParameter);

                    while (rdr2.Read())
                    {

                        tempVal = tempVal + "'" + ComLib.GetText(rdr2["RoleID"]) + "',";
                    }
                    oTempResult.RolesList = tempVal;

                    hasDataFlag = true;
                }

                //Validate Company User
                rdr3 = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                 odal.CreateParameter("@table", SqlDbType.VarChar, "company_by_user") as IDbDataParameter,
                 odal.CreateParameter("@skey1", SqlDbType.VarChar, username) as IDbDataParameter);

                string company;
                while (rdr3.Read())
                {

                    company = ComLib.GetText(rdr3["CompanyID"]);

                    if (company == pCompanyID)
                    {
                        hasDataFlag = true;
                    }
                    else
                    {
                        hasDataFlag = false;
                    }
                }

                if (!hasDataFlag)
                {
                    oTempResult = new loginResult();
                    oTempResult.Result = "No Data";
                    return oTempResult;
                }
            }


            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                rdr2 = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }

            // Try
            // theEntry = New DirectoryEntry("LDAP://" + oTempResult.DomainName, username, pass, AuthenticationTypes.Secure)
            // Dim theObject As Object
            // theObject = theEntry.NativeObject
            // Catch ex As Exception
            // oTempResult.Result = ex.Message()
            // Finally
            // theEntry.Close()
            // theEntry.Dispose()
            // End Try

            return oTempResult;
        }

        public UserLogin GetLoginValue(string UserID, string Userpassword)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            UserLogin oparamLog = null;
            try
            {

                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                    odal.CreateParameter("@table", SqlDbType.VarChar, "get_login_client_user") as IDbDataParameter,
                    odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                    odal.CreateParameter("@skey1", SqlDbType.VarChar, UserID) as IDbDataParameter,
                    odal.CreateParameter("@skey2", SqlDbType.VarChar, Userpassword) as IDbDataParameter);

                oparamLog = new UserLogin();

                if (rdr.Read())
                {
                    oparamLog.UserID = rdr["UserID"].ToString();
                    oparamLog.UserPassword = rdr["UserPassword"].ToString();
                    oparamLog.userType = rdr["UserType"].ToString();
                    oparamLog.UserStatus = rdr["UserStatus"].ToString();
                    oparamLog.KeyUpdate = rdr["LastKey"].ToString();
                    oparamLog.CodeStatus = rdr["CodeStatus"].ToString();
                    oparamLog.CodeMessage = rdr["CodeMessage"].ToString();
                }
                else
                {
                    oparamLog = null;
                }
            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("GetLoginValue", "StoredProcedure", "usp_get_records", "get_login_client_user", ex.Message.ToString());

                throw ex;

            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }
            return oparamLog;
        }

        public string GetLastKeyPwd(string UserID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            string result = null;
            try
            {

                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                    odal.CreateParameter("@table", SqlDbType.VarChar, "get_lastKeyUpdate_client_user") as IDbDataParameter,
                    odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                    odal.CreateParameter("@skey1", SqlDbType.VarChar, UserID) as IDbDataParameter);

                if (rdr.Read())
                {
                    result = rdr["LastKey"].ToString();
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("GetLastKeyPwd", "StoredProcedure", "usp_get_records", "get_lastKeyUpdate_client_user", ex.Message.ToString());
                throw ex;
            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }

            return result;
        }



        public HomePage GetHomePage(HomePageParam oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            HomePage ohome = null;
            BannerImage oimg = null;
            int rowno = 0;
            try
            {

                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                    odal.CreateParameter("@table", SqlDbType.VarChar, "client_get_homepage") as IDbDataParameter,
                    odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                    odal.CreateParameter("@nkey1", SqlDbType.Int, oParam.Height) as IDbDataParameter,
                    odal.CreateParameter("@nkey2", SqlDbType.Int, oParam.Width) as IDbDataParameter,
                    odal.CreateParameter("@dkey1", SqlDbType.DateTime, oParam.LastUpdatedBannerImage) as IDbDataParameter);

                ohome = new HomePage();
                while (rdr.Read())
                {
                    if (rowno == 0)
                    {
                        ohome.LastUpdatedBannerImage = helperLib.GetDate(rdr["LastUpdatedBannerImage"]);
                        ohome.BannerImages = new List<BannerImage>();
                        rowno = +1;
                    }

                    oimg = new BannerImage();
                    oimg.ImageSequence = (int)rdr["ImageSequence"];
                    oimg.ImageDetail = helperLib.Getbyte(rdr["ImageDetail"]);
                    oimg.ImageURL = rdr["ImageURL"].ToString();

                    if (oimg.ImageDetail != null)
                    {
                        ohome.BannerImages.Add(oimg);
                    }
                }

                ohome.Result = "SUCCESS";
                ohome.Message = "";

                if (ohome == null)
                {
                    ohome = new HomePage();
                    ohome.Result = "FAILURE";
                    ohome.Message = "Tidak ada data";
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("GetHomePage", "StoredProcedure", "usp_get_records", "client_get_homepage", ex.Message.ToString());
            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }
            return ohome;
        }

        public HomePage_Path GetHomePage_Path(HomePageParam oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            HomePage_Path ohome = null;
            BannerImage_Path oimg = null;
            int rowno = 0;
            try
            {

                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                    odal.CreateParameter("@table", SqlDbType.VarChar, "client_get_homepage_path") as IDbDataParameter,
                    odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                    odal.CreateParameter("@nkey1", SqlDbType.Int, oParam.Height) as IDbDataParameter,
                    odal.CreateParameter("@nkey2", SqlDbType.Int, oParam.Width) as IDbDataParameter,
                    odal.CreateParameter("@dkey1", SqlDbType.DateTime, oParam.LastUpdatedBannerImage) as IDbDataParameter);

                ohome = new HomePage_Path();
                while (rdr.Read())
                {
                    if (rowno == 0)
                    {
                        ohome.LastUpdatedBannerImage = helperLib.GetDate(rdr["LastUpdatedBannerImage"]);
                        ohome.BannerImages = new List<BannerImage_Path>();
                        rowno = +1;
                    }

                    oimg = new BannerImage_Path();
                    oimg.ImageSequence = (int)rdr["ImageSequence"];
                    oimg.ImageDetail = rdr["ImageLink"].ToString();
                    oimg.ImageURL = rdr["ImageURL"].ToString();

                    if (oimg.ImageDetail != null)
                    {
                        ohome.BannerImages.Add(oimg);
                    }
                }

                ohome.Result = "SUCCESS";
                ohome.Message = "";

                if (ohome == null)
                {
                    ohome = new HomePage_Path();
                    ohome.Result = "FAILURE";
                    ohome.Message = "Tidak ada data";
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("GetHomePage_Path", "StoredProcedure", "usp_get_records", "client_get_homepage_path", ex.Message.ToString());
            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }
            return ohome;
        }

        /*
           updated : RS
           date : 31 MAY 2018
           Reason : ADD Date value Token check for Old Token 
       */

        public DefaultReturn RegisterUser(RegisterParam oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oret = null;
            helperSec objhelperSec = new helperSec();
            try
            {
                oret = new DefaultReturn();

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "02") as IDbDataParameter,
                odal.CreateParameter("@UserID", SqlDbType.VarChar, oParam.UserName) as IDbDataParameter,
                odal.CreateParameter("@POL_ID", SqlDbType.VarChar, oParam.PolicyID) as IDbDataParameter,
                odal.CreateParameter("@BirthDate", SqlDbType.DateTime, helperLib.GetDateFormat(oParam.BirthDate, "yyyyMMdd")) as IDbDataParameter,
                odal.CreateParameter("@NewPassword", SqlDbType.VarChar, oParam.UserPassword) as IDbDataParameter,
                odal.CreateParameter("@ActivationType", SqlDbType.VarChar, oParam.ActivationType) as IDbDataParameter,
                odal.CreateParameter("@ActivationInfo", SqlDbType.VarChar, oParam.ActivationInfo) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oret.Result, 100, ParameterDirection.Output) as IDbDataParameter,
                odal.CreateParameter("@result2", SqlDbType.VarChar, oret.OtherValue, 100, ParameterDirection.Output) as IDbDataParameter);

                odal.CommitTransactions();

                if (odal.get_ReturnValue(1) == "RG_CLIENT_01")
                {
                    oret.Result = "FAILURE";
                    //oret.Message = "Data Polis dan Tanggal Lahir anda Salah";
                    oret.Message = "Nomor polis, tanggal lahir dan alamat email/nomor ponsel yang Anda masukkan tidak sesuai.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_02")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID telah terdaftar. Gunakan User ID lainnya.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_03")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "Nomor polis, tanggal lahir dan alamat email/nomor ponsel yang Anda masukkan tidak sesuai.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_04")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "Anda sudah melakukan registrasi. Silakan gunakan fasilitas Lupa Kata Sandi untuk mengetahui informasi login anda.";
                }
                else
                {
                    ResendActivationCode(oParam.UserName);
                    oret.OtherValue = odal.get_ReturnValue(2);
                    oret.Result = "SUCCESS";
                    oret.Message = "";
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("RegisterUser", "StoredProcedure", "usp_user_management_client", "", ex.Message.ToString());
                odal.RollBackTransactions();
                oret.Result = "FAILURE";
                oret.Message = "Input Parameter tidak valid atau ada kesalahan System";
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }
            return oret;
        }

        public DefaultReturn RegisterUserPhase1(RegisterParamV1 oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oret = null;
            helperSec objhelperSec = new helperSec();
            try
            {
                oret = new DefaultReturn();

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "08") as IDbDataParameter,
                odal.CreateParameter("@POL_ID", SqlDbType.VarChar, oParam.PolicyID) as IDbDataParameter,
                odal.CreateParameter("@BirthDate", SqlDbType.DateTime, helperLib.GetDateFormat(oParam.BirthDate, "yyyyMMdd")) as IDbDataParameter,
                odal.CreateParameter("@ActivationType", SqlDbType.VarChar, oParam.ActivationType) as IDbDataParameter,
                odal.CreateParameter("@ActivationInfo", SqlDbType.VarChar, oParam.ActivationInfo) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oret.Result, 100, ParameterDirection.Output) as IDbDataParameter);

                odal.CommitTransactions();

                if (odal.get_ReturnValue(1) == "RG_CLIENT_01")
                {
                    oret.Result = "FAILURE";
                    //oret.Message = "Data Polis dan Tanggal Lahir anda Salah";
                    oret.Message = "Data yang Anda masukkan belum sesuai. Hubungi Pusat Layanan Nasabah di 1500786 untuk bantuan.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_03")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "Data yang Anda masukkan belum sesuai. Hubungi Pusat Layanan Nasabah di 1500786 untuk bantuan.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_04")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID Anda sudah terdaftar. Silahkan melakukan Login akun Anda.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_05")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID Anda sudah terdaftar. Silahkan melanjutkan aktivasi akun pada halaman Login.";
                }
                else
                {
                    oret.Result = "SUCCESS";
                    oret.Message = "";
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("RegisterUserPhase1", "StoredProcedure", "usp_user_management_client", "", ex.Message.ToString());
                oret.Result = "FAILURE";
                oret.Message = "Input Parameter tidak valid atau ada kesalahan System";
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }
            return oret;
        }

        /*
           updated : RS
           date : 31 MAY 2018
           Reason : ADD Date value Token check for Old Token 
       */

        public DefaultReturn RegisterUserPhase2(RegisterParam oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oret = null;
            helperSec objhelperSec = new helperSec();
            try
            {
                oret = new DefaultReturn();

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "09") as IDbDataParameter,
                odal.CreateParameter("@UserID", SqlDbType.VarChar, oParam.UserName) as IDbDataParameter,
                odal.CreateParameter("@POL_ID", SqlDbType.VarChar, oParam.PolicyID) as IDbDataParameter,
                odal.CreateParameter("@BirthDate", SqlDbType.DateTime, helperLib.GetDateFormat(oParam.BirthDate, "yyyyMMdd")) as IDbDataParameter,
                odal.CreateParameter("@NewPassword", SqlDbType.VarChar, oParam.UserPassword) as IDbDataParameter,
                odal.CreateParameter("@ActivationType", SqlDbType.VarChar, oParam.ActivationType) as IDbDataParameter,
                odal.CreateParameter("@ActivationInfo", SqlDbType.VarChar, oParam.ActivationInfo) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oret.Result, 100, ParameterDirection.Output) as IDbDataParameter,
                odal.CreateParameter("@result2", SqlDbType.VarChar, oret.OtherValue, 100, ParameterDirection.Output) as IDbDataParameter);

                odal.CommitTransactions();

                if (odal.get_ReturnValue(1) == "RG_CLIENT_01")
                {
                    oret.Result = "FAILURE";
                    //oret.Message = "Data Polis dan Tanggal Lahir anda Salah";
                    oret.Message = "Nomor polis, tanggal lahir dan alamat email/nomor ponsel yang Anda masukkan tidak sesuai.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_02")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID telah terdaftar. Gunakan User ID lainnya.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_03")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "Nomor polis, tanggal lahir dan alamat email/nomor ponsel yang Anda masukkan tidak sesuai.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_04")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID Anda sudah terdaftar. Silahkan melakukan Login akun Anda.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_05")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID Anda sudah terdaftar. Silahkan melanjutkan aktivasi akun pada halaman Login.";
                }
                else
                {
                    ResendActivationCode(oParam.UserName);
                    oret.OtherValue = odal.get_ReturnValue(2);
                    oret.Result = "SUCCESS";
                    oret.Message = "User ID Anda telah terdaftar dan kode aktivasi akan dikirimkan ke nomor ponsel/email Anda. Silakan lanjutkan aktivasi.";
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("RegisterUserPhase2", "StoredProcedure", "usp_user_management_client", "", ex.Message.ToString());
                odal.RollBackTransactions();
                oret.Result = "FAILURE";
                oret.Message = "Input Parameter tidak valid atau ada kesalahan System";
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }
            return oret;
        }

        public DefaultReturn ValidateUserName(String UserName)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            DefaultReturn oret = null;

            try
            {

                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                    odal.CreateParameter("@table", SqlDbType.VarChar, "client_validate_username") as IDbDataParameter,
                    odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                    odal.CreateParameter("@skey1", SqlDbType.VarChar, UserName) as IDbDataParameter);

                oret = new DefaultReturn();

                if (rdr.Read())
                {
                    oret.Message = "User Name sudah digunakan";
                    oret.Result = "FAILURE";
                }
                else
                {
                    oret.Message = "";
                    oret.Result = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("ValidateUserName", "StoredProcedure", "usp_get_records", "client_validate_username", ex.Message.ToString());
                throw ex;
            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }

            return oret;
        }

        public DefaultReturn ValidateActivationCode(ActivationParam oparam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oret = null;
            oret = new DefaultReturn();
            try
            {

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "06") as IDbDataParameter,
                odal.CreateParameter("@UserID", SqlDbType.VarChar, oparam.UserName) as IDbDataParameter,
                odal.CreateParameter("@skey4", SqlDbType.VarChar, oparam.ActivationCode) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oret.Result, 100, ParameterDirection.Output) as IDbDataParameter);

                if (odal.get_ReturnValue(1) == "AC_CLIENT_01")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "Kode aktivasi Anda tidak sesuai.";
                }
                else
                {
                    oret.Result = "SUCCESS";
                    oret.Message = "Aktivasi akun berhasil.";
                }

                odal.CommitTransactions();

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("ValidateActivationCode", "StoredProcedure", "usp_user_management_client", "", ex.Message.ToString());
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }

            return oret;
        }

        public DefaultReturn changeUserImage(changeUserImage oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oResult = null;
            try
            {
                oResult = new DefaultReturn();

                odal.BeginTransactions();

                odal.ExecuteCommandText("update tuser set UserPicture=@UserPicture,UserPictureDate=@UserPictureDate where UserID=@UserID and UserType=@UserType", true,
                    odal.CreateParameter("@UserPicture", SqlDbType.VarBinary, helperLib.Cbytefrom64(oParam.UserImage)) as IDbDataParameter,
                    odal.CreateParameter("@UserID", SqlDbType.VarChar, oParam.UserName) as IDbDataParameter,
                    odal.CreateParameter("@UserPictureDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                    odal.CreateParameter("@UserType", SqlDbType.VarChar, "C") as IDbDataParameter);

                odal.CommitTransactions();


                oResult.Message = "";
                oResult.Result = "SUCCESS";

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("changeUserImage", "CommandText", "update tuser set UserPicture", "", ex.Message.ToString());
                oResult.Message = "Input Parameter tidak valid atau ada kesalahan System";
                oResult.Result = "FAILURE";
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }
            return oResult;
        }

        public DefaultReturn ResendActivationCode(String UserName)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            ActivationParam oActv = null;
            DefaultReturn oret = null;
            helperCom objhelperCom = new helperCom();
            helperSec objhelperSec = new helperSec();
            oActv = new ActivationParam();
            oret = new DefaultReturn();
            try
            {

                String aktivasiCode = objhelperCom.RandomStringNumber(6);

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "05") as IDbDataParameter,
                odal.CreateParameter("@UserID", SqlDbType.VarChar, UserName) as IDbDataParameter,
                odal.CreateParameter("@skey4", SqlDbType.VarChar, objhelperSec.GetMD5(aktivasiCode + "SunClient2017")) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oActv.ActivationType, 100, ParameterDirection.Output) as IDbDataParameter,
                odal.CreateParameter("@result2", SqlDbType.VarChar, oActv.ActivationInfo, 100, ParameterDirection.Output) as IDbDataParameter);

                oActv.ActivationType = odal.get_ReturnValue(1);
                oActv.ActivationInfo = odal.get_ReturnValue(2);

                odal.CommitTransactions();

                if (oActv.ActivationType.ToUpper() == "E")
                {
                    emailParam oEmail = null;
                    odal = new SQLAccess("PORTAL");
                    rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                       odal.CreateParameter("@table", SqlDbType.VarChar, "get_SMTP_setting_ClientAktivasi") as IDbDataParameter,
                       odal.CreateParameter("@skey1", SqlDbType.VarChar, "SMTP_ACT_CODE") as IDbDataParameter,
                       odal.CreateParameter("@skey2", SqlDbType.VarChar, UserName) as IDbDataParameter,
                       odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                       odal.CreateParameter("@skey10", SqlDbType.VarChar, aktivasiCode) as IDbDataParameter);

                    while (rdr.Read())
                    {

                        oEmail = new emailParam();
                        oEmail.fromMail = rdr["fromMail"].ToString();
                        oEmail.sendemail = oActv.ActivationInfo;
                        oEmail.subjectMsg = rdr["subjectMsg"].ToString();
                        oEmail.bodyMsg = rdr["bodyMSG"].ToString();
                        oEmail.SMTPServer = rdr["SMTPServer"].ToString();
                        oEmail.PortServer = (int)rdr["PortServer"];
                        oEmail.SMTPId = rdr["SMTPId"].ToString();
                        oEmail.SMTPPassword = rdr["SMTPPassword"].ToString();
                        oEmail.SMTPSSL = bool.Parse(rdr["SMTPSSL"].ToString());
                        oEmail.SMTPbodyHTML = bool.Parse(rdr["SMTPbodyHTML"].ToString());
                    }

                    if (objhelperCom.sendEmailViaWebApi(oEmail))
                    {
                        oret.Result = "FAILURE";
                        oret.Message = "Reset Kata Sandi Sukses, tetapi Kata Sandi baru gagal dikirim ke alamat email Anda";

                    }
                    else
                    {
                        oret.Result = "SUCCESS";
                        oret.Message = "Kode aktivasi telah terkirim ke alamat email Anda.";
                    }

                }
                else if (oActv.ActivationType.ToUpper() == "M")
                {
                    smsParam oSMS = null;
                    odal = new SQLAccess("PORTAL");
                    rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                       odal.CreateParameter("@table", SqlDbType.VarChar, "get_SMS_setting_ClientAktivasi") as IDbDataParameter,
                       odal.CreateParameter("@skey1", SqlDbType.VarChar, "SMS_ACT_CODE") as IDbDataParameter,
                       odal.CreateParameter("@skey2", SqlDbType.VarChar, UserName) as IDbDataParameter,
                       odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                       odal.CreateParameter("@skey10", SqlDbType.VarChar, aktivasiCode) as IDbDataParameter);

                    while (rdr.Read())
                    {

                        oSMS = new smsParam();
                        oSMS.serviceLink = rdr["serviceLink"].ToString();
                        oSMS.userID = rdr["userID"].ToString();
                        oSMS.userPassword = rdr["userPassword"].ToString();
                        oSMS.phoneNo = oActv.ActivationInfo;
                        oSMS.SMSContent = rdr["SMSContent"].ToString();
                        oSMS.senderID = rdr["senderID"].ToString();
                        oSMS.divisionID = rdr["divisionID"].ToString();
                        oSMS.batchname = rdr["batchname"].ToString();
                        oSMS.uploadBy = rdr["uploadBy"].ToString();
                        oSMS.channel = rdr["channel"].ToString();

                    }

                    string tempresult = null;
                    tempresult = objhelperCom.isms_SendSMS(oSMS);

                    odal.BeginTransactions();

                    odal.ExecuteCommandText("insert into tclient_sms_result (UserID,SMSType,ResponseText,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy) select @UserID,@SMSType,@ResponseText,@CreatedDate,@CreatedBy,@UpdatedDate,@UpdatedBy", true,
                        odal.CreateParameter("@UserID", SqlDbType.VarChar, UserName) as IDbDataParameter,
                        odal.CreateParameter("@SMSType", SqlDbType.VarChar, "ACTIVATION_CODE") as IDbDataParameter,
                        odal.CreateParameter("@ResponseText", SqlDbType.VarChar, tempresult) as IDbDataParameter,
                        odal.CreateParameter("@CreatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                        odal.CreateParameter("@CreatedBy", SqlDbType.VarChar, "SYS") as IDbDataParameter,
                        odal.CreateParameter("@UpdatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                        odal.CreateParameter("@UpdatedBy", SqlDbType.VarChar, "SYS") as IDbDataParameter);

                    odal.CommitTransactions();

                    if (tempresult.Contains("ERROR"))
                    {
                        oret.Result = "FAILURE";
                        oret.Message = "Reset Kata Sandi Sukses, tetapi Kata Sandi baru gagal dikirim ke nomor ponsel Anda";
                    }
                    else
                    {
                        oret.Result = "SUCCESS";
                        oret.Message = "Kode aktivasi telah terkirim ke nomor ponsel Anda.";
                    }

                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("ResendActivationCode", "StoredProcedure and CommandText", "", "", ex.Message.ToString());
                oret.Result = "FAILURE";
                oret.Message = "Input Parameter tidak valid atau ada kesalahan System";

            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }
            return oret;
        }

        /*
            updated : RS
            date : 31 MAY 2018
            Reason : ADD Date value Token check for Old Token 
        */
        public cMenuList GetMenuList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("SPORTAL");
            IDataReader rdr;

            cMenuListDetail oMenuListDetail;
            cMenuList oMenu;
            oMenu = new cMenuList();
            oMenu.cMenuListDetail = new List<cMenuListDetail>();


            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                               odal.CreateParameter("@table", SqlDbType.VarChar, "Menu_List") as IDbDataParameter,
                               odal.CreateParameter("@skey1", SqlDbType.VarChar, ComLib.GetText(pprm.userid)) as IDbDataParameter,
                               odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter);
                while (rdr.Read())
                {
                    oMenuListDetail = new cMenuListDetail();

                    oMenuListDetail.CompanyId = rdr["CompanyId"].ToString();
                    oMenuListDetail.ObjectId = rdr["ObjectId"].ToString();
                    oMenuListDetail.ObjectName = rdr["ObjectName"].ToString();
                    oMenuListDetail.ObjectTitle = rdr["ObjectTitle"].ToString();
                    oMenuListDetail.ObjectType = rdr["ObjectType"].ToString();
                    oMenuListDetail.ObjectParent = rdr["ObjectParent"].ToString();
                    oMenuListDetail.ObjectSequence = rdr["ObjectSequence"].ToString();
                    oMenuListDetail.ObjectLink = rdr["ObjectLink"].ToString();
                    oMenuListDetail.ObjectImage = rdr["ObjectImage"].ToString();
                    oMenuListDetail.LinkChild = rdr["LinkChild"].ToString();
                    oMenuListDetail.ObjectShortcut = ComLib.GetBool(rdr["IsValidateAuthorization"]);
                    oMenu.cMenuListDetail.Add(oMenuListDetail);
                }
            }
            // oTAX.message = "SUCCESS"
            // oTAX.result = ""

            // Dim listMenu As List(Of cMenuListDetail2) = New List(Of cMenuListDetail2)
            // listMenu = New cMenuListDetail
            // listMenu = New cMenuList


            // Child = From menuch In oMenu.cMenuListDetail
            // Where menuch.ObjectParent = menu.ObjectId
            // Order By menuch.ObjectSequence Descending
            // Select
            // menuch.ObjectId,
            // menuch.ObjectName,
            // menuch.ObjectLink

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            // Dim listMenu As List(Of cMenuListDetail2) = New List(Of cMenuListDetail2)()
            // listMenu = (From menu In oMenu.cMenuListDetail
            // Where menu.ObjectType = "G"
            // Select New cMenuListDetail With {
            // .ObjectId = menu.ObjectId,
            // .ObjectName = menu.ObjectName,
            // .ObjectLink = menu.ObjectLink,
            // .Child = From menuch In oMenu.cMenuListDetail
            // Where menuch.ObjectParent = menu.ObjectId
            // Order By menuch.ObjectSequence Descending
            // Select New cMenuListDetail With {
            // .ObjectId = menuch.ObjectId,
            // .ObjectName = menuch.ObjectName,
            // .ObjectLink = menuch.ObjectLink
            // }})

            return oMenu;
        }

        public DefaultReturn ChangePassword(ChangePassword oPwd)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oparamLog = null;
            try
            {
                oparamLog = new DefaultReturn();

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "00") as IDbDataParameter,
                odal.CreateParameter("@UserID", SqlDbType.VarChar, oPwd.UserName) as IDbDataParameter,
                odal.CreateParameter("@OldPassword", SqlDbType.VarChar, oPwd.OldPassword) as IDbDataParameter,
                odal.CreateParameter("@NewPassword", SqlDbType.VarChar, oPwd.NewPassword) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oPwd.Result, 100, ParameterDirection.Output) as IDbDataParameter,
                odal.CreateParameter("@result2", SqlDbType.VarChar, oparamLog.OtherValue, 100, ParameterDirection.Output) as IDbDataParameter);

                if (odal.get_ReturnValue(1) == "CHG_PWD_01")
                {
                    oparamLog.Result = "FAILURE";
                    oparamLog.Message = "Kata Sandi Lama Tidak Sesuai";
                }
                else if (odal.get_ReturnValue(1) == "CHG_PWD_02")
                {
                    oparamLog.Result = "FAILURE";
                    oparamLog.Message = "Kata Sandi Tidak Dapat Menggunakan 5 Kata Sandi Sebelumnya";
                }
                else
                {
                    oparamLog.Result = "SUCCESS";
                    oparamLog.Message = "Pembaharuan Kata Sandi Berhasil";
                }

                oparamLog.OtherValue = odal.get_ReturnValue(2);

                odal.CommitTransactions();

            }
            catch
            {
                odal.RollBackTransactions();
                oparamLog.Result = "FAILURE";
                oparamLog.Message = "Input Parameter tidak valid atau ada kesalahan System";
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }
            return oparamLog;
        }

        public DefaultReturn ForgotPassword(ForgotPassword oPwd)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            DefaultReturn oret = null;
            helperCom objhelperLib = new helperCom();
            helperSec objhelperSec = new helperSec();
            oret = new DefaultReturn();
            try
            {

                rdr = odal.ExecuteDataReader("usp_user_management_client", CommandType.StoredProcedure,
                    odal.CreateParameter("@FlagP", SqlDbType.VarChar, "07") as IDbDataParameter,
                    odal.CreateParameter("@skey2", SqlDbType.VarChar, oPwd.PolicyID) as IDbDataParameter,
                    odal.CreateParameter("@ParamType", SqlDbType.VarChar, oPwd.SendingType.ToString().ToUpper()) as IDbDataParameter,
                    odal.CreateParameter("@ParamValue", SqlDbType.VarChar, oPwd.SendingInfo) as IDbDataParameter);

                oPwd.Result2 = "0";

                while (rdr.Read())
                {

                    if (rdr["RESULT"].ToString() == "FGT_PWD_02")
                    {
                        oPwd.Result2 = "DATA EXISTS";
                        oPwd.Result = "FAILURE";
                        oPwd.Message = "Nomor polis dan alamat email/nomor ponsel tidak sesuai.";
                    }
                    else
                    {
                        oPwd.UserName = rdr["UserID"].ToString();
                        oPwd.Result2 = "DATA EXISTS";
                        String passwordAgent = objhelperLib.RandomString(8);

                        odal.BeginTransactions();

                        odal.ExecuteCommandSP("usp_user_management_client",
                        odal.CreateParameter("@FlagP", SqlDbType.VarChar, "01") as IDbDataParameter,
                        odal.CreateParameter("@UserID", SqlDbType.VarChar, oPwd.UserName) as IDbDataParameter,
                        odal.CreateParameter("@NewPassword", SqlDbType.VarChar, objhelperSec.GetMD5(oPwd.UserName.ToUpper() + passwordAgent)) as IDbDataParameter,
                        odal.CreateParameter("@ParamType", SqlDbType.VarChar, oPwd.SendingType.ToString().ToUpper()) as IDbDataParameter,
                        odal.CreateParameter("@ParamValue", SqlDbType.VarChar, oPwd.SendingInfo) as IDbDataParameter,
                        odal.CreateParameter("@flagResult", SqlDbType.Bit, oPwd.ResultFlag, 1, ParameterDirection.Output) as IDbDataParameter,
                        odal.CreateParameter("@result", SqlDbType.VarChar, oPwd.Result, 100, ParameterDirection.Output) as IDbDataParameter);

                        oPwd.ResultFlag = bool.Parse(odal.get_ReturnValue(1));

                        if (odal.get_ReturnValue(2) == "FGT_PWD_01")
                        {
                            oPwd.Result = "SUCCESS";
                            oPwd.Message = "Kata Sandi baru telah terkirim ke alamat email yang terdaftar";
                        }
                        else if (odal.get_ReturnValue(2) == "FGT_PWD_02")
                        {
                            oPwd.Result = "FAILURE";
                            oPwd.Message = "Nomor polis dan alamat email/nomor ponsel tidak sesuai.";
                        }
                        else if (odal.get_ReturnValue(2) == "FGT_PWD_03")
                        {
                            oPwd.Result = "SUCCESS";
                            oPwd.Message = "Kata Sandi baru telah terkirim ke nomor ponsel yang terdaftar";
                        }
                        else if (odal.get_ReturnValue(2) == "FGT_PWD_04")
                        {
                            oPwd.Result = "FAILURE";
                            oPwd.Message = "Nomor polis dan alamat email/nomor ponsel tidak sesuai.";
                        }

                        odal.CommitTransactions();

                        if (oPwd.SendingType.ToString().ToUpper() == "E" && oPwd.ResultFlag == true)
                        {
                            emailParam oEmail = null;
                            odal = new SQLAccess("PORTAL");
                            rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                               odal.CreateParameter("@table", SqlDbType.VarChar, "get_SMTP_setting_ClientforgotPassword") as IDbDataParameter,
                               odal.CreateParameter("@skey1", SqlDbType.VarChar, "SMTP_FG_PWD") as IDbDataParameter,
                               odal.CreateParameter("@skey2", SqlDbType.VarChar, oPwd.UserName) as IDbDataParameter,
                               odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                               odal.CreateParameter("@skey10", SqlDbType.VarChar, passwordAgent) as IDbDataParameter);

                            while (rdr.Read())
                            {

                                oEmail = new emailParam();
                                oEmail.fromMail = rdr["fromMail"].ToString();
                                oEmail.sendemail = oPwd.SendingInfo;
                                oEmail.subjectMsg = rdr["subjectMsg"].ToString();
                                oEmail.bodyMsg = rdr["bodyMSG"].ToString();
                                oEmail.SMTPServer = rdr["SMTPServer"].ToString();
                                oEmail.PortServer = (int)rdr["PortServer"];
                                oEmail.SMTPId = rdr["SMTPId"].ToString();
                                oEmail.SMTPPassword = rdr["SMTPPassword"].ToString();
                                oEmail.SMTPSSL = bool.Parse(rdr["SMTPSSL"].ToString());
                                oEmail.SMTPbodyHTML = bool.Parse(rdr["SMTPbodyHTML"].ToString());
                            }

                            if (objhelperLib.sendEmailViaWebApi(oEmail))
                            {
                                oPwd.Result = "FAILURE";
                                oPwd.Message = "Reset Kata Sandi Sukses, tetapi Kata Sandi baru gagal dikirim ke email anda.";
                            }

                        }
                        else if (oPwd.SendingType.ToString().ToUpper() == "M" && oPwd.ResultFlag == true)
                        {
                            smsParam oSMS = null;
                            odal = new SQLAccess("PORTAL");
                            rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                               odal.CreateParameter("@table", SqlDbType.VarChar, "get_SMS_setting_ClientforgotPassword") as IDbDataParameter,
                               odal.CreateParameter("@skey1", SqlDbType.VarChar, "SMS_FG_PWD") as IDbDataParameter,
                               odal.CreateParameter("@skey2", SqlDbType.VarChar, oPwd.UserName) as IDbDataParameter,
                               odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                               odal.CreateParameter("@skey10", SqlDbType.VarChar, passwordAgent) as IDbDataParameter);

                            while (rdr.Read())
                            {

                                oSMS = new smsParam();
                                oSMS.serviceLink = rdr["serviceLink"].ToString();
                                oSMS.userID = rdr["userID"].ToString();
                                oSMS.userPassword = rdr["userPassword"].ToString();
                                oSMS.phoneNo = oPwd.SendingInfo;
                                oSMS.SMSContent = rdr["SMSContent"].ToString();
                                oSMS.senderID = rdr["senderID"].ToString();
                                oSMS.divisionID = rdr["divisionID"].ToString();
                                oSMS.batchname = rdr["batchname"].ToString();
                                oSMS.uploadBy = rdr["uploadBy"].ToString();
                                oSMS.channel = rdr["channel"].ToString();

                            }

                            string tempresult = null;
                            tempresult = objhelperLib.isms_SendSMS(oSMS);

                            odal.BeginTransactions();

                            odal.ExecuteCommandText("insert into tclient_sms_result (UserID,SMSType,ResponseText,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy) select @UserID,@SMSType,@ResponseText,@CreatedDate,@CreatedBy,@UpdatedDate,@UpdatedBy", true,
                                odal.CreateParameter("@UserID", SqlDbType.VarChar, oPwd.UserName) as IDbDataParameter,
                                odal.CreateParameter("@SMSType", SqlDbType.VarChar, "FORGOT_PWD") as IDbDataParameter,
                                odal.CreateParameter("@ResponseText", SqlDbType.VarChar, tempresult) as IDbDataParameter,
                                odal.CreateParameter("@CreatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                                odal.CreateParameter("@CreatedBy", SqlDbType.VarChar, "SYS") as IDbDataParameter,
                                odal.CreateParameter("@UpdatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                                odal.CreateParameter("@UpdatedBy", SqlDbType.VarChar, "SYS") as IDbDataParameter);

                            odal.CommitTransactions();

                            if (tempresult.Contains("ERROR"))
                            {
                                oPwd.Result = "FAILURE";
                                oPwd.Message = "Reset Kata Sandi Sukses, tetapi Kata Sandi baru gagal dikirim ke nomor ponsel anda.";
                            }

                        }

                    }

                }

                if (oPwd.Result2 == "0")
                {
                    oPwd.Result = "FAILURE";
                    oPwd.Message = "User ID Anda belum terdaftar. Lakukan pendaftaran terlebih dahulu.";
                }

                oret.Result = oPwd.Result;
                oret.Message = oPwd.Message;

            }
            catch (Exception ex)
            {
                oret.Result = "FAILURE";
                oret.Message = "Input Parameter tidak valid atau ada kesalahan System";

            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }
            return oret;
        }

        public List<DropDownViewModel> GetCompanyList(/*cParams pprm*/)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;

            var dd = new DropDownViewModel();
            var list = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                               odal.CreateParameter("@table", SqlDbType.VarChar, "list_company") as IDbDataParameter);
                while (rdr.Read())
                {
                    dd = new DropDownViewModel();

                    dd.Id = rdr["Value1"].ToString();
                    dd.Value = rdr["Value2"].ToString();
                    list.Add(dd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public List<cButtonList> GetButtonList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;

            var dd = new cButtonList();
            var list = new List<cButtonList>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                               odal.CreateParameter("@table", SqlDbType.VarChar, "Button_List") as IDbDataParameter,
                               odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                               odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.userid) as IDbDataParameter);
                while (rdr.Read())
                {
                    dd = new cButtonList();

                    dd.CompanyId = rdr["CompanyId"].ToString();
                    dd.ObjectId = rdr["ObjectId"].ToString();
                    dd.ObjectName = rdr["ObjectName"].ToString();
                    dd.ObjectTitle = rdr["ObjectTitle"].ToString();
                    dd.ObjectType = rdr["ObjectType"].ToString();
                    dd.ObjectParent = rdr["ObjectParent"].ToString();
                    dd.ObjectSequence = rdr["ObjectSequence"].ToString();
                    dd.ObjectLink = rdr["ObjectLink"].ToString();
                    dd.ObjectImage = rdr["ObjectImage"].ToString();
                    dd.LinkChild = rdr["LinkChild"].ToString();
                    dd.ObjectShortcut = ComLib.GetBool(rdr["IsValidateAuthorization"]);
                    list.Add(dd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        #region POLICY
        public IEnumerable<PolicyListViewModel> GetPolicyList(ParameterListPolicyViewModel param)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<PolicyListViewModel>();

            try
            {
                var minDate = new DateTime(1753, 1, 1);
                var maxDate = DateTime.MaxValue;
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_policy") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, param.CompanyID) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, param.PolicyNo != null ? param.PolicyNo : "") as IDbDataParameter,
                        odal.CreateParameter("@skey2", SqlDbType.VarChar, param.ApplicationNo != null ? param.ApplicationNo : "") as IDbDataParameter,
                        odal.CreateParameter("@skey3", SqlDbType.VarChar, param.PlanCode != null ? param.PlanCode : "") as IDbDataParameter,
                        odal.CreateParameter("@skey4", SqlDbType.VarChar, param.Status != null ? param.Status : "") as IDbDataParameter,
                        odal.CreateParameter("@skey5", SqlDbType.VarChar, param.MobileNo != null ? param.MobileNo : "") as IDbDataParameter,
                        odal.CreateParameter("@dkey1", SqlDbType.DateTime, param.BirthDate != null ? param.BirthDate : minDate) as IDbDataParameter,
                        odal.CreateParameter("@dkey2", SqlDbType.DateTime, param.BirthDate != null ? param.BirthDate : maxDate) as IDbDataParameter,
                        odal.CreateParameter("@dkey3", SqlDbType.DateTime, param.EffectiveDateFrom != null ? param.EffectiveDateFrom.Value.Date : minDate) as IDbDataParameter,
                        odal.CreateParameter("@dkey4", SqlDbType.DateTime, param.EffectiveDateTo != null ? param.EffectiveDateTo.Value.Date : Convert.ToDateTime(DateTime.MaxValue.Date)) as IDbDataParameter,
                        odal.CreateParameter("@skey6", SqlDbType.VarChar, param.CustomerName != null ? param.CustomerName : "") as IDbDataParameter,
                        odal.CreateParameter("@pageNum", SqlDbType.Int, param.Page) as IDbDataParameter,
                        odal.CreateParameter("@pageSize", SqlDbType.Int, param.PerPage) as IDbDataParameter,
                        odal.CreateParameter("@prop", SqlDbType.VarChar, param.Prop != null ? param.Prop : "") as IDbDataParameter,
                        odal.CreateParameter("@sort", SqlDbType.VarChar, param.Sort != null ? param.Sort : "") as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new PolicyListViewModel
                    {
                        ID = rdr["CertificateID"].ToString(),
                        PolicyNo = rdr["PolicyNo"].ToString(),
                        ApplicationNo = rdr["ApplicationNo"].ToString(),
                        CertificateNo = rdr["CertificateNo"].ToString(),
                        CodeID = rdr["CodeID"].ToString(),
                        Status = rdr["Status"].ToString(),
                        OwnerName = rdr["OwnerName"].ToString(),
                        PlanCode = rdr["PlanCode"].ToString(),
                        PlanName = rdr["PlanName"].ToString(),
                        Premium = rdr.IsDBNull(9) ? 0 : Convert.ToDecimal(rdr["Premium"].ToString()),
                        EffectiveDate = helperLib.GetDate(rdr["EffectiveDate"]),
                        CoverageDuration = rdr.IsDBNull(11) ? 0 : Convert.ToInt32(rdr["CoverageDuration"].ToString()),
                        FreeLookPending = rdr["FreeLookPending"].ToString(),
                        BirthDate = helperLib.GetDate(rdr["BirthDate"]),
                        MobileNo = rdr["MobileNo"].ToString(),
                        CustomerName = rdr["CertificateName"].ToString(),
                        Length = Convert.ToInt32(rdr["Length"])
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<DropDownViewModel> GetStatusList(string codeType)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;

            var dd = new DropDownViewModel();
            var list = new List<DropDownViewModel>();
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_status") as IDbDataParameter,
                      odal.CreateParameter("@skey1", SqlDbType.VarChar, codeType) as IDbDataParameter);
                while (rdr.Read())
                {
                    dd = new DropDownViewModel();
                    dd.Id = rdr["CodeID"].ToString();
                    dd.Value = rdr["CodeName"].ToString();
                    list.Add(dd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;

        }

        public List<DropDownViewModel> GetPlanList()
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;

            var dd = new DropDownViewModel();
            var list = new List<DropDownViewModel>();
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_plan") as IDbDataParameter);
                while (rdr.Read())
                {
                    dd = new DropDownViewModel();
                    dd.Id = rdr["PlanCode"].ToString();
                    dd.Value = rdr["PlanCode"].ToString() + " - " + rdr["PlanName"].ToString();
                    list.Add(dd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public List<DropDownViewModel> GetPlanListByProduct(string productID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;

            var dd = new DropDownViewModel();
            var list = new List<DropDownViewModel>();
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_plan_by_product") as IDbDataParameter,
                      odal.CreateParameter("@skey1", SqlDbType.VarChar, productID) as IDbDataParameter);
                while (rdr.Read())
                {
                    dd = new DropDownViewModel();
                    dd.Id = rdr["PlanCode"].ToString();
                    dd.Value = rdr["PlanCode"].ToString() + " - " + rdr["PlanName"].ToString();
                    list.Add(dd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public List<string> GetColumnUpload(string productCode)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;

            var stringArray = new List<string> { };
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_upload") as IDbDataParameter,
                      odal.CreateParameter("@skey1", SqlDbType.VarChar, productCode) as IDbDataParameter);
                while (rdr.Read())
                {
                    stringArray.Add(rdr["FieldName"].ToString().ToLower().TrimStart().TrimEnd());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return stringArray;
        }

        public List<TdCodeViewModel> GetTdCode()
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var dd = new TdCodeViewModel();
            List<TdCodeViewModel> list = new List<TdCodeViewModel>();
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_tdcode") as IDbDataParameter);

                while (rdr.Read())
                {
                    dd = new TdCodeViewModel();
                    dd.CodeType = rdr["CodeType"].ToString();
                    dd.CodeID = rdr["CodeID"].ToString();
                    dd.CodeName = rdr["CodeName"].ToString();
                    list.Add(dd);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public List<TdCodeViewModel> GetTParameter()
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var dd = new TdCodeViewModel();
            List<TdCodeViewModel> list = new List<TdCodeViewModel>();
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_tparameter") as IDbDataParameter);

                while (rdr.Read())
                {
                    dd = new TdCodeViewModel();
                    dd.CodeType = rdr["ParameterType"].ToString();
                    dd.CodeID = rdr["ParameterID"].ToString();
                    dd.CodeName = rdr["ParameterName"].ToString();
                    list.Add(dd);
                }

            }
            catch (Exception ex) { throw ex; }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public string ValidatePolicy(string productCode, string column, object input)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            string message = null;

            try
            {
                var dataTypeInput = input.GetType().Name;
                if (dataTypeInput.ToLower() == "datetime")
                {
                    input = Convert.ToDateTime(input).ToString("yyyy-MM-dd");
                }

                rdr = odal.ExecuteDataReader("usp_validate_template", CommandType.StoredProcedure,
                      odal.CreateParameter("@productCode", SqlDbType.VarChar, productCode.ToLower()) as IDbDataParameter,
                      odal.CreateParameter("@column", SqlDbType.VarChar, column.ToLower()) as IDbDataParameter,
                      odal.CreateParameter("@input", SqlDbType.VarChar, input.ToString()) as IDbDataParameter);
                while (rdr.Read())
                {
                    message = rdr["Message"].ToString() == "" ? message = null : message = rdr["Message"].ToString();
                }

                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
            }
        }

        public string NotifExclude(string religion, string cityArea, string domisili, string planCode) {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            string message = null;

            try
            {
                rdr = odal.ExecuteDataReader("usp_validate_exclude", CommandType.StoredProcedure,
                      odal.CreateParameter("@agama", SqlDbType.VarChar, religion.ToLower()) as IDbDataParameter,
                      odal.CreateParameter("@tinggalKTP", SqlDbType.VarChar, cityArea.ToLower()) as IDbDataParameter,
                      odal.CreateParameter("@domisili", SqlDbType.VarChar, domisili.ToLower()) as IDbDataParameter,
                      odal.CreateParameter("@planCode", SqlDbType.VarChar, planCode.ToString()) as IDbDataParameter);
                while (rdr.Read())
                {
                    message = rdr["Message"].ToString() == "" ? message = null : message = rdr["Message"].ToString();
                }

                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
            }

        }

        public List<ProductViewModel> GetProductsList()
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;

            var list = new List<ProductViewModel>();
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_product") as IDbDataParameter);
                while (rdr.Read())
                {
                    var dd = new ProductViewModel();
                    dd.ProductCode = rdr["ProductCode"].ToString();
                    dd.ProductName = rdr["ProductName"].ToString();
                    dd.FileType = rdr["FileType"].ToString();
                    dd.TemplateFile = rdr["TemplateFile"].ToString();
                    list.Add(dd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;

        }

        public void PolicyApproved(int ID, string userID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            var model = new List<DropDownViewModel>();
            IDataReader rdr1;
            try
            {
                rdr1 = odal.ExecuteDataReader("usp_crud_certificate", CommandType.StoredProcedure,
                      odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_STATUS") as IDbDataParameter,
                      odal.CreateParameter("@certificateStatus", SqlDbType.VarChar, StatusPolicy.Approve) as IDbDataParameter,
                      odal.CreateParameter("@updatedBy", SqlDbType.VarChar, userID) as IDbDataParameter,
                      odal.CreateParameter("@certificateID", SqlDbType.Int, ID) as IDbDataParameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr1 = null;
            }
        }

        public List<TUploadViewModel> GetTUpload(string productCode)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<TUploadViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_upload") as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, productCode) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new TUploadViewModel
                    {
                        FieldName = rdr["FieldName"].ToString(),
                        DataType = rdr["DataType"].ToString(),
                        Sequence = Convert.ToInt32(rdr["Sequence"]),
                        Parameter = rdr["Parameter"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public int CreatePolicy(string userID, string companyID, string productCode, RowData input)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            int certificateID = 0;
            try
            {
                var columnTemplate = GetTUpload(productCode);
                var list = new List<TUploadViewModel>();
                foreach (var item in columnTemplate)
                {
                    if (item.Parameter.Contains(","))
                    {
                        var param = item.Parameter.Split(new string[] { ", " }, StringSplitOptions.None);
                        foreach (var p in param)
                        {
                            list.Add(new TUploadViewModel { FieldName = item.FieldName, DataType = item.DataType, Sequence = item.Sequence, Parameter = p });
                        }
                    }
                    else
                    {
                        list.Add(item);
                    }
                }

                SqlConnection con = new SqlConnection(odal.GetConnectionString());

                using (SqlCommand cmd = new SqlCommand("usp_save_policy", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@productCode", productCode));
                    cmd.Parameters.Add(new SqlParameter("@companyID", companyID));
                    foreach (var item in list)
                    {
                        if (item.DataType.ToLower() == "varchar")
                        {
                            cmd.Parameters.Add(new SqlParameter(item.Parameter, input.Value[item.Sequence].ToString().TrimStart().TrimEnd()));
                        }
                        else if (item.DataType.ToLower() == "datetime")
                        {
                            if (input.Value[item.Sequence].ToString().TrimStart().TrimEnd() != "")
                            {
                                cmd.Parameters.Add(new SqlParameter(item.Parameter, Convert.ToDateTime(input.Value[item.Sequence])));
                            }
                        }
                        else if (item.DataType.ToLower() == "decimal")
                        {
                            if (input.Value[item.Sequence].ToString().TrimStart().TrimEnd() != "")
                            {
                                cmd.Parameters.Add(new SqlParameter(item.Parameter, Convert.ToDecimal(input.Value[item.Sequence])));
                            }
                        }
                    }
                    cmd.Parameters.Add(new SqlParameter("@createdDate", DateTime.Now));
                    cmd.Parameters.Add(new SqlParameter("@createdBy", userID));
                    //cmd.ExecuteNonQuery();
                    rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        certificateID = Convert.ToInt32(rdr["CertificateID"]);
                    }
                }

                return certificateID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }
        }

        public string ValidatePerRow(List<CertificateRowViewModel> row, string productCode)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            SqlConnection con = new SqlConnection(odal.GetConnectionString());
            string message = null;
            IDataReader rdr;
            using (SqlCommand cmd = new SqlCommand("usp_validate_row @row", con))
            {
                using (var table = new DataTable())
                {
                    con.Open();
                    table.Columns.Add("Header", typeof(string));
                    table.Columns.Add("DataType", typeof(string));
                    table.Columns.Add("Value", typeof(string));

                    table.Rows.Add("product code", "string", productCode);

                    foreach (var r in row)
                    {
                        var rValue = r.Value;
                        if (r.DataType == "string")
                        {
                            rValue = r.Value.ToString().ToLower();
                        }
                        else if (r.DataType == "datetime")
                        {
                            rValue = Convert.ToDateTime(r.Value).ToString("yyyy-MM-dd");
                        }
                        table.Rows.Add(r.Header, r.DataType, rValue);
                    }

                    var list = new SqlParameter("@row", SqlDbType.Structured);
                    list.TypeName = "dbo.CertificateRow";
                    list.Value = table;
                    cmd.Parameters.Add(list);

                    rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        message = rdr["Message"].ToString() == "" ? message = null : message = rdr["Message"].ToString();
                    }

                    return message;
                }
            }
        }

        public List<ECertificateViewModel> GetECertificateData(string companyID, string arrayID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            var ListeCertificate = new List<ECertificateViewModel>();
            IDataReader rdr1;
            try
            {
                rdr1 = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "get_ecertificate_data") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, companyID) as IDbDataParameter,
                      odal.CreateParameter("@skey10", SqlDbType.VarChar, arrayID) as IDbDataParameter);
                while (rdr1.Read())
                {
                    ListeCertificate.Add(new ECertificateViewModel
                    {
                        certificatenumber = rdr1["CertificateNo"].ToString(),
                        effectivedate = string.Format("{0:dd-MMM-yy}", rdr1["EffectiveDate"]),
                        planname = rdr1["PlanName"].ToString(),
                        ownername = rdr1["OwnerName"].ToString(),
                        ownerdob = string.Format("{0:dd-MMM-yy}", rdr1["OwnerDOB"]),
                        ownerage = rdr1["OwnerAge"].ToString(),
                        gender = rdr1["Gender"].ToString(),
                        address = rdr1["Address"].ToString(),
                        mobilenumber = rdr1["MobileNo"].ToString(),
                        EMAIL_1 = rdr1["Email"].ToString(),
                        customername = rdr1["InsuredName"].ToString(),
                        dateofbirth = string.Format("{0:dd-MMM-yy}", rdr1["InsuredDOB"]),
                        insuredage = rdr1["InsuredAge"].ToString(),
                        totalpremium = rdr1.IsDBNull(13) ? "0" : rdr1["TotalPremium"].ToString(),
                        sumassured = rdr1.IsDBNull(14) ? "0" : rdr1["SumAssured"].ToString(),
                        hospital = rdr1["Hospital"].ToString(),
                        adb = rdr1.IsDBNull(16) ? "0" : rdr1["adb"].ToString(),
                        tpd = rdr1.IsDBNull(17) ? "0" : rdr1["tpd"].ToString(),
                        adbmotorcycle = rdr1.IsDBNull(18) ? "0" : rdr1["ADBMotorcycle"].ToString(),
                        deathterrorism = rdr1.IsDBNull(19) ? "0" : rdr1["DeathBenefitTerorism"].ToString(),
                        beneficiaryname = rdr1["BeneficiaryName"].ToString(),
                        maturitydate = string.Format("{0:dd-MMM-yy}", rdr1["MaturityDate"]),
                        partnername = rdr1["PartnerName"].ToString(),
                        coverageduration = rdr1.IsDBNull(23) ? "0" : rdr1["CoverageDuration"].ToString(),
                        hospitallimit = rdr1.IsDBNull(24) ? "0" : rdr1["HospitalLimit"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }

            return ListeCertificate;
        }

        public void UpdateCertificateRequestID(string certificateNo, string requestID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;

            try
            {
                rdr = odal.ExecuteDataReader("usp_crud_certificate", CommandType.StoredProcedure,
                             odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_REQUESTID") as IDbDataParameter,
                             odal.CreateParameter("@requestID", SqlDbType.VarChar, requestID) as IDbDataParameter,
                             odal.CreateParameter("@certificateNo", SqlDbType.VarChar, certificateNo) as IDbDataParameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally 
            {
                odal.CloseAllConnection();
                odal = null;
                rdr = null;
            }
        }

        public void InsertNotification(TNotification model)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;

            try
            {
                rdr = odal.ExecuteDataReader("usp_crud_notification", CommandType.StoredProcedure,
                             odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT") as IDbDataParameter,
                             odal.CreateParameter("@certificateNo", SqlDbType.VarChar, model.CertificateNo) as IDbDataParameter,
                             odal.CreateParameter("@clientNotifRefId", SqlDbType.VarChar, model.ClientNotifRefId) as IDbDataParameter,
                             odal.CreateParameter("@message", SqlDbType.VarChar, model.Message) as IDbDataParameter,
                             odal.CreateParameter("@requestID", SqlDbType.VarChar, model.RequestID) as IDbDataParameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr = null;
            }
        }
        #endregion

        #region POLICY MAINTENANCE
        public List<DropDownViewModel> GetBillingMode(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var list = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_billing_mode") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                      odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    list.Add(new DropDownViewModel
                    {
                        Id = rdr["CodeID"].ToString(),
                        Value = rdr["CodeName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public List<DropDownViewModel> GetBillingStatus(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var list = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_billing_status") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                      odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    list.Add(new DropDownViewModel
                    {
                        Id = rdr["CodeID"].ToString(),
                        Value = rdr["CodeName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public List<DropDownViewModel> GetCurrency(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var list = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_currency") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                      odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    list.Add(new DropDownViewModel
                    {
                        Id = rdr["CodeID"].ToString(),
                        Value = rdr["CodeName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public int GetSequenceTUpload(string productCode, string column)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            int seq = 0;
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "get_sequence_tupload") as IDbDataParameter,
                      odal.CreateParameter("@skey1", SqlDbType.VarChar, productCode.ToLower().TrimStart().TrimEnd()) as IDbDataParameter,
                      odal.CreateParameter("@skey2", SqlDbType.VarChar, column.ToLower().TrimStart().TrimEnd()) as IDbDataParameter);
                while (rdr.Read())
                {
                    seq = Convert.ToInt32(rdr["Sequence"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return seq;
        }

        public List<DropDownViewModel> GetGender(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var list = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_gender") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                      odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    list.Add(new DropDownViewModel
                    {
                        Id = rdr["CodeID"].ToString(),
                        Value = rdr["CodeName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public HeaderPolicyMaintenanceViewModel GetHeaderPolicy(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new HeaderPolicyMaintenanceViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "header_policy") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.VarChar, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.ID = rdr["CertificateID"].ToString();
                    model.PolicyNo = rdr["PolicyNo"].ToString();
                    model.SPAJNo = rdr["SPAJNo"].ToString();
                    model.PolicyStatus = rdr["PolicyStatus"].ToString();
                    model.UnderwritingStatus = rdr["UnderwritingStatus"].ToString();
                    model.FreeLookPolicy = rdr["FreeLookPolicy"].ToString();
                    model.OwnerName = rdr["OwnerName"].ToString();
                    model.OwnerBirthday = helperLib.GetDate(rdr["OwnerBirthday"].ToString());
                    model.BusinessPartner = rdr["BusinessPartner"].ToString();
                    model.ProductName = rdr["ProductName"].ToString();
                    model.PrevCertificateNo = rdr.IsDBNull(10) ? null : rdr["PrevCertificateNo"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public PolicyInfoViewModel GetPolicyInformation(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new PolicyInfoViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "policy_information") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.VarChar, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.EffectiveDate = rdr.IsDBNull(0) ? (DateTime?)null : helperLib.GetDate(rdr["EffectiveDate"]);
                    model.ReceivedDate = rdr.IsDBNull(1) ? (DateTime?)null : helperLib.GetDate(rdr["ReceivedDate"]);
                    model.SignDate = rdr.IsDBNull(2) ? (DateTime?)null : helperLib.GetDate(rdr["SignDate"]);
                    model.RequiredHC = rdr["RequiredHC"].ToString();
                    model.DeliveryFee = rdr.IsDBNull(4) ? 0 : Convert.ToDecimal(rdr["DeliveryFee"]);
                    model.Currency = rdr["Currency"].ToString();
                    model.CurrencyName = rdr["CurrencyName"].ToString();
                    model.BillingMode = rdr["BillingMode"].ToString();
                    model.BillingModeName = rdr["BillingModeName"].ToString();
                    model.BillingStatus = rdr["BillingStatus"].ToString();
                    model.BillingStatusName = rdr["BillingStatusName"].ToString();
                    model.MaturityDate = rdr.IsDBNull(11) ? (DateTime?)null : helperLib.GetDate(rdr["MaturityDate"]);
                    model.CancelDate = rdr.IsDBNull(12) ? (DateTime?)null : helperLib.GetDate(rdr["CancelDate"]);
                    model.PremiumAmount = rdr.IsDBNull(13) ? 0 : Convert.ToDecimal(rdr["PremiumAmount"]);
                    model.PremiumNett = rdr.IsDBNull(14) ? 0 : Convert.ToDecimal(rdr["PremiumNett"]);
                    model.PremiumTax = rdr.IsDBNull(15) ? 0 : Convert.ToDecimal(rdr["PremiumTax"]);
                    model.TaxPercentage = rdr.IsDBNull(16) ? 0 : float.Parse(rdr["TaxPercentage"].ToString()); //float
                    model.ExtraMortalityRating = rdr.IsDBNull(17) ? 0 : float.Parse(rdr["ExtraMortalityRating"].ToString()); //float
                    model.ExtraPremiumAmount = rdr.IsDBNull(18) ? 0 : Convert.ToDecimal(rdr["ExtraPremiumAmount"]);
                    model.ExtraNettPremiumAmount = rdr.IsDBNull(19) ? 0 : Convert.ToDecimal(rdr["ExtraNettPremiumAmount"]);
                    model.TotalPremiumAmount = rdr.IsDBNull(20) ? 0 : Convert.ToDecimal(rdr["TotalPremiumAmount"]);
                    model.Notes = rdr["Notes"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public OwnerInformationViewModel GetOwnerInformation(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new OwnerInformationViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "owner_information") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.VarChar, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.OwnerID = rdr["OwnerID"].ToString();
                    model.OwnerName = rdr["OwnerName"].ToString();
                    model.OwnerDOB = rdr.IsDBNull(2) ? (DateTime?)null : helperLib.GetDate(rdr["OwnerDOB"]);
                    model.OwnerGender = rdr["OwnerGender"].ToString();
                    model.OwnerContact = rdr["OwnerContact"].ToString();
                    model.OwnerAddress = rdr["OwnerAddress"].ToString();
                    model.City = rdr["City"].ToString();
                    model.Province = rdr["Province"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<InsuredORbeneficiaryViewModel> GetInsuredInformationList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<InsuredORbeneficiaryViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "insured_information") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.VarChar, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new InsuredORbeneficiaryViewModel
                    {
                        ID = rdr["ID"].ToString(),
                        CustomerID = rdr["CustomerID"].ToString(),
                        Name = rdr["Name"].ToString(),
                        DOB = rdr.IsDBNull(2) ? (DateTime?)null : helperLib.GetDate(rdr["DOB"]),
                        Gender = rdr["Gender"].ToString(),
                        Relation = rdr["Relation"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<InsuredORbeneficiaryViewModel> GetBeneficiaryInformationList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<InsuredORbeneficiaryViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "beneficiary_information") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.VarChar, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new InsuredORbeneficiaryViewModel
                    {
                        ID = rdr["ID"].ToString(),
                        CustomerID = rdr["CustomerID"].ToString(),
                        Name = rdr["Name"].ToString(),
                        DOB = rdr.IsDBNull(2) ? (DateTime?)null : helperLib.GetDate(rdr["DOB"]),
                        Gender = rdr["Gender"].ToString(),
                        Relation = rdr["Relation"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<CustomerViewModel> GetCustomerListForPolicyMaintenance(string companyID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<CustomerViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "customer_list_policymaintenance") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, companyID) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new CustomerViewModel
                    {
                        CustomerID = rdr["CustomerID"].ToString(),
                        Name = rdr["Name"].ToString(),
                        DOB = rdr.IsDBNull(2) ? (DateTime?)null : helperLib.GetDate(rdr["DOB"]),
                        Gender = rdr["Gender"].ToString(),
                        MainContact = rdr["MainContact"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<CustomerListViewModel> GetCustomerList(ParameterCustomerListViewModel param)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<CustomerListViewModel>();

            try
            {
                var minDate = new DateTime(1753, 1, 1);
                var maxDate = DateTime.MaxValue;
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "customer_list") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, param.CompanyID) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, param.CustomerName) as IDbDataParameter,
                        odal.CreateParameter("@skey2", SqlDbType.VarChar, param.CustomerType) as IDbDataParameter,
                        odal.CreateParameter("@dkey1", SqlDbType.DateTime, param.DOB != null ? param.DOB : minDate) as IDbDataParameter,
                        odal.CreateParameter("@dkey2", SqlDbType.DateTime, param.DOB != null ? param.DOB : maxDate) as IDbDataParameter,
                        odal.CreateParameter("@skey3", SqlDbType.VarChar, param.Gender) as IDbDataParameter,
                        odal.CreateParameter("@pageNum", SqlDbType.Int, param.Page) as IDbDataParameter,
                        odal.CreateParameter("@pageSize", SqlDbType.Int, param.PerPage) as IDbDataParameter,
                        odal.CreateParameter("@prop", SqlDbType.VarChar, param.Prop != null ? param.Prop : "") as IDbDataParameter,
                        odal.CreateParameter("@sort", SqlDbType.VarChar, param.Sort != null ? param.Sort : "") as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new CustomerListViewModel
                    {
                        ID = rdr["CustomerID"].ToString(),
                        Name = rdr["Name"].ToString(),
                        GenderID = rdr["GenderID"].ToString(),
                        Gender = rdr["Gender"].ToString(),
                        DOB = rdr.IsDBNull(2) ? (DateTime?)null : helperLib.GetDate(rdr["DOB"]),
                        TypeID = rdr["TypeID"].ToString(),
                        Type = rdr["Type"].ToString(),
                        Length = Convert.ToInt32(rdr["Length"])
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<CoverageViewModel> GetCoverageInfoList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<CoverageViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "coverage_information") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.VarChar, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new CoverageViewModel
                    {
                        ID = Convert.ToInt32(rdr["ID"]),
                        PlanCode = rdr["PlanCode"].ToString(),
                        PlanName = rdr["PlanName"].ToString(),
                        PremiumAmount = rdr.IsDBNull(3) ? 0 : Convert.ToDecimal(rdr["FaceAmount"]),
                        Period = rdr.IsDBNull(4) ? 0 : Convert.ToInt32(rdr["Duration"]),
                        PeriodType = rdr.IsDBNull(5) ? "Days" : rdr["DurationType"].ToString(),
                        EffectiveDate = rdr.IsDBNull(6) ? (DateTime?)null : helperLib.GetDate(rdr["EffectiveDate"]),
                        Maturity = rdr.IsDBNull(7) ? (DateTime?)null : helperLib.GetDate(rdr["Maturity"]),
                        status = rdr["status"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<BenefitViewModel> GetBenefitList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<BenefitViewModel>();
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "benefit_list") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.VarChar, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new BenefitViewModel
                    {
                        Benefit = rdr["Name"].ToString(),
                        SumAssured = Convert.ToDecimal(rdr["BenefitAmount"]),
                        LimitClaim = Convert.ToInt32(rdr["BenefitDuration"])
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }

            return model;
        }

        public List<ActivityViewModel> GetActivityList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<ActivityViewModel>();
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "activity_list") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.VarChar, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new ActivityViewModel
                    {
                        HistoryDate = helperLib.GetDate(rdr["HistoryDate"]),
                        By = rdr["By"].ToString(),
                        Activity = rdr["Activity"].ToString(),
                        Notes = rdr["Notes"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }

            return model;

        }

        public void CreateActivity(string transactionType, string historyType, int certifID, string notes, string createdBy)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            try
            {
                rdr = odal.ExecuteDataReader("usp_crud_activity", CommandType.StoredProcedure,
                        odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT") as IDbDataParameter,
                        odal.CreateParameter("@certificateID", SqlDbType.Int, certifID) as IDbDataParameter,
                        odal.CreateParameter("@transactionType", SqlDbType.VarChar, transactionType) as IDbDataParameter,
                        odal.CreateParameter("@historyType", SqlDbType.VarChar, historyType) as IDbDataParameter,
                        odal.CreateParameter("@historyDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                        odal.CreateParameter("@notes", SqlDbType.VarChar, notes) as IDbDataParameter,
                        odal.CreateParameter("@createdBy", SqlDbType.VarChar, createdBy) as IDbDataParameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public void UpdatePolicy(PolicyMaintenanceViewModel param)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr1;
            IDataReader rdr2;
            IDataReader rdr3;
            IDataReader rdr4;
            IDataReader rdr5;
            try
            {
                //tCertificate
                //Policy Info
                rdr1 = odal.ExecuteDataReader("usp_crud_certificate", CommandType.StoredProcedure,
                 odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE") as IDbDataParameter,
                 odal.CreateParameter("@certificateID", SqlDbType.Int, Convert.ToInt32(param.HeaderPolicy.ID)) as IDbDataParameter,
                 odal.CreateParameter("@companyID", SqlDbType.VarChar, param.CompanyID) as IDbDataParameter,
                 odal.CreateParameter("@effectiveDate", SqlDbType.DateTime, Convert.ToDateTime(param.PolicyInfo.EffectiveDate)) as IDbDataParameter,
                 odal.CreateParameter("@receivedDate", SqlDbType.DateTime, Convert.ToDateTime(param.PolicyInfo.ReceivedDate)) as IDbDataParameter,
                 odal.CreateParameter("@signDate", SqlDbType.DateTime, Convert.ToDateTime(param.PolicyInfo.SignDate)) as IDbDataParameter,
                 odal.CreateParameter("@hardCopyPolicy", SqlDbType.VarChar, param.PolicyInfo.RequiredHC) as IDbDataParameter,
                 odal.CreateParameter("@deliveryFee", SqlDbType.Decimal, Convert.ToDecimal(param.PolicyInfo.DeliveryFee)) as IDbDataParameter,
                 odal.CreateParameter("@currency", SqlDbType.VarChar, param.PolicyInfo.Currency) as IDbDataParameter,
                 odal.CreateParameter("@billingMode", SqlDbType.VarChar, param.PolicyInfo.BillingMode) as IDbDataParameter,
                 odal.CreateParameter("@billingStatus", SqlDbType.VarChar, param.PolicyInfo.BillingStatus) as IDbDataParameter,
                 odal.CreateParameter("@premium", SqlDbType.Decimal, Convert.ToDecimal(param.PolicyInfo.PremiumAmount)) as IDbDataParameter,
                 odal.CreateParameter("@totalPremium", SqlDbType.Decimal, Convert.ToDecimal(param.PolicyInfo.PremiumNett)) as IDbDataParameter,
                 odal.CreateParameter("@premiumTax", SqlDbType.Decimal, Convert.ToDecimal(param.PolicyInfo.PremiumTax)) as IDbDataParameter,
                 odal.CreateParameter("@taxPercentage", SqlDbType.Float, param.PolicyInfo.TaxPercentage) as IDbDataParameter,
                 odal.CreateParameter("@extraMortalityRating", SqlDbType.Float, param.PolicyInfo.ExtraMortalityRating) as IDbDataParameter,
                 odal.CreateParameter("@notes", SqlDbType.VarChar, param.PolicyInfo.Notes) as IDbDataParameter,
                 odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                 odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);

                //TCertificate_Coverage
                //Policy Info
                rdr2 = odal.ExecuteDataReader("usp_crud_certificate_coverage", CommandType.StoredProcedure,
                 odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE") as IDbDataParameter,
                 odal.CreateParameter("@certificateID", SqlDbType.Int, Convert.ToInt32(param.HeaderPolicy.ID)) as IDbDataParameter,
                 odal.CreateParameter("@extraPremiumAmount", SqlDbType.Decimal, Convert.ToDecimal(param.PolicyInfo.ExtraPremiumAmount)) as IDbDataParameter,
                 odal.CreateParameter("@extraPremiumNettAmount", SqlDbType.Decimal, Convert.ToDecimal(param.PolicyInfo.ExtraNettPremiumAmount)) as IDbDataParameter,
                 odal.CreateParameter("@totalPremium", SqlDbType.Decimal, Convert.ToDecimal(param.PolicyInfo.TotalPremiumAmount)) as IDbDataParameter,
                 odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                 odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);

                //TCertificate_Beneficiary
                bool IsCheckBeneficiary = false;
                if (param.BeneficiaryList != null)
                {
                    var arrayID = string.Join(",", param.BeneficiaryList.Select(m => m.ID));
                    rdr5 = odal.ExecuteDataReader("usp_crud_certificate_benificiary", CommandType.StoredProcedure,
                         odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_BENEFICIARY_USING_NOT_IN") as IDbDataParameter,
                         odal.CreateParameter("@arrayID", SqlDbType.VarChar, arrayID) as IDbDataParameter,
                         odal.CreateParameter("@CertificateID", SqlDbType.VarChar, param.HeaderPolicy.ID) as IDbDataParameter);
                    foreach (var benefData in param.BeneficiaryList)
                    {
                        rdr3 = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                                odal.CreateParameter("@table", SqlDbType.VarChar, "getTCBeneficiaryByID") as IDbDataParameter,
                                odal.CreateParameter("@nkey1", SqlDbType.Int, Convert.ToInt32(benefData.ID)) as IDbDataParameter);
                        while (rdr3.Read())
                        {
                            IsCheckBeneficiary = Convert.ToBoolean(rdr3["IsCheck"]);
                        }

                        if (IsCheckBeneficiary)
                        {
                            rdr4 = odal.ExecuteDataReader("usp_crud_certificate_benificiary", CommandType.StoredProcedure,
                             odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE") as IDbDataParameter,
                             odal.CreateParameter("@beneficiaryID", SqlDbType.Int, Convert.ToInt32(benefData.ID)) as IDbDataParameter,
                             //odal.CreateParameter("@customerID", SqlDbType.VarChar, benefData.CustomerID) as IDbDataParameter,
                             odal.CreateParameter("@beneficiaryName", SqlDbType.VarChar, benefData.Name) as IDbDataParameter,
                             odal.CreateParameter("@beneficiaryDOB", SqlDbType.DateTime, Convert.ToDateTime(benefData.DOB)) as IDbDataParameter,
                             odal.CreateParameter("@beneficiarySex", SqlDbType.VarChar, benefData.Gender) as IDbDataParameter,
                             odal.CreateParameter("@beneficiaryRelationship", SqlDbType.VarChar, benefData.Relation) as IDbDataParameter,
                             odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                             odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                        }
                        else
                        {
                            rdr4 = odal.ExecuteDataReader("usp_crud_certificate_benificiary", CommandType.StoredProcedure,
                             odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT") as IDbDataParameter,
                             odal.CreateParameter("@CertificateID", SqlDbType.Int, Convert.ToInt32(param.HeaderPolicy.ID)) as IDbDataParameter,
                             odal.CreateParameter("@customerID", SqlDbType.VarChar, benefData.CustomerID) as IDbDataParameter,
                             odal.CreateParameter("@beneficiaryName", SqlDbType.VarChar, benefData.Name) as IDbDataParameter,
                             odal.CreateParameter("@beneficiaryDOB", SqlDbType.DateTime, Convert.ToDateTime(benefData.DOB)) as IDbDataParameter,
                             odal.CreateParameter("@beneficiarySex", SqlDbType.VarChar, benefData.Gender) as IDbDataParameter,
                             odal.CreateParameter("@beneficiaryRelationship", SqlDbType.VarChar, benefData.Relation) as IDbDataParameter,
                             odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                             odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                        }
                    }
                }
                //TCertificate_Insured
                if (param.InsuredList != null)
                {
                    var arrayID = string.Join(",", param.InsuredList.Select(m => m.ID));
                    rdr5 = odal.ExecuteDataReader("usp_crud_certificate_insured", CommandType.StoredProcedure,
                         odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_INSURED_USING_NOT_IN") as IDbDataParameter,
                         odal.CreateParameter("@arrayID", SqlDbType.VarChar, arrayID) as IDbDataParameter,
                         odal.CreateParameter("@CertificateID", SqlDbType.VarChar, param.HeaderPolicy.ID) as IDbDataParameter);
                    foreach (var insurData in param.InsuredList)
                    {
                        if (insurData.ID == null || insurData.ID == "")
                        {
                            rdr5 = odal.ExecuteDataReader("usp_crud_certificate_insured", CommandType.StoredProcedure,
                             odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT") as IDbDataParameter,
                             odal.CreateParameter("@CertificateID", SqlDbType.Int, Convert.ToInt32(param.HeaderPolicy.ID)) as IDbDataParameter,
                             odal.CreateParameter("@customerID", SqlDbType.VarChar, insurData.CustomerID) as IDbDataParameter,
                             odal.CreateParameter("@insuredName", SqlDbType.VarChar, insurData.Name) as IDbDataParameter,
                             odal.CreateParameter("@insuredDOB", SqlDbType.DateTime, Convert.ToDateTime(insurData.DOB)) as IDbDataParameter,
                             odal.CreateParameter("@insuredSex", SqlDbType.VarChar, insurData.Gender) as IDbDataParameter,
                             odal.CreateParameter("@insuredRelation", SqlDbType.VarChar, insurData.Relation) as IDbDataParameter,
                             odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                             odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                        }
                        else
                        {
                            rdr5 = odal.ExecuteDataReader("usp_crud_certificate_insured", CommandType.StoredProcedure,
                                odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE") as IDbDataParameter,
                                odal.CreateParameter("@insuredID", SqlDbType.Int, Convert.ToInt32(insurData.ID)) as IDbDataParameter,
                                //odal.CreateParameter("@customerID", SqlDbType.VarChar, insurData.CustomerID) as IDbDataParameter,
                                odal.CreateParameter("@insuredName", SqlDbType.VarChar, insurData.Name) as IDbDataParameter,
                                odal.CreateParameter("@insuredDOB", SqlDbType.DateTime, Convert.ToDateTime(insurData.DOB)) as IDbDataParameter,
                                odal.CreateParameter("@insuredSex", SqlDbType.VarChar, insurData.Gender) as IDbDataParameter,
                                odal.CreateParameter("@insuredRelation", SqlDbType.VarChar, insurData.Relation) as IDbDataParameter,
                                odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                                odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);

                        }
                    }

                }

                //TActivity_History
                CreateActivity("POLICY", HistoryType.ModifyCertificate, Convert.ToInt32(param.HeaderPolicy.ID), "", param.UserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr1 = null;
                rdr2 = null;
                rdr3 = null;
                rdr4 = null;
                rdr5 = null;
            }
        }

        public List<RemarksViewModel> GetRemarksList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var dd = new RemarksViewModel();
            List<RemarksViewModel> list = new List<RemarksViewModel>();
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                    odal.CreateParameter("@table", SqlDbType.VarChar, "remarks_list") as IDbDataParameter,
                    odal.CreateParameter("@nkey1", SqlDbType.Int, pprm.ikey1) as IDbDataParameter);

                while (rdr.Read())
                {
                    dd = new RemarksViewModel();
                    dd.ID = Convert.ToInt32(rdr["ID"]);
                    dd.Date = Convert.ToDateTime(rdr["Date"]);
                    dd.By = rdr["By"].ToString();
                    dd.RemarksNotes = rdr["Remarks"].ToString();
                    dd.RemarksFileName = rdr["RemarksFileName"].ToString();
                    dd.RemarksFilePath = rdr["RemarksFilePath"].ToString();
                    list.Add(dd);

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public void CancelationPolicy(PolicyCancelationViewModel param)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr1;
            try
            {
                rdr1 = odal.ExecuteDataReader("usp_crud_certificate", CommandType.StoredProcedure,
                      odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE") as IDbDataParameter,
                      odal.CreateParameter("@certificateID", SqlDbType.Int, param.ID) as IDbDataParameter,
                      odal.CreateParameter("@certificateStatus", SqlDbType.VarChar, StatusPolicy.Cancel) as IDbDataParameter,
                      odal.CreateParameter("@cancelDate", SqlDbType.DateTime, param.CancelDate) as IDbDataParameter,
                      odal.CreateParameter("@cancelReason", SqlDbType.VarChar, param.CancelReason) as IDbDataParameter,
                      odal.CreateParameter("@cancelReasonOther", SqlDbType.VarChar, param.OtherReason) as IDbDataParameter,
                      odal.CreateParameter("@cancelNotes", SqlDbType.VarChar, param.Notes) as IDbDataParameter,
                      odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                      odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);

                CreateActivity("POLICY", HistoryType.CancelCertificate, param.ID, param.Notes, param.UserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr1 = null;
            }
        }

        public void AddRemarksPolicy(RemarksViewModel param)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            try
            {
                rdr = odal.ExecuteDataReader("usp_crud_remarks", CommandType.StoredProcedure,
                        odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT") as IDbDataParameter,
                        odal.CreateParameter("@certificateID", SqlDbType.Int, Convert.ToInt32(param.CertificateID)) as IDbDataParameter,
                        odal.CreateParameter("@remarksNotes", SqlDbType.VarChar, param.RemarksNotes) as IDbDataParameter,
                        odal.CreateParameter("@remarksFilePath", SqlDbType.VarChar, param.RemarksFilePath) as IDbDataParameter,
                        odal.CreateParameter("@remarksFileName", SqlDbType.VarChar, param.RemarksFileName) as IDbDataParameter,
                        odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                        odal.CreateParameter("@createdDate", SqlDbType.DateTime, param.Date) as IDbDataParameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr = null;
            }


        }

        public bool HasRenewed(cParams pprm) {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var dd = false;
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                       odal.CreateParameter("@table", SqlDbType.VarChar, "has_renewed") as IDbDataParameter,
                       odal.CreateParameter("@nkey1", SqlDbType.Int, pprm.ikey1) as IDbDataParameter);

                while (rdr.Read())
                {
                    dd = Convert.ToBoolean(rdr["IsCheck"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr = null;
            }
            return dd;
        }

        public string GetCertificateStatus(int id)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            string dd = "";
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                       odal.CreateParameter("@table", SqlDbType.VarChar, "get_certificate_status") as IDbDataParameter,
                       odal.CreateParameter("@nkey1", SqlDbType.Int, id) as IDbDataParameter);

                while (rdr.Read())
                { dd = rdr["CertificateStatus"].ToString(); }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr = null;
            }
            return dd;
        }

        public string Renewal(int ID, string userID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            string certificateNo = null;
            try
            {
                rdr = odal.ExecuteDataReader("usp_save_renewal", CommandType.StoredProcedure,
                      odal.CreateParameter("@certificateID", SqlDbType.Int, ID) as IDbDataParameter,
                      odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                      odal.CreateParameter("@createdBy", SqlDbType.VarChar, userID) as IDbDataParameter);
                while (rdr.Read())
                {
                    certificateNo = rdr["CertificateNo"].ToString();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return certificateNo;
        }
        #endregion

        #region CUSTOMER
        public List<TdCodeViewModel> GetTdCodeByCompanyID(string companyID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var dd = new TdCodeViewModel();
            List<TdCodeViewModel> list = new List<TdCodeViewModel>();
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_tdcode_byCompanyID") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, companyID) as IDbDataParameter);
                while (rdr.Read())
                {
                    dd = new TdCodeViewModel();
                    dd.CodeType = rdr["CodeType"].ToString();
                    dd.CodeID = rdr["CodeID"].ToString();
                    dd.CodeName = rdr["CodeName"].ToString();
                    list.Add(dd);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public HeaderCustomerMaintenanceViewModel GetHeaderCustomer(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new HeaderCustomerMaintenanceViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "header_customer") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.CustomerID = rdr["CustomerID"].ToString();
                    model.CustomerType = rdr["Type"].ToString();
                    model.FirstName = rdr["FirstName"].ToString();
                    model.LastName = rdr["LastName"].ToString();
                    model.DOB = Convert.ToDateTime(rdr["DOB"]);
                    model.Gender = rdr["Gender"].ToString();
                    model.Nationality = rdr["Nationality"].ToString();
                    model.Occupation = rdr["Occupation"].ToString();
                    model.IDType = rdr["IDCardType"].ToString();
                    model.IDNumber = rdr["IDCardNo"].ToString();
                    model.IDTypeName = rdr["IDTypeName"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<ContactAddress> GetContactDetailList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<ContactAddress>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "contact_detail_list_customer") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new ContactAddress
                    {
                        ID = rdr["ID"].ToString(),
                        ContactTypeID = rdr["ContactTypeID"].ToString(),
                        ContactType = rdr["ContactType"].ToString(),
                        ContactDetail = rdr["ContactDetail"].ToString(),
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<ContactAddress> GetAddressList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<ContactAddress>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "address_list_customer") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new ContactAddress
                    {
                        ID = rdr["ID"].ToString(),
                        ContactTypeID = rdr["ContactTypeID"].ToString(),
                        ContactType = rdr["ContactType"].ToString(),
                        Address = rdr["Address"].ToString(),
                        City = rdr["City"].ToString(),
                        Province = rdr["Province"].ToString(),
                        PostalCode = rdr["PostalCode"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }


        public void SaveCustomer(CustomerMaintenanceViewModel param)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr1, rdr2, rdr3, rdr4, rdr5, rdr6, rdr7, rdr8, rdr9, rdr10;
            try
            {
                if (param.HeaderCustomer.CustomerID == null || param.HeaderCustomer.CustomerID == "")
                {
                    rdr1 = odal.ExecuteDataReader("usp_crud_customer", CommandType.StoredProcedure,
                      odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT") as IDbDataParameter,
                      odal.CreateParameter("@customerType", SqlDbType.VarChar, param.HeaderCustomer.CustomerType) as IDbDataParameter,
                      odal.CreateParameter("@customerFirstName", SqlDbType.VarChar, param.HeaderCustomer.FirstName) as IDbDataParameter,
                      odal.CreateParameter("@customerLastName", SqlDbType.VarChar, param.HeaderCustomer.LastName) as IDbDataParameter,
                      odal.CreateParameter("@sex", SqlDbType.VarChar, param.HeaderCustomer.Gender) as IDbDataParameter,
                      odal.CreateParameter("@nationality", SqlDbType.VarChar, param.HeaderCustomer.Nationality) as IDbDataParameter,
                      odal.CreateParameter("@occupation", SqlDbType.VarChar, param.HeaderCustomer.Occupation) as IDbDataParameter,
                      odal.CreateParameter("@birthDate", SqlDbType.DateTime, param.HeaderCustomer.DOB) as IDbDataParameter,
                      odal.CreateParameter("@IDCardType", SqlDbType.VarChar, param.HeaderCustomer.IDType) as IDbDataParameter,
                      odal.CreateParameter("@IDCardNo", SqlDbType.VarChar, param.HeaderCustomer.IDNumber) as IDbDataParameter,
                      odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                      odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                    while (rdr1.Read())
                    {
                        param.HeaderCustomer.CustomerID = rdr1["CustomerID"].ToString();
                    }

                    //Contact Detail
                    if (param.ContactDetailList != null)
                    {
                        foreach (var contact in param.ContactDetailList)
                        {
                            rdr2 = odal.ExecuteDataReader("usp_crud_customer", CommandType.StoredProcedure,
                              odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT_CONTACT") as IDbDataParameter,
                              odal.CreateParameter("@customerID", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter,
                              odal.CreateParameter("@contactType", SqlDbType.VarChar, contact.ContactTypeID) as IDbDataParameter,
                              odal.CreateParameter("@contactDetail1", SqlDbType.VarChar, contact.ContactDetail) as IDbDataParameter,
                              odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                              odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                        }
                    }

                    //Address
                    if (param.AddressList != null)
                    {
                        foreach (var address in param.AddressList)
                        {
                            rdr3 = odal.ExecuteDataReader("usp_crud_customer", CommandType.StoredProcedure,
                              odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT_CONTACT") as IDbDataParameter,
                              odal.CreateParameter("@customerID", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter,
                              odal.CreateParameter("@contactType", SqlDbType.VarChar, address.ContactTypeID) as IDbDataParameter,
                              odal.CreateParameter("@contactDetail1", SqlDbType.VarChar, address.Address) as IDbDataParameter,
                              odal.CreateParameter("@contactDetail2", SqlDbType.VarChar, address.City) as IDbDataParameter,
                              odal.CreateParameter("@contactDetail3", SqlDbType.VarChar, address.Province) as IDbDataParameter,
                              odal.CreateParameter("@contactDetail4", SqlDbType.VarChar, address.PostalCode) as IDbDataParameter,
                              odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                              odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                        }
                    }
                }
                else
                {
                    rdr4 = odal.ExecuteDataReader("usp_crud_customer", CommandType.StoredProcedure,
                     odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE") as IDbDataParameter,
                     odal.CreateParameter("@customerID", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter,
                     odal.CreateParameter("@customerType", SqlDbType.VarChar, param.HeaderCustomer.CustomerType) as IDbDataParameter,
                     odal.CreateParameter("@customerFirstName", SqlDbType.VarChar, param.HeaderCustomer.FirstName) as IDbDataParameter,
                     odal.CreateParameter("@customerLastName", SqlDbType.VarChar, param.HeaderCustomer.LastName) as IDbDataParameter,
                     odal.CreateParameter("@sex", SqlDbType.VarChar, param.HeaderCustomer.Gender) as IDbDataParameter,
                     odal.CreateParameter("@nationality", SqlDbType.VarChar, param.HeaderCustomer.Nationality) as IDbDataParameter,
                     odal.CreateParameter("@occupation", SqlDbType.VarChar, param.HeaderCustomer.Occupation) as IDbDataParameter,
                     odal.CreateParameter("@birthDate", SqlDbType.DateTime, param.HeaderCustomer.DOB) as IDbDataParameter,
                     odal.CreateParameter("@IDCardType", SqlDbType.VarChar, param.HeaderCustomer.IDType) as IDbDataParameter,
                      odal.CreateParameter("@IDCardNo", SqlDbType.VarChar, param.HeaderCustomer.IDNumber) as IDbDataParameter,
                     odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                     odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);

                    //Contact Detail
                    if (param.ContactDetailList != null)
                    {
                        var arrayID = string.Join(",", param.ContactDetailList.Select(m => m.ID));
                        rdr5 = odal.ExecuteDataReader("usp_crud_customer", CommandType.StoredProcedure,
                             odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_CONTACT_USING_NOT_IN") as IDbDataParameter,
                             odal.CreateParameter("@arrayID", SqlDbType.VarChar, arrayID) as IDbDataParameter,
                             odal.CreateParameter("@customerID", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter,
                             odal.CreateParameter("@contactTypeDelete", SqlDbType.VarChar, "CONTACT_DETAIL") as IDbDataParameter);

                        foreach (var contact in param.ContactDetailList)
                        {
                            if (contact.ID == null || contact.ID == "")
                            {
                                rdr6 = odal.ExecuteDataReader("usp_crud_customer", CommandType.StoredProcedure,
                                 odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT_CONTACT") as IDbDataParameter,
                                 odal.CreateParameter("@customerID", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter,
                                 odal.CreateParameter("@contactType", SqlDbType.VarChar, contact.ContactTypeID) as IDbDataParameter,
                                 odal.CreateParameter("@contactDetail1", SqlDbType.VarChar, contact.ContactDetail) as IDbDataParameter,
                                 odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                                 odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                            }
                            else
                            {
                                rdr6 = odal.ExecuteDataReader("usp_crud_customer", CommandType.StoredProcedure,
                                 odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_CONTACT") as IDbDataParameter,
                                 odal.CreateParameter("@id", SqlDbType.VarChar, contact.ID) as IDbDataParameter,
                                 odal.CreateParameter("@customerID", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter,
                                 odal.CreateParameter("@contactType", SqlDbType.VarChar, contact.ContactTypeID) as IDbDataParameter,
                                 odal.CreateParameter("@contactDetail1", SqlDbType.VarChar, contact.ContactDetail) as IDbDataParameter,
                                 odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                                 odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                            }
                        }
                    }

                    //Address
                    if (param.AddressList != null)
                    {
                        var arrayID = string.Join(",", param.AddressList.Select(m => m.ID));
                        rdr7 = odal.ExecuteDataReader("usp_crud_customer", CommandType.StoredProcedure,
                             odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_CONTACT_USING_NOT_IN") as IDbDataParameter,
                             odal.CreateParameter("@arrayID", SqlDbType.VarChar, arrayID) as IDbDataParameter,
                             odal.CreateParameter("@customerID", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter,
                             odal.CreateParameter("@contactTypeDelete", SqlDbType.VarChar, "ADDRESS") as IDbDataParameter);

                        foreach (var address in param.AddressList)
                        {
                            if (address.ID == null || address.ID == "")
                            {
                                rdr8 = odal.ExecuteDataReader("usp_crud_customer", CommandType.StoredProcedure,
                                 odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT_CONTACT") as IDbDataParameter,
                                 odal.CreateParameter("@customerID", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter,
                                 odal.CreateParameter("@contactType", SqlDbType.VarChar, address.ContactTypeID) as IDbDataParameter,
                                 odal.CreateParameter("@contactDetail1", SqlDbType.VarChar, address.Address) as IDbDataParameter,
                                 odal.CreateParameter("@contactDetail2", SqlDbType.VarChar, address.City) as IDbDataParameter,
                                 odal.CreateParameter("@contactDetail3", SqlDbType.VarChar, address.Province) as IDbDataParameter,
                                 odal.CreateParameter("@contactDetail4", SqlDbType.VarChar, address.PostalCode) as IDbDataParameter,
                                 odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                                 odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                            }
                            else
                            {
                                rdr8 = odal.ExecuteDataReader("usp_crud_customer", CommandType.StoredProcedure,
                                 odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_CONTACT") as IDbDataParameter,
                                 odal.CreateParameter("@id", SqlDbType.VarChar, address.ID) as IDbDataParameter,
                                 odal.CreateParameter("@customerID", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter,
                                 odal.CreateParameter("@contactType", SqlDbType.VarChar, address.ContactTypeID) as IDbDataParameter,
                                 odal.CreateParameter("@contactDetail1", SqlDbType.VarChar, address.Address) as IDbDataParameter,
                                 odal.CreateParameter("@contactDetail2", SqlDbType.VarChar, address.City) as IDbDataParameter,
                                 odal.CreateParameter("@contactDetail3", SqlDbType.VarChar, address.Province) as IDbDataParameter,
                                 odal.CreateParameter("@contactDetail4", SqlDbType.VarChar, address.PostalCode) as IDbDataParameter,
                                 odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                                 odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                            }
                        }

                    }

                    //Update in certificate
                    //jika customer type insured
                    var IsCheck = false;
                    if (param.HeaderCustomer.CustomerType == CustomerType.Insured)
                    {
                        rdr9 = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                                odal.CreateParameter("@table", SqlDbType.VarChar, "CheckInsuredByCustomerID") as IDbDataParameter,
                                odal.CreateParameter("@skey1", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter);
                        while (rdr9.Read())
                        {
                            IsCheck = Convert.ToBoolean(rdr9["IsCheck"]);
                        }

                        if (IsCheck)
                        {
                            rdr10 = odal.ExecuteDataReader("usp_crud_certificate_insured", CommandType.StoredProcedure,
                                odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_BY_CUSTOMERID") as IDbDataParameter,
                                odal.CreateParameter("@insuredName", SqlDbType.VarChar, param.HeaderCustomer.FirstName + " " + param.HeaderCustomer.LastName) as IDbDataParameter,
                                odal.CreateParameter("@insuredDOB", SqlDbType.DateTime, Convert.ToDateTime(param.HeaderCustomer.DOB)) as IDbDataParameter,
                                odal.CreateParameter("@insuredSex", SqlDbType.VarChar, param.HeaderCustomer.Gender) as IDbDataParameter,
                                odal.CreateParameter("@customerID", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter
                                );
                        }
                    }
                    else if (param.HeaderCustomer.CustomerType == CustomerType.Beneficiary)
                    {
                        rdr9 = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                                odal.CreateParameter("@table", SqlDbType.VarChar, "CheckBeneficiaryByCustomerID") as IDbDataParameter,
                                odal.CreateParameter("@skey1", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter);
                        while (rdr9.Read())
                        {
                            IsCheck = Convert.ToBoolean(rdr9["IsCheck"]);
                        }

                        if (IsCheck)
                        {
                            rdr10 = odal.ExecuteDataReader("usp_crud_certificate_benificiary", CommandType.StoredProcedure,
                            odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_BY_CUSTOMERID") as IDbDataParameter,
                            odal.CreateParameter("@beneficiaryName", SqlDbType.VarChar, param.HeaderCustomer.FirstName + " " + param.HeaderCustomer.LastName) as IDbDataParameter,
                            odal.CreateParameter("@beneficiaryDOB", SqlDbType.DateTime, Convert.ToDateTime(param.HeaderCustomer.DOB)) as IDbDataParameter,
                            odal.CreateParameter("@beneficiarySex", SqlDbType.VarChar, param.HeaderCustomer.Gender) as IDbDataParameter,
                            odal.CreateParameter("@nationality", SqlDbType.VarChar, param.HeaderCustomer.Nationality) as IDbDataParameter,
                            odal.CreateParameter("@customerID", SqlDbType.VarChar, param.HeaderCustomer.CustomerID) as IDbDataParameter
                            );
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null; rdr1 = null; rdr2 = null; rdr3 = null; rdr4 = null;
                rdr5 = null; rdr6 = null; rdr7 = null; rdr8 = null;
            }
        }
        #endregion

        #region PAYMENT
        public IEnumerable<PaymentListViewModel> GetPaymentList(ParameterPaymentListViewModel param)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<PaymentListViewModel>();

            try
            {
                var minDate = new DateTime(1753, 1, 1);
                var maxDate = DateTime.MaxValue;
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_payment") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, param.CompanyID) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, param.PolicyNo != null ? param.PolicyNo : "") as IDbDataParameter,
                        odal.CreateParameter("@skey2", SqlDbType.VarChar, param.ApplicationNo != null ? param.ApplicationNo : "") as IDbDataParameter,
                        odal.CreateParameter("@skey3", SqlDbType.VarChar, param.PaymentType != null ? param.PaymentType : "") as IDbDataParameter,
                        odal.CreateParameter("@skey4", SqlDbType.VarChar, param.ProgressStatus != null ? param.ProgressStatus : "") as IDbDataParameter,
                        odal.CreateParameter("@dkey1", SqlDbType.DateTime, param.PaymentDate != null ? param.PaymentDate : minDate) as IDbDataParameter,
                        odal.CreateParameter("@dkey2", SqlDbType.DateTime, param.PaymentDate != null ? param.PaymentDate : maxDate) as IDbDataParameter,
                        odal.CreateParameter("@pageNum", SqlDbType.Int, param.Page) as IDbDataParameter,
                        odal.CreateParameter("@pageSize", SqlDbType.Int, param.PerPage) as IDbDataParameter,
                        odal.CreateParameter("@prop", SqlDbType.VarChar, param.Prop != null ? param.Prop : "") as IDbDataParameter,
                        odal.CreateParameter("@sort", SqlDbType.VarChar, param.Sort != null ? param.Sort : "") as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new PaymentListViewModel
                    {
                        ID = Convert.ToInt64(rdr["ID"]),
                        PaymentNo = rdr["PaymentNo"].ToString(),
                        PolicyNo = rdr["PolicyNo"].ToString(),
                        ApplicationNo = rdr["ApplicationNo"].ToString(),
                        ProgressStatus = rdr["ProgressStatus"].ToString(),
                        PaymentStatus = rdr["PaymentStatus"].ToString(),
                        OwnerName = rdr["PolicyOwnerName"].ToString(),
                        PaymentType = rdr["PaymentType"].ToString(),
                        Source = rdr["Source"].ToString(),
                        PaymentDate = helperLib.GetDate(rdr["PaymentDate"]),
                        Length = Convert.ToInt32(rdr["Length"])
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public HeaderPaymentMaintenanceViewModel GetHeaderPayment(cParams pprm) {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new HeaderPaymentMaintenanceViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "header_payment") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.Int, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.PaymentID = rdr["PaymentID"].ToString();
                    model.PaymentNo = rdr["PaymentNo"].ToString();
                    model.PolicyNo = rdr["PolicyNo"].ToString();
                    model.PaymentSource = rdr["PaymentSource"].ToString();
                    model.PaymentType = rdr["PaymentType"].ToString();
                    model.ApplicationNo = rdr["ApplicationNo"].ToString();
                    model.TransType = rdr["TransType"].ToString();
                    model.OwnerName = rdr["OwnerName"].ToString();
                    model.PaymentStatus = rdr["PaymentStatus"].ToString();
                    model.ProgressStatus = rdr["ProgressStatus"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;

        }

        public DetailPaymentMaintenanceViewModel GetDetailPayment(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new DetailPaymentMaintenanceViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "payment_detail") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.Int, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.PaymentDate = Convert.ToDateTime(rdr["PaymentDate"]);
                    model.EffectiveDate = Convert.ToDateTime(rdr["EffectiveDate"]);
                    model.DueDate = rdr.IsDBNull(2) ? (DateTime?)null : helperLib.GetDate(rdr["DueDate"]);
                    model.ApprovedBy = rdr["ApprovedBy"].ToString();
                    model.ApprovedDate = rdr.IsDBNull(4) ? (DateTime?)null : helperLib.GetDate(rdr["ApprovedDate"]);
                    model.UploadedBy = rdr["UploadedBy"].ToString();
                    model.UploadDate = Convert.ToDateTime(rdr["UploadDate"]);
                    model.Currency = rdr["Currency"].ToString();
                    model.PaymentAmount = Convert.ToDecimal(rdr["PaymentAmount"]);
                    model.RefferenceNo = rdr["RefferenceNo"].ToString();
                    model.TrxRefno = rdr["trx_refno"].ToString();
                    model.TrxDate = rdr["trx_date"].ToString();
                    model.TrxTime = rdr["trx_time"].ToString();
                    model.Merchant = rdr["merchant"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;

        }

        public List<HistoryPaymentMaintenanceViewModel> GetHistoryPayment(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<HistoryPaymentMaintenanceViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "payment_history") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.Int, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new HistoryPaymentMaintenanceViewModel
                    {
                        HistoryDate = helperLib.GetDate(rdr["HistoryDate"]),
                        HistoryType = rdr["HistoryType"].ToString(),
                        HistoryBy = rdr["HistoryBy"].ToString(),
                        RefferenceNo = rdr["RefferenceNo"].ToString(),
                        PaymentInfo = rdr["PaymentInfo"].ToString(),
                        Notes = rdr["Notes"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;

        }

        public void SavePayment(PaymentMaintenanceViewModel param) 
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var valueNull = DBNull.Value;

            try
            {
                if (param.HeaderPayment.PaymentID == null || param.HeaderPayment.PaymentID == "")
                {
                    rdr = odal.ExecuteDataReader("usp_crud_payment", CommandType.StoredProcedure,
                      odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT") as IDbDataParameter,
                      odal.CreateParameter("@reffernceNo", SqlDbType.VarChar, param.DetailPayment.RefferenceNo == null ? (object)DBNull.Value : param.DetailPayment.RefferenceNo) as IDbDataParameter,
                      odal.CreateParameter("@paymentSource", SqlDbType.VarChar, param.HeaderPayment.PaymentSource == null ? (object)DBNull.Value : param.HeaderPayment.PaymentSource) as IDbDataParameter,
                      odal.CreateParameter("@transType", SqlDbType.VarChar, param.HeaderPayment.TransType) as IDbDataParameter,
                      odal.CreateParameter("@paymentDate", SqlDbType.DateTime, param.DetailPayment.PaymentDate) as IDbDataParameter,
                      odal.CreateParameter("@effectiveDate", SqlDbType.DateTime, param.DetailPayment.EffectiveDate) as IDbDataParameter,
                      odal.CreateParameter("@paymentType", SqlDbType.VarChar, param.HeaderPayment.PaymentType == null ? (object)DBNull.Value : param.HeaderPayment.PaymentType) as IDbDataParameter,
                      odal.CreateParameter("@policyID", SqlDbType.VarChar, param.HeaderPayment.PolicyNo == null ? (object)DBNull.Value : param.HeaderPayment.PolicyNo) as IDbDataParameter,
                      odal.CreateParameter("@applicationNo", SqlDbType.VarChar, param.HeaderPayment.ApplicationNo == null ? (object)DBNull.Value : param.HeaderPayment.ApplicationNo) as IDbDataParameter,
                      odal.CreateParameter("@dueDate", SqlDbType.DateTime, param.DetailPayment.DueDate == null ? (object)DBNull.Value : param.DetailPayment.DueDate.Value) as IDbDataParameter,
                      odal.CreateParameter("@policyOwnerName", SqlDbType.VarChar, param.HeaderPayment.OwnerName == null ? (object)DBNull.Value : param.HeaderPayment.OwnerName) as IDbDataParameter,
                      odal.CreateParameter("@paymentAmount", SqlDbType.Decimal, param.DetailPayment.PaymentAmount) as IDbDataParameter,
                      odal.CreateParameter("@paymentStatus", SqlDbType.VarChar, param.HeaderPayment.PaymentStatus == null ? (object)DBNull.Value : param.HeaderPayment.PaymentStatus) as IDbDataParameter,
                      //odal.CreateParameter("@progressStatus", SqlDbType.VarChar, null) as IDbDataParameter,
                      odal.CreateParameter("@approvedDate", SqlDbType.DateTime, param.DetailPayment.ApprovedDate == null ? (object)DBNull.Value : param.DetailPayment.ApprovedDate.Value) as IDbDataParameter,
                      odal.CreateParameter("@approvedBy", SqlDbType.VarChar, param.DetailPayment.ApprovedBy == null ? (object)DBNull.Value : param.DetailPayment.ApprovedBy) as IDbDataParameter,
                      odal.CreateParameter("@acq_id", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@term_id", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@merchant", SqlDbType.VarChar, param.DetailPayment.Merchant) as IDbDataParameter,
                      odal.CreateParameter("@trx_date", SqlDbType.VarChar, param.DetailPayment.TrxDate) as IDbDataParameter,
                      odal.CreateParameter("@trx_time", SqlDbType.VarChar, param.DetailPayment.TrxTime) as IDbDataParameter,
                      odal.CreateParameter("@trx_refno", SqlDbType.VarChar, param.DetailPayment.TrxRefno) as IDbDataParameter,
                      //odal.CreateParameter("@bankCode", SqlDbType.VarChar, null) as IDbDataParameter,
                      odal.CreateParameter("@currencyID", SqlDbType.VarChar, param.DetailPayment.Currency) as IDbDataParameter,
                      odal.CreateParameter("@exchangeRate", SqlDbType.Decimal, DBNull.Value) as IDbDataParameter,
                      //odal.CreateParameter("@cardType", SqlDbType.VarChar, null) as IDbDataParameter,
                      odal.CreateParameter("@CCNumber", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@CCExpiredMonth", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@CCExpiredYear", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@CCHolderName", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@approvalCode", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@approvalDate", SqlDbType.DateTime, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@chequeType", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@chequeNumber", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@chequeDate", SqlDbType.DateTime, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@depositDate", SqlDbType.DateTime, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@bankAccount", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                      odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                      odal.CreateParameter("@remarks", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter
                      );

                }

                else {
                    rdr = odal.ExecuteDataReader("usp_crud_payment", CommandType.StoredProcedure,
                      odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE") as IDbDataParameter,
                      odal.CreateParameter("@paymentID", SqlDbType.BigInt, Convert.ToInt32(param.HeaderPayment.PaymentID)) as IDbDataParameter,
                      odal.CreateParameter("@reffernceNo", SqlDbType.VarChar, param.DetailPayment.RefferenceNo == null ? (object)DBNull.Value : param.DetailPayment.RefferenceNo) as IDbDataParameter,
                      odal.CreateParameter("@paymentSource", SqlDbType.VarChar, param.HeaderPayment.PaymentSource == null ? (object)DBNull.Value : param.HeaderPayment.PaymentSource) as IDbDataParameter,
                      odal.CreateParameter("@transType", SqlDbType.VarChar, param.HeaderPayment.TransType == null ? (object)DBNull.Value : param.HeaderPayment.TransType) as IDbDataParameter,
                      odal.CreateParameter("@paymentDate", SqlDbType.DateTime,param.DetailPayment.PaymentDate) as IDbDataParameter,
                      odal.CreateParameter("@effectiveDate", SqlDbType.DateTime, param.DetailPayment.EffectiveDate) as IDbDataParameter,
                      odal.CreateParameter("@paymentType", SqlDbType.VarChar, param.HeaderPayment.PaymentType == null ? (object)DBNull.Value : param.HeaderPayment.PaymentType) as IDbDataParameter,
                      odal.CreateParameter("@policyID", SqlDbType.VarChar, param.HeaderPayment.PolicyNo == null ? (object)DBNull.Value : param.HeaderPayment.PolicyNo) as IDbDataParameter,
                      odal.CreateParameter("@applicationNo", SqlDbType.VarChar, param.HeaderPayment.ApplicationNo == null ? (object)DBNull.Value : param.HeaderPayment.ApplicationNo) as IDbDataParameter,
                      odal.CreateParameter("@dueDate", SqlDbType.DateTime, param.DetailPayment.DueDate == null ? (object)DBNull.Value : param.DetailPayment.DueDate.Value) as IDbDataParameter,
                      odal.CreateParameter("@policyOwnerName", SqlDbType.VarChar, param.HeaderPayment.OwnerName == null ? (object)DBNull.Value : param.HeaderPayment.OwnerName) as IDbDataParameter,
                      odal.CreateParameter("@paymentAmount", SqlDbType.Decimal, param.DetailPayment.PaymentAmount) as IDbDataParameter,
                      odal.CreateParameter("@paymentStatus", SqlDbType.VarChar, param.HeaderPayment.PaymentStatus == null ? (object)DBNull.Value : param.HeaderPayment.PaymentStatus) as IDbDataParameter,
                      //odal.CreateParameter("@progressStatus", SqlDbType.VarChar, null) as IDbDataParameter,
                      odal.CreateParameter("@approvedDate", SqlDbType.DateTime, param.DetailPayment.ApprovedDate == null ? (object)DBNull.Value : param.DetailPayment.ApprovedDate.Value) as IDbDataParameter,
                      odal.CreateParameter("@approvedBy", SqlDbType.VarChar, param.DetailPayment.ApprovedBy) as IDbDataParameter,
                      odal.CreateParameter("@acq_id", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@term_id", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@merchant", SqlDbType.VarChar, param.DetailPayment.Merchant) as IDbDataParameter,
                      odal.CreateParameter("@trx_date", SqlDbType.VarChar, param.DetailPayment.TrxDate) as IDbDataParameter,
                      odal.CreateParameter("@trx_time", SqlDbType.VarChar, param.DetailPayment.TrxTime) as IDbDataParameter,
                      odal.CreateParameter("@trx_refno", SqlDbType.VarChar, param.DetailPayment.TrxRefno) as IDbDataParameter,
                      //odal.CreateParameter("@bankCode", SqlDbType.VarChar, null) as IDbDataParameter,
                      odal.CreateParameter("@currencyID", SqlDbType.VarChar, param.DetailPayment.Currency) as IDbDataParameter,
                      odal.CreateParameter("@exchangeRate", SqlDbType.Decimal, DBNull.Value) as IDbDataParameter,
                      //odal.CreateParameter("@cardType", SqlDbType.VarChar, null) as IDbDataParameter,
                      odal.CreateParameter("@CCNumber", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@CCExpiredMonth", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@CCExpiredYear", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@CCHolderName", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@approvalCode", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@approvalDate", SqlDbType.DateTime, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@chequeType", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@chequeNumber", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@chequeDate", SqlDbType.DateTime, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@depositDate", SqlDbType.DateTime, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@bankAccount", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter,
                      odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                      odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                      odal.CreateParameter("@remarks", SqlDbType.VarChar, null) as IDbDataParameter
                      );
                }

            }
            catch(Exception ex) {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
        }
        public void PaymentApproved(int ID, string userID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            var model = new List<DropDownViewModel>();
            IDataReader rdr1;
            try
            {
                rdr1 = odal.ExecuteDataReader("usp_crud_payment", CommandType.StoredProcedure,
                      odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_STATUS") as IDbDataParameter,
                      odal.CreateParameter("@progressStatus", SqlDbType.VarChar, "A") as IDbDataParameter,
                      odal.CreateParameter("@updatedBy", SqlDbType.VarChar, userID) as IDbDataParameter,
                      odal.CreateParameter("@paymentID", SqlDbType.Int, ID) as IDbDataParameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr1 = null;
            }
        }

        public List<DropDownViewModel> GetAppNoList(string CompanyID) 
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            var model = new List<DropDownViewModel>();
            IDataReader rdr1;
            try
            {
                rdr1 = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "appnum_list") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, CompanyID) as IDbDataParameter);
                while (rdr1.Read())
                {
                    model.Add(new DropDownViewModel {Id = rdr1["CertificateNo"].ToString() });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr1 = null;
            }
            return model;
        }

        public List<DropDownViewModel> GetAppNoClaimList(string CompanyID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            var model = new List<DropDownViewModel>();
            IDataReader rdr1;
            try
            {
                rdr1 = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "appnum_claim_list") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, CompanyID) as IDbDataParameter);
                while (rdr1.Read())
                {
                    model.Add(new DropDownViewModel { Id = rdr1["CertificateNo"].ToString() });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr1 = null;
            }
            return model;
        }

        public DataPaymentByAppNoViewModel GetOwnerNameByAppNo(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            var model = new DataPaymentByAppNoViewModel() ; 
            IDataReader rdr1;
            try
            {
                rdr1 = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "owner_name_by_appnum") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                      odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter);
                while (rdr1.Read())
                {
                    model.OwnerName = rdr1["OwnerName"].ToString();
                    model.EffectiveDate = helperLib.GetDate(rdr1["EffectiveDate"]);
                    model.PaymentAmount = Convert.ToDecimal(rdr1["PaymentAmount"]);
                    model.Currency = rdr1["Currency"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr1 = null;
            }
            return model;
        }
        #endregion

        #region REPORT
        public List<PolicyReportViewModel> GetPolicyReport(ParameterPolicyReportViewModel param)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<PolicyReportViewModel>();

            try
            {
                var minDate = new DateTime(1753, 1, 1);
                var maxDate = DateTime.MaxValue;
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "report_policy") as IDbDataParameter,
                        odal.CreateParameter("@dkey1", SqlDbType.DateTime, param.EffectiveDateFrom != null ? param.EffectiveDateFrom.Value.Date : minDate) as IDbDataParameter,
                        odal.CreateParameter("@dkey2", SqlDbType.DateTime, param.EffectiveDateTo != null ? param.EffectiveDateTo.Value.Date : Convert.ToDateTime(DateTime.MaxValue.Date)) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, param.PlanCode != null ? param.PlanCode : "") as IDbDataParameter,
                        odal.CreateParameter("@skey2", SqlDbType.VarChar, param.ProductCode != null ? param.ProductCode : "") as IDbDataParameter,
                        odal.CreateParameter("@skey3", SqlDbType.VarChar, param.CertificateStatus != null ? param.CertificateStatus : "") as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new PolicyReportViewModel
                    {
                        CIFNumber = rdr["CIFNumber"].ToString(),
                        CertificateNumber = rdr["CertificateNumber"].ToString(),
                        EffectiveDate = helperLib.GetDate(rdr["EffectiveDate"]),
                        ProductName = rdr["ProductName"].ToString(),
                        PlanCode = rdr["PlanCode"].ToString(),
                        PlanName = rdr["PlanName"].ToString(),
                        OwnerName = rdr["OwnerName"].ToString(),
                        OwnerDOB = helperLib.GetDate(rdr["OwnerDOB"]),
                        OwnerAge = rdr["OwnerAge"].ToString(),
                        NIK = rdr["NIK"].ToString(),
                        Gender = rdr["Gender"].ToString(),
                        Occupation = rdr["Occupation"].ToString(),
                        Religion = rdr["Religion"].ToString(),
                        Address = rdr["Address"].ToString(),
                        KotaDomisili = rdr["KotaDomisili"].ToString(),
                        MobileNumber = rdr["MobileNumber"].ToString(),
                        Email = rdr["Email"].ToString(),
                        CustomerName = rdr["CustomerName"].ToString(),
                        PlaceOfBirth = rdr["PlaceOfBirth"].ToString(),
                        DateOfBirth = helperLib.GetDate(rdr["DateOfBirth"]),
                        InsuredAge = rdr["InsuredAge"].ToString(),
                        InsuredRelation = rdr["InsuredRelation"].ToString(),
                        HardCopyPolicy = rdr["HardCopyPolicy"].ToString(),
                        DeliveryFee = rdr.IsDBNull(23) ? 0 : Convert.ToDecimal(rdr["DeliveryFee"]),
                        Premium = rdr.IsDBNull(24) ? 0 : Convert.ToDecimal(rdr["Premium"]),
                        TotalPremium = rdr.IsDBNull(25) ? 0 : Convert.ToDecimal(rdr["TotalPremium"]),
                        OriginalPartnerComission = rdr.IsDBNull(26) ? 0 : Convert.ToDecimal(rdr["OriginalPartnerComission"]),
                        ActualPartnerComission = rdr.IsDBNull(27) ? 0 : Convert.ToDecimal(rdr["ActualPartnerComission"]),
                        PartnerComission = rdr.IsDBNull(28) ? 0 : Convert.ToDecimal(rdr["PartnerComission"]),
                        PaymentSource = rdr["PaymentSource"].ToString(),
                        SumAssured = rdr.IsDBNull(30) ? 0 : Convert.ToDecimal(rdr["SumAssured"]),
                        Hospital = rdr.IsDBNull(31) ? 0 : Convert.ToDecimal(rdr["Hospital"]),
                        ADB = rdr.IsDBNull(32) ? 0 : Convert.ToDecimal(rdr["ADB"]),
                        TPD = rdr.IsDBNull(33) ? 0 : Convert.ToDecimal(rdr["TPD"]),
                        ADBMotorcycle = rdr.IsDBNull(34) ? 0 : Convert.ToDecimal(rdr["ADBMotorcycle"]),
                        DeathTerrorism = rdr.IsDBNull(35) ? 0 : Convert.ToDecimal(rdr["DeathTerrorism"]),
                        SumAssuredAdditional = rdr.IsDBNull(36) ? 0 : Convert.ToDecimal(rdr["SumAssuredAdditional"]),
                        HospitalLimit = rdr["HospitalLimit"].ToString(),
                        BeneficiaryName = rdr["BeneficiaryName"].ToString(),
                        BeneficiaryRelationship = rdr["BeneficiaryRelationship"].ToString(),
                        BeneficiaryDOB = rdr.IsDBNull(40) ? (DateTime?)null : helperLib.GetDate(rdr["BeneficiaryDOB"]),
                        CoverageDuration = rdr["CoverageDuration"].ToString(),
                        MaturityDate = helperLib.GetDate(rdr["MaturityDate"]),
                        CertificateStatus = rdr["CertificateStatus"].ToString(),
                        CancelDate = rdr.IsDBNull(44) ? (DateTime?)null : helperLib.GetDate(rdr["CancelDate"]),
                        CancelRemark = rdr["CancelRemark"].ToString(),
                        PartnerName = rdr["PartnerName"].ToString(),
                        DistributionChannel = rdr["DistributionChannel"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }
        public TparameterViewModel GetReportServiceURL()
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new TparameterViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "get_reportserviceurl") as IDbDataParameter);
                while (rdr.Read())
                {
                    model.ParameterID = rdr["ParameterID"].ToString();
                    model.ParameterValue = rdr["ListValueName"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public TparameterViewModel GetReportServiceUser()
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new TparameterViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "get_reportserviceuser") as IDbDataParameter);
                while (rdr.Read())
                {
                    model.ParameterID = rdr["ParameterID"].ToString();
                    model.ParameterValue = rdr["ListValueName"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public TparameterViewModel GetReportServicePassword()
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new TparameterViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "get_reportservicepassword") as IDbDataParameter);
                while (rdr.Read())
                {
                    model.ParameterID = rdr["ParameterID"].ToString();
                    model.ParameterValue = rdr["ListValueName"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public TparameterViewModel GetFolderReportPolicy()
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new TparameterViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "get_folderrreportpolicy") as IDbDataParameter);
                while (rdr.Read())
                {
                    model.ParameterID = rdr["ParameterID"].ToString();
                    model.ParameterValue = rdr["ListValueName"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }
        #endregion

        #region Billing
        public IEnumerable<BillingListViewModel> GetBillingList(ParameterListBillingViewModel param) {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<BillingListViewModel>();

            try
            {
                var minDate = new DateTime(1753, 1, 1);
                var maxDate = DateTime.MaxValue;
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_bill") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, param.CompanyID) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, param.PolicyNo != null ? param.PolicyNo : "") as IDbDataParameter,
                        odal.CreateParameter("@skey2", SqlDbType.VarChar, param.ApplicationNo != null ? param.ApplicationNo : "") as IDbDataParameter,
                        odal.CreateParameter("@skey3", SqlDbType.VarChar, param.BillingType != null ? param.BillingType : "") as IDbDataParameter,
                        odal.CreateParameter("@skey4", SqlDbType.VarChar, param.Status != null ? param.Status : "") as IDbDataParameter,
                        odal.CreateParameter("@dkey1", SqlDbType.DateTime, param.DueDate != null ? param.DueDate : minDate) as IDbDataParameter,
                        odal.CreateParameter("@dkey2", SqlDbType.DateTime, param.DueDate != null ? param.DueDate : maxDate) as IDbDataParameter,
                        odal.CreateParameter("@pageNum", SqlDbType.Int, param.Page) as IDbDataParameter,
                        odal.CreateParameter("@pageSize", SqlDbType.Int, param.PerPage) as IDbDataParameter,
                        odal.CreateParameter("@prop", SqlDbType.VarChar, param.Prop != null ? param.Prop : "") as IDbDataParameter,
                        odal.CreateParameter("@sort", SqlDbType.VarChar, param.Sort != null ? param.Sort : "") as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new BillingListViewModel
                    {
                        BillID = Convert.ToInt32(rdr["BillID"]),
                        BillNo = rdr["BillNo"].ToString(),
                        Duedate = helperLib.GetDate(rdr["DueDate"]),
                        PolicyNo = rdr["PolicyNo"].ToString(),
                        ApplicationNo = rdr["ApplicationNo"].ToString(),
                        StatusCode = rdr["StatusCode"].ToString(),
                        Status = rdr["Status"].ToString(),
                        TypeCode = rdr["TypeCode"].ToString(),
                        Type = rdr["Type"].ToString(),
                        OwnerName = rdr["OwnerName"].ToString(),
                        Source = rdr["Source"].ToString(),
                        Length = Convert.ToInt32(rdr["Length"])
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;

        }
        public List<DropDownViewModel> GetBillingStatusBill(string CompanyID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var list = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_bill_status") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, CompanyID) as IDbDataParameter);
                while (rdr.Read())
                {
                    list.Add(new DropDownViewModel
                    {
                        Id = rdr["CodeID"].ToString(),
                        Value = rdr["CodeName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }
        public List<DropDownViewModel> GetBillingTypeBill(string CompanyID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var list = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_bill_type") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, CompanyID) as IDbDataParameter);
                while (rdr.Read())
                {
                    list.Add(new DropDownViewModel
                    {
                        Id = rdr["CodeID"].ToString(),
                        Value = rdr["CodeName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return list;
        }

        public HeaderBillingMaintenanceViewModel GetHeaderBilling(cParams pprm) {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new HeaderBillingMaintenanceViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "header_billing") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                      odal.CreateParameter("@nkey1", SqlDbType.VarChar, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.BillingID = rdr["BillingID"].ToString();
                    model.BillingNo = rdr["BillingNo"].ToString();
                    model.PolicyNo = rdr["PolicyNo"].ToString();
                    model.BillingType = rdr["BillingType"].ToString();
                    model.BillingAmount = Convert.ToDecimal(rdr["BillingAmount"]);
                    model.ApplicationNo = rdr["ApplicationNo"].ToString();
                    model.BillingMode = rdr["BillingMode"].ToString();
                    model.BillingStatus = rdr["BillingStatus"].ToString();
                    model.OwnerName = rdr["PolicyOwnerName"].ToString();
                }

            }
            catch (Exception ex) { throw ex; }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;

        }

        public DetailBillingMaintenanceViewModel GetDetailBilling(cParams pprm) {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new DetailBillingMaintenanceViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "detail_billing") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                      odal.CreateParameter("@nkey1", SqlDbType.VarChar, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.BillingDate = helperLib.GetDate(rdr["BillingDate"]);
                    model.EffectiveDate = helperLib.GetDate(rdr["EffectiveDate"]);
                    model.DueDate = helperLib.GetDate(rdr["DueDate"]);
                    model.LastBillingDate = helperLib.GetDate(rdr["LastBillingDate"]);
                    model.LastPaymentDate = helperLib.GetDate(rdr["LastPaymentDate"]);
                    model.CreditCardType = rdr["CreditCardType"].ToString();
                    model.CreditCardNumber = rdr["CreditCardNumber"].ToString();
                    model.CreditCardHolderName = rdr["CreditCardHolderName"].ToString();
                    model.BankAccountNumber = rdr["BankAccountNumber"].ToString();
                    model.BankAccountHolderName = rdr["BankAccountHolderName"].ToString();
                    model.BankCode = rdr["BankCode"].ToString();
                    model.BankBranchCode = rdr["BankBranchCode"].ToString();
                    model.Currency = rdr["Currency"].ToString();
                }
            }
            catch (Exception ex) { throw ex; }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;

        }

        public void SaveBilling(BillingMaintenanceViewModel param) {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;

            try {
                
                    rdr = odal.ExecuteDataReader("usp_crud_bill", CommandType.StoredProcedure,
                          odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE") as IDbDataParameter,
                          odal.CreateParameter("@billID", SqlDbType.BigInt, Convert.ToInt64(param.HeaderBilling.BillingID)) as IDbDataParameter,
                          odal.CreateParameter("@dueDate", SqlDbType.DateTime, param.DetailBilling.DueDate == null ? (object)DBNull.Value : param.DetailBilling.DueDate.Value) as IDbDataParameter,
                          odal.CreateParameter("@effectiveDate", SqlDbType.DateTime, param.DetailBilling.EffectiveDate) as IDbDataParameter,
                          odal.CreateParameter("@billType", SqlDbType.VarChar, param.HeaderBilling.BillingType) as IDbDataParameter,
                          odal.CreateParameter("@policyID", SqlDbType.VarChar, param.HeaderBilling.PolicyNo == null ? (object)DBNull.Value : param.HeaderBilling.PolicyNo) as IDbDataParameter,
                          odal.CreateParameter("@applicationNo", SqlDbType.VarChar, param.HeaderBilling.ApplicationNo == null ? (object)DBNull.Value : param.HeaderBilling.ApplicationNo) as IDbDataParameter,
                          odal.CreateParameter("@policyOwnerName", SqlDbType.VarChar, param.HeaderBilling.OwnerName == null ? (object)DBNull.Value : param.HeaderBilling.OwnerName) as IDbDataParameter,
                          odal.CreateParameter("@billAmount", SqlDbType.Float, param.HeaderBilling.BillingAmount) as IDbDataParameter,
                          //odal.CreateParameter("@billStatus", SqlDbType.VarChar, param.HeaderBilling.BillingAmount) as IDbDataParameter,
                          odal.CreateParameter("@bankBranchCode", SqlDbType.VarChar, param.DetailBilling.BankBranchCode == null ? (object)DBNull.Value : param.DetailBilling.BankBranchCode) as IDbDataParameter,
                          odal.CreateParameter("@CCHolderName", SqlDbType.VarChar, param.DetailBilling.CreditCardHolderName == null ? (object)DBNull.Value : param.DetailBilling.CreditCardHolderName) as IDbDataParameter,
                          odal.CreateParameter("@bankAccountHolderName", SqlDbType.VarChar, param.DetailBilling.BankAccountHolderName == null ? (object)DBNull.Value : param.DetailBilling.BankAccountHolderName) as IDbDataParameter,
                          odal.CreateParameter("@bankCode", SqlDbType.VarChar, param.DetailBilling.BankCode) as IDbDataParameter,
                          odal.CreateParameter("@currencyID", SqlDbType.VarChar, param.DetailBilling.Currency) as IDbDataParameter,
                          //odal.CreateParameter("@exchangeRate", SqlDbType.Float, param.DetailBilling.Currency) as IDbDataParameter,
                          odal.CreateParameter("@cardType", SqlDbType.VarChar, param.DetailBilling.CreditCardType) as IDbDataParameter,
                          odal.CreateParameter("@CCNumber", SqlDbType.VarChar, param.DetailBilling.CreditCardNumber) as IDbDataParameter,
                          odal.CreateParameter("@bankAccount", SqlDbType.VarChar, param.DetailBilling.BankAccountNumber) as IDbDataParameter,
                          odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                          odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                          odal.CreateParameter("@remarks", SqlDbType.VarChar, DBNull.Value) as IDbDataParameter
                          );
                
            }
            catch (Exception ex) { throw ex; }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public void BillingApproved(int ID, string userID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr1;
            try
            {
                rdr1 = odal.ExecuteDataReader("usp_crud_bill", CommandType.StoredProcedure,
                      odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_STATUS_APPROVE") as IDbDataParameter,
                      odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                      odal.CreateParameter("@updatedBy", SqlDbType.VarChar, userID) as IDbDataParameter,
                      odal.CreateParameter("@billID", SqlDbType.BigInt, ID) as IDbDataParameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr1 = null;
            }
        }
        #endregion

        #region Claim
        public List<DropDownViewModel> GetClaimTypeList(string companyID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_claimtype") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, companyID) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new DropDownViewModel
                    {
                        Id = rdr["CodeID"].ToString(),
                        Value = rdr["CodeName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<DropDownViewModel> GetClaimStatusList(string companyID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_claimstatus") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, companyID) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new DropDownViewModel
                    {
                        Id = rdr["CodeID"].ToString(),
                        Value = rdr["CodeName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<DropDownViewModel> GetDiagnosisCodeList(string companyID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_diagnosiscode") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, companyID) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new DropDownViewModel
                    {
                        Id = rdr["CodeID"].ToString(),
                        Value = rdr["CodeName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<DropDownViewModel> GetHospitalCodeList(string companyID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_hospitalcode") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, companyID) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new DropDownViewModel
                    {
                        Id = rdr["CodeID"].ToString(),
                        Value = rdr["CodeName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<DropDownViewModel> GetCuaseOfDeathList(string companyID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_causeofdeath") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, companyID) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new DropDownViewModel
                    {
                        Id = rdr["CodeID"].ToString(),
                        Value = rdr["CodeName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<DropDownViewModel> GetPlanType(string companyID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_plantype") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, companyID) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new DropDownViewModel
                    {
                        Id = rdr["PlanCode"].ToString(),
                        Value = rdr["PlanCode"].ToString() + " - " + rdr["PlanName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<DropDownViewModel> GetPlanTypeByApplicationNo(string companyID, string applicationNo)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<DropDownViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                      odal.CreateParameter("@table", SqlDbType.VarChar, "list_plantype_byApplicationNo") as IDbDataParameter,
                      odal.CreateParameter("@CompanyID", SqlDbType.VarChar, companyID) as IDbDataParameter,
                      odal.CreateParameter("@skey1", SqlDbType.VarChar, applicationNo) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new DropDownViewModel
                    {
                        Id = rdr["PlanCode"].ToString(),
                        Value = rdr["PlanCode"].ToString() + " - " + rdr["PlanName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<ClaimListViewModel> GetClaimList(ParameterClaimListViewModel param)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<ClaimListViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_claim") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, param.CompanyID) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, param.ClaimNo != null ? param.ClaimNo : "") as IDbDataParameter,
                        odal.CreateParameter("@skey2", SqlDbType.VarChar, param.PolicyNo != null ? param.PolicyNo : "") as IDbDataParameter,
                        odal.CreateParameter("@skey3", SqlDbType.VarChar, param.InsuredName != null ? param.InsuredName : "") as IDbDataParameter,
                        odal.CreateParameter("@skey4", SqlDbType.VarChar, param.ClaimType != null ? param.ClaimType : "") as IDbDataParameter,
                        odal.CreateParameter("@skey5", SqlDbType.VarChar, param.ClaimStatus != null ? param.ClaimStatus : "") as IDbDataParameter,
                        odal.CreateParameter("@pageNum", SqlDbType.Int, param.Page) as IDbDataParameter,
                        odal.CreateParameter("@pageSize", SqlDbType.Int, param.PerPage) as IDbDataParameter,
                        odal.CreateParameter("@prop", SqlDbType.VarChar, param.Prop != null ? param.Prop : "") as IDbDataParameter,
                        odal.CreateParameter("@sort", SqlDbType.VarChar, param.Sort != null ? param.Sort : "") as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new ClaimListViewModel
                    {
                        ID = rdr["ID"].ToString(),
                        ClaimNo = rdr["ClaimNo"].ToString(),
                        PolicyNo = rdr["PolicyNo"].ToString(),
                        InsuredName = rdr["InsuredName"].ToString(),
                        ClaimType = rdr["ClaimType"].ToString(),
                        ClaimDate = helperLib.GetDate(rdr["ClaimDate"]),
                        Status = rdr["Status"].ToString(),
                        Length = Convert.ToInt32(rdr["Length"])
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public HeaderClaimMaintenanceViewModel GetHeaderClaim(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new HeaderClaimMaintenanceViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "header_claim") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.Int, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.ID = rdr["ID"].ToString();
                    model.ClaimNo = rdr["ClaimNo"].ToString();
                    model.ApplicationNo = rdr["ApplicationNo"].ToString();
                    model.InsuredDate = rdr.IsDBNull(3) ? (DateTime?)null : helperLib.GetDate(rdr["InsuredDate"]);
                    model.ClaimStatus = rdr["ClaimStatus"].ToString();
                    model.ClaimStatusName = rdr["ClaimStatusName"].ToString();
                    model.ClaimDate = rdr.IsDBNull(6) ? (DateTime?)null : helperLib.GetDate(rdr["ClaimDate"]);
                    model.CreatedBy = rdr["CreatedBy"].ToString();
                    model.ClaimType = rdr["ClaimType"].ToString();
                    model.ClaimTypeName = rdr["ClaimTypeName"].ToString();
                    model.ClaimType = rdr["ClaimType"].ToString();
                    model.ApprovedBy = rdr["ApprovedBy"].ToString();
                    model.Notes = rdr["Notes"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;

        }

        public ClaimDetailViewModel GetClaimDetail(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new ClaimDetailViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "claim_detail") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.Int, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.PolicyNo = rdr["PolicyNo"].ToString();
                    model.PolicyOwner = rdr["PolicyOwner"].ToString();
                    model.EffectiveDate = helperLib.GetDate(rdr["EffectiveDate"]);
                    model.CertificateNo = rdr["CertificateNo"].ToString();
                    model.MaturityDate = helperLib.GetDate(rdr["MaturityDate"]);
                    model.InsuredName = rdr["InsuredName"].ToString();
                    model.InsuredDOB = helperLib.GetDate(rdr["InsuredDOB"]);
                    model.InsuredSex = rdr["InsuredSex"].ToString();
                    model.InsuredAge = rdr["InsuredAge"].ToString();
                    model.BankAccountName = rdr["BankAccountName"].ToString();
                    model.BankAccountNo = rdr["BankAccountNo"].ToString();
                    model.BankName = rdr["BankName"].ToString();
                    model.BeneficiaryAccountName = rdr["BeneficiaryAccountName"].ToString();
                    model.BeneficiaryAccountNo = rdr["BeneficiaryAccountNo"].ToString();
                    model.BeneficiaryBankName = rdr["BeneficiaryBankName"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;

        }

        public AssesmentViewModel GetAssesment(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new AssesmentViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "assesment") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.Int, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.DiagnosisCode = rdr["DiagnosisCode"].ToString();
                    model.DiagnosisCodeName = rdr["DiagnosisCodeName"].ToString();
                    model.OtherDiagnosis = rdr["OtherDiagnosis"].ToString();
                    model.Physician = rdr["Physician"].ToString();
                    model.HospitalCode = rdr["HospitalCode"].ToString();
                    model.HospitalCodeName = rdr["HospitalCodeName"].ToString();
                    model.HospitalName = rdr["HospitalName"].ToString();
                    model.HospitalizationStartDate = rdr.IsDBNull(7) ? (DateTime?)null : helperLib.GetDate(rdr["HospitalizationStartDate"]);
                    model.HospitalizationEndDate = rdr.IsDBNull(8) ? (DateTime?)null : helperLib.GetDate(rdr["HospitalizationEndDate"]);
                    model.HospitalChargeCurrency = rdr["HospitalChargeCurrency"].ToString();
                    model.HospitalChargeCurrencyName = rdr["HospitalChargeCurrencyName"].ToString();
                    model.HospitalCharge = rdr.IsDBNull(11) ? 0 : Convert.ToDecimal(rdr["HospitalCharge"]);
                    model.DateOfDeath = rdr.IsDBNull(12) ? (DateTime?)null : helperLib.GetDate(rdr["DateOfDeath"]); 
                    model.PlaceOfDeath = rdr["PlaceOfDeath"].ToString();
                    model.CauseOfDeath = rdr["CauseOfDeath"].ToString();
                    model.CauseOfDeathName = rdr["CauseOfDeathName"].ToString();
                    model.OtherDeath = rdr["OtherDeath"].ToString();
                    model.ClaimCurrency = rdr["ClaimCurrency"].ToString();
                    model.ClaimCurrencyName = rdr["ClaimCurrencyName"].ToString();
                    model.ClaimAmount = rdr.IsDBNull(19) ? 0 : Convert.ToDecimal(rdr["ClaimAmount"]);
                    model.PlanType = rdr["PlanType"].ToString();
                    model.PlanTypeName = rdr["PlanTypeName"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;

        }

        public List<ClaimBenefitViewModel> GetClaimBenefitList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<ClaimBenefitViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_claimbenefit") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.Int, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new ClaimBenefitViewModel
                    {
                        ID = rdr["ID"].ToString(),
                        BenefitCode = rdr["BenefitCode"].ToString(),
                        BenefitCodeName = rdr["BenefitCodeName"].ToString(),
                        ClaimAmount = rdr.IsDBNull(3) ? 0 : Convert.ToDecimal(rdr["ClaimAmount"]),
                        NoOfDays = rdr.IsDBNull(4) ? 0 : Convert.ToInt32(rdr["NoOfDays"]),
                        TotalClaimAmount = rdr.IsDBNull(5) ? 0 : Convert.ToDecimal(rdr["TotalClaimAmount"]),
                        Notes = rdr["Notes"].ToString(),
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<ClaimActivityViewModel> GetActivityClaimList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<ClaimActivityViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_activityclaim") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.Int, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new ClaimActivityViewModel
                    {
                        ActivityDate = helperLib.GetDate(rdr["ActivityDate"]),
                        ActivityBy = rdr["ActivityBy"].ToString(),
                        ActivityHistory = rdr["ActivityHistory"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<PreviousClaimViewModel> GetPreviousClaimList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<PreviousClaimViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "list_previousclaim") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter,
                        odal.CreateParameter("@nkey1", SqlDbType.Int, pprm.ikey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new PreviousClaimViewModel
                    {
                        ID = rdr["ID"].ToString(),
                        ClaimNo = rdr["ClaimNo"].ToString(),
                        ClaimDate = helperLib.GetDate(rdr["ClaimDate"]),
                        ClaimantName = rdr["ClaimantName"].ToString(),
                        ClaimAmount = rdr.IsDBNull(3) ? 0 : Convert.ToDecimal(rdr["ClaimAmount"].ToString()),
                        ClaimStatus = rdr["ClaimStatus"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public List<BenefitCodeViewModel> GetBenefitCodeList(cParams pprm) {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new List<BenefitCodeViewModel>();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "benefit_code_list") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.Add(new BenefitCodeViewModel
                    {
                        BenefitCode = rdr["BenefitCode"].ToString(),
                        BenefitName = rdr["BenefitName"].ToString(),
                        LimitAmount = Convert.ToDecimal(rdr["LimitAmount"]),
                        LimitDuration = Convert.ToInt32(rdr["LimitDuration"])
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public Decimal GetRemainingAmount(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            Decimal remainingAmount = 0;
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "amount_remaining") as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter,
                        odal.CreateParameter("@skey2", SqlDbType.VarChar, pprm.skey2) as IDbDataParameter,
                        odal.CreateParameter("@skey3", SqlDbType.VarChar, pprm.skey3) as IDbDataParameter);
                while (rdr.Read())
                {
                    remainingAmount = Convert.ToDecimal(rdr["remainingAmount"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return remainingAmount;
        }

        public int GetRemainingDuration(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            int remainingDuration = 0;
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "duration_remaining") as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter,
                        odal.CreateParameter("@skey2", SqlDbType.VarChar, pprm.skey2) as IDbDataParameter,
                        odal.CreateParameter("@skey3", SqlDbType.VarChar, pprm.skey3) as IDbDataParameter);
                while (rdr.Read())
                {
                    remainingDuration = Convert.ToInt32(rdr["remainingDuration"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return remainingDuration;
        }
        public string GetAutoCalculate(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            string AutoCalculate = "";
            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "is_auto_calculate") as IDbDataParameter,
                        odal.CreateParameter("@skey2", SqlDbType.VarChar, pprm.skey2) as IDbDataParameter,
                        odal.CreateParameter("@skey3", SqlDbType.VarChar, pprm.skey3) as IDbDataParameter);
                while (rdr.Read())
                {
                    AutoCalculate = rdr["AutoCalculate"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return AutoCalculate;
        }

        public ClaimDetailViewModel GetClaimDataByApplicationNo(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr;
            var model = new ClaimDetailViewModel();

            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                        odal.CreateParameter("@table", SqlDbType.VarChar, "dataclaim_byappplicationNo") as IDbDataParameter,
                        odal.CreateParameter("@CompanyID", SqlDbType.VarChar, pprm.companyid) as IDbDataParameter,
                        odal.CreateParameter("@skey1", SqlDbType.VarChar, pprm.skey1) as IDbDataParameter);
                while (rdr.Read())
                {
                    model.PolicyNo = rdr["PolicyNo"].ToString();
                    model.PolicyOwner = rdr["PolicyOwner"].ToString();
                    model.EffectiveDate = helperLib.GetDate(rdr["EffectiveDate"]);
                    model.CertificateNo = rdr["CertificateNo"].ToString();
                    model.MaturityDate = helperLib.GetDate(rdr["MaturityDate"]);
                    model.InsuredName = rdr["InsuredName"].ToString();
                    model.InsuredDOB = helperLib.GetDate(rdr["InsuredDOB"]);
                    model.InsuredSex = rdr["InsuredSex"].ToString();
                    model.InsuredAge = rdr["InsuredAge"].ToString();
                    model.PlanType = rdr["PlanType"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return model;
        }

        public void SaveClaim(ClaimMaintenanceViewModel param)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr1, rdr2;
            var valueNull = DBNull.Value;

            try
            {
                if (param.HeaderClaim.ID == null || param.HeaderClaim.ID == "")
                {
                    rdr1 = odal.ExecuteDataReader("usp_crud_claim", CommandType.StoredProcedure,
                        odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT") as IDbDataParameter,
                        odal.CreateParameter("@claimType", SqlDbType.VarChar, param.HeaderClaim.ClaimType) as IDbDataParameter,
                        odal.CreateParameter("@claimDate", SqlDbType.DateTime, param.HeaderClaim.ClaimDate == null ? (object)DBNull.Value : param.HeaderClaim.ClaimDate.Value) as IDbDataParameter,
                        odal.CreateParameter("@claimCurrency", SqlDbType.VarChar, param.Assesment.ClaimCurrency) as IDbDataParameter,
                        odal.CreateParameter("@claimAmount", SqlDbType.Float, param.Assesment.ClaimAmount) as IDbDataParameter,
                        odal.CreateParameter("@applicationNo", SqlDbType.VarChar, param.HeaderClaim.ApplicationNo) as IDbDataParameter,
                        odal.CreateParameter("@certificateNo", SqlDbType.VarChar, param.ClaimDetail.CertificateNo) as IDbDataParameter,
                        odal.CreateParameter("@claimStatus", SqlDbType.VarChar, param.HeaderClaim.ClaimStatus) as IDbDataParameter,
                        odal.CreateParameter("@insuredDate", SqlDbType.DateTime, param.HeaderClaim.InsuredDate == null ? (object)DBNull.Value : param.HeaderClaim.InsuredDate.Value) as IDbDataParameter,
                        odal.CreateParameter("@bankAccountName", SqlDbType.VarChar, param.ClaimDetail.BankAccountName) as IDbDataParameter,
                        odal.CreateParameter("@bankAccountNo", SqlDbType.VarChar, param.ClaimDetail.BankAccountNo) as IDbDataParameter,
                        odal.CreateParameter("@bankName", SqlDbType.VarChar, param.ClaimDetail.BankName) as IDbDataParameter,
                        odal.CreateParameter("@benefAccountName", SqlDbType.VarChar, param.ClaimDetail.BeneficiaryAccountName) as IDbDataParameter,
                        odal.CreateParameter("@benefAccountNo", SqlDbType.VarChar, param.ClaimDetail.BeneficiaryAccountNo) as IDbDataParameter,
                        odal.CreateParameter("@benefBankName", SqlDbType.VarChar, param.ClaimDetail.BeneficiaryBankName) as IDbDataParameter,
                        odal.CreateParameter("@diagnosisCode", SqlDbType.VarChar, param.Assesment.DiagnosisCode) as IDbDataParameter,
                        odal.CreateParameter("@otherDiagnosis", SqlDbType.VarChar, param.Assesment.OtherDiagnosis) as IDbDataParameter,
                        odal.CreateParameter("@physician", SqlDbType.VarChar, param.Assesment.Physician) as IDbDataParameter,
                        odal.CreateParameter("@hospitalCode", SqlDbType.VarChar, param.Assesment.HospitalCode) as IDbDataParameter,
                        odal.CreateParameter("@hospitalName", SqlDbType.VarChar, param.Assesment.HospitalName) as IDbDataParameter,
                        odal.CreateParameter("@hospitalizationStartDate", SqlDbType.DateTime, param.Assesment.HospitalizationStartDate == null ? (object)DBNull.Value : param.Assesment.HospitalizationStartDate.Value) as IDbDataParameter,
                        odal.CreateParameter("@hospitalizationEndDate", SqlDbType.DateTime, param.Assesment.HospitalizationEndDate == null ? (object)DBNull.Value : param.Assesment.HospitalizationEndDate.Value) as IDbDataParameter,
                        odal.CreateParameter("@hospitalchargecurrency", SqlDbType.VarChar, param.Assesment.HospitalChargeCurrency) as IDbDataParameter,
                        odal.CreateParameter("@hospitalchargeAmount", SqlDbType.Float, param.Assesment.HospitalCharge) as IDbDataParameter,
                        odal.CreateParameter("@dateOfDeath", SqlDbType.DateTime, param.Assesment.DateOfDeath == null ? (object)DBNull.Value : param.Assesment.DateOfDeath.Value) as IDbDataParameter,
                        odal.CreateParameter("@placeOfDeath", SqlDbType.VarChar, param.Assesment.PlaceOfDeath) as IDbDataParameter,
                        odal.CreateParameter("@causeOfDeath", SqlDbType.VarChar, param.Assesment.CauseOfDeath) as IDbDataParameter,
                        odal.CreateParameter("@otherDeath", SqlDbType.VarChar, param.Assesment.OtherDeath) as IDbDataParameter,
                        odal.CreateParameter("@notes", SqlDbType.VarChar, param.HeaderClaim.Notes) as IDbDataParameter,
                        odal.CreateParameter("@planID", SqlDbType.VarChar, param.Assesment.PlanType) as IDbDataParameter,
                        odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                        odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter);
                    while (rdr1.Read())
                    {
                        param.HeaderClaim.ID = rdr1["ClaimID"].ToString();
                    }

                    //Benefit
                    if (param.ClaimBenefitList != null)
                    {
                        foreach (var benefit in param.ClaimBenefitList)
                        {
                            rdr2 = odal.ExecuteDataReader("usp_crud_claim", CommandType.StoredProcedure,
                              odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT_BENEFIT") as IDbDataParameter,
                              odal.CreateParameter("@claimID", SqlDbType.VarChar, param.HeaderClaim.ID) as IDbDataParameter,
                              odal.CreateParameter("@planID", SqlDbType.VarChar, param.Assesment.PlanType) as IDbDataParameter,
                              odal.CreateParameter("@benefitCode", SqlDbType.VarChar, benefit.BenefitCode) as IDbDataParameter,
                              odal.CreateParameter("@benefitClaimAmount", SqlDbType.Float, benefit.ClaimAmount) as IDbDataParameter,
                              odal.CreateParameter("@benefitClaimDuration", SqlDbType.Int, benefit.NoOfDays) as IDbDataParameter,
                              odal.CreateParameter("@totalClaimAmount", SqlDbType.Float, benefit.TotalClaimAmount) as IDbDataParameter,
                              odal.CreateParameter("@notes", SqlDbType.VarChar, benefit.Notes) as IDbDataParameter,
                              odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                              odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                        }
                    }
                }
                else
                {
                    rdr1 = odal.ExecuteDataReader("usp_crud_claim", CommandType.StoredProcedure,
                        odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE") as IDbDataParameter,
                        odal.CreateParameter("@claimID", SqlDbType.VarChar, param.HeaderClaim.ID) as IDbDataParameter,
                        odal.CreateParameter("@claimType", SqlDbType.VarChar, param.HeaderClaim.ClaimType == null ? (object)DBNull.Value : param.HeaderClaim.ClaimType) as IDbDataParameter,
                        odal.CreateParameter("@claimDate", SqlDbType.DateTime, param.HeaderClaim.ClaimDate == null ? (object)DBNull.Value : param.HeaderClaim.ClaimDate.Value) as IDbDataParameter,
                        odal.CreateParameter("@claimCurrency", SqlDbType.VarChar, param.Assesment.ClaimCurrency) as IDbDataParameter,
                        odal.CreateParameter("@claimAmount", SqlDbType.Float, param.Assesment.ClaimAmount) as IDbDataParameter,
                        odal.CreateParameter("@applicationNo", SqlDbType.VarChar, param.HeaderClaim.ApplicationNo == null ? (object)DBNull.Value : param.HeaderClaim.ApplicationNo) as IDbDataParameter,
                        odal.CreateParameter("@certificateNo", SqlDbType.VarChar, param.ClaimDetail.CertificateNo == null ? (object)DBNull.Value : param.ClaimDetail.CertificateNo) as IDbDataParameter,
                        odal.CreateParameter("@claimStatus", SqlDbType.VarChar, param.HeaderClaim.ClaimStatus == null ? (object)DBNull.Value : param.HeaderClaim.ClaimStatus) as IDbDataParameter,
                        odal.CreateParameter("@insuredDate", SqlDbType.DateTime, param.HeaderClaim.InsuredDate == null ? (object)DBNull.Value : param.HeaderClaim.InsuredDate.Value) as IDbDataParameter,
                        odal.CreateParameter("@bankAccountName", SqlDbType.VarChar, param.ClaimDetail.BankAccountName) as IDbDataParameter,
                        odal.CreateParameter("@bankAccountNo", SqlDbType.VarChar, param.ClaimDetail.BankAccountNo) as IDbDataParameter,
                        odal.CreateParameter("@bankName", SqlDbType.VarChar, param.ClaimDetail.BankName) as IDbDataParameter,
                        odal.CreateParameter("@benefAccountName", SqlDbType.VarChar, param.ClaimDetail.BeneficiaryAccountName) as IDbDataParameter,
                        odal.CreateParameter("@benefAccountNo", SqlDbType.VarChar, param.ClaimDetail.BeneficiaryAccountNo) as IDbDataParameter,
                        odal.CreateParameter("@benefBankName", SqlDbType.VarChar, param.ClaimDetail.BeneficiaryBankName) as IDbDataParameter,
                        odal.CreateParameter("@diagnosisCode", SqlDbType.VarChar, param.Assesment.DiagnosisCode) as IDbDataParameter,
                        odal.CreateParameter("@otherDiagnosis", SqlDbType.VarChar, param.Assesment.OtherDiagnosis) as IDbDataParameter,
                        odal.CreateParameter("@physician", SqlDbType.VarChar, param.Assesment.Physician) as IDbDataParameter,
                        odal.CreateParameter("@hospitalCode", SqlDbType.VarChar, param.Assesment.HospitalCode) as IDbDataParameter,
                        odal.CreateParameter("@hospitalName", SqlDbType.VarChar, param.Assesment.HospitalName) as IDbDataParameter,
                        odal.CreateParameter("@hospitalizationStartDate", SqlDbType.DateTime, param.Assesment.HospitalizationStartDate == null ? (object)DBNull.Value : param.Assesment.HospitalizationStartDate.Value) as IDbDataParameter,
                        odal.CreateParameter("@hospitalizationEndDate", SqlDbType.DateTime, param.Assesment.HospitalizationEndDate == null ? (object)DBNull.Value : param.Assesment.HospitalizationEndDate.Value) as IDbDataParameter,
                        odal.CreateParameter("@hospitalchargecurrency", SqlDbType.VarChar, param.Assesment.HospitalChargeCurrency) as IDbDataParameter,
                        odal.CreateParameter("@hospitalchargeAmount", SqlDbType.Float, param.Assesment.HospitalCharge) as IDbDataParameter,
                        odal.CreateParameter("@dateOfDeath", SqlDbType.DateTime, param.Assesment.DateOfDeath == null ? (object)DBNull.Value : param.Assesment.DateOfDeath.Value) as IDbDataParameter,
                        odal.CreateParameter("@placeOfDeath", SqlDbType.VarChar, param.Assesment.PlaceOfDeath) as IDbDataParameter,
                        odal.CreateParameter("@causeOfDeath", SqlDbType.VarChar, param.Assesment.CauseOfDeath) as IDbDataParameter,
                        odal.CreateParameter("@otherDeath", SqlDbType.VarChar, param.Assesment.OtherDeath) as IDbDataParameter,
                        odal.CreateParameter("@notes", SqlDbType.VarChar, param.HeaderClaim.Notes) as IDbDataParameter,
                        odal.CreateParameter("@planID", SqlDbType.VarChar, param.Assesment.PlanType) as IDbDataParameter,
                        odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                        odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter);


                    //Benefit
                    if (param.ClaimBenefitList != null)
                    {
                        var arrayID = string.Join(",", param.ClaimBenefitList.Select(m => m.ID));
                        rdr2 = odal.ExecuteDataReader("usp_crud_claim", CommandType.StoredProcedure,
                             odal.CreateParameter("@actionType", SqlDbType.VarChar, "DELETE_NOT_IN") as IDbDataParameter,
                             odal.CreateParameter("@arrayID", SqlDbType.VarChar, arrayID) as IDbDataParameter,
                             odal.CreateParameter("@claimID", SqlDbType.VarChar, param.HeaderClaim.ID) as IDbDataParameter,
                             odal.CreateParameter("@planID", SqlDbType.VarChar, param.Assesment.PlanType) as IDbDataParameter);

                        foreach (var benefit in param.ClaimBenefitList)
                        {
                            if (benefit.ID == null || benefit.ID == "")
                            {
                                rdr2 = odal.ExecuteDataReader("usp_crud_claim", CommandType.StoredProcedure,
                                  odal.CreateParameter("@actionType", SqlDbType.VarChar, "INSERT_BENEFIT") as IDbDataParameter,
                                  odal.CreateParameter("@claimID", SqlDbType.VarChar, param.HeaderClaim.ID) as IDbDataParameter,
                                  odal.CreateParameter("@planID", SqlDbType.VarChar, param.Assesment.PlanType) as IDbDataParameter,
                                  odal.CreateParameter("@benefitCode", SqlDbType.VarChar, benefit.BenefitCode) as IDbDataParameter,
                                  odal.CreateParameter("@benefitClaimAmount", SqlDbType.Float, benefit.ClaimAmount) as IDbDataParameter,
                                  odal.CreateParameter("@benefitClaimDuration", SqlDbType.Int, benefit.NoOfDays) as IDbDataParameter,
                                  odal.CreateParameter("@totalClaimAmount", SqlDbType.Float, benefit.TotalClaimAmount) as IDbDataParameter,
                                  odal.CreateParameter("@notes", SqlDbType.VarChar, benefit.Notes) as IDbDataParameter,
                                  odal.CreateParameter("@createdBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                                  odal.CreateParameter("@createdDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                            }
                            else
                            {
                                rdr2 = odal.ExecuteDataReader("usp_crud_claim", CommandType.StoredProcedure,
                                 odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_BENEFIT") as IDbDataParameter,
                                 odal.CreateParameter("@id", SqlDbType.VarChar, benefit.ID) as IDbDataParameter,
                                 odal.CreateParameter("@claimID", SqlDbType.VarChar, param.HeaderClaim.ID) as IDbDataParameter,
                                 odal.CreateParameter("@planID", SqlDbType.VarChar, param.Assesment.PlanType) as IDbDataParameter,
                                 odal.CreateParameter("@benefitCode", SqlDbType.VarChar, benefit.BenefitCode) as IDbDataParameter,
                                 odal.CreateParameter("@benefitClaimAmount", SqlDbType.Float, benefit.ClaimAmount) as IDbDataParameter,
                                 odal.CreateParameter("@benefitClaimDuration", SqlDbType.Int, benefit.NoOfDays) as IDbDataParameter,
                                 odal.CreateParameter("@totalClaimAmount", SqlDbType.Float, benefit.TotalClaimAmount) as IDbDataParameter,
                                 odal.CreateParameter("@notes", SqlDbType.VarChar, benefit.Notes) as IDbDataParameter,
                                 odal.CreateParameter("@updatedBy", SqlDbType.VarChar, param.UserID) as IDbDataParameter,
                                 odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr1 = null/* TODO Change to default(_) if this is not a reference type */;
                rdr2 = null;
                //pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public void UpdateClaimStatus(int ID, string userID, string type)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            var model = new List<DropDownViewModel>();
            IDataReader rdr1;
            try
            {
                rdr1 = odal.ExecuteDataReader("usp_crud_claim", CommandType.StoredProcedure,
                      odal.CreateParameter("@actionType", SqlDbType.VarChar, "UPDATE_CLAIM_STATUS") as IDbDataParameter,
                      odal.CreateParameter("@type", SqlDbType.VarChar, type) as IDbDataParameter,
                      odal.CreateParameter("@updatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                      odal.CreateParameter("@updatedBy", SqlDbType.VarChar, userID) as IDbDataParameter,
                      odal.CreateParameter("@claimID", SqlDbType.Int, ID) as IDbDataParameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
                rdr1 = null;
            }
        }
        #endregion
    }
}