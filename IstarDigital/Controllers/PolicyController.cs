﻿using IstarDigital.BLL;
using IstarDigital.Models.Policy;
using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using IstarDigital.Models;
using System.Data;
using ExcelDataReader;
using System.IO;
using System.Collections.Generic;
using System.Security.Claims;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using static IstarDigital.Helpers.helperLib;

namespace IstarDigital.Controllers
{
    public class PolicyController : ApiController
    {
        IstarDigitalbll objAccountBLL = new IstarDigitalbll();

        #region POLICY LIST AND NEW
        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        public dynamic GetStatusList(string codeType)
        {
            return objAccountBLL.GetStatusList(codeType);
        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        public dynamic GetPlanList()
        {
            return objAccountBLL.GetPlanList();
        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        public dynamic ProductUploadInfo()
        {
            return objAccountBLL.GetProductsList();
        }

        [HttpPost]
        //[Authorize]
        public ApiDefaultReturn GetPolicyList(ParameterListPolicyViewModel param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                var data = objAccountBLL.GetPolicyList(param);
                model.IsSuccess = true;
                model.Data = data;
                model.Message = "Success";
                model.Length = data.Count() > 0 ? data.FirstOrDefault().Length : 0;
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }

        [HttpPost]
        public ApiDefaultReturn TemplateValidated(string productCode)
        {
            var upload = HttpContext.Current.Request.Files.Count > 0 ?
            HttpContext.Current.Request.Files[0] : null;

            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    // ExcelDataReader works with the binary Excel file, so it needs a FileStream
                    // to get started. This is how we avoid dependencies on ACE or Interop:
                    Stream stream = upload.InputStream;

                    // We return the interface, so that
                    IExcelDataReader reader = null;

                    if (upload.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (upload.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    else
                    {
                        return new ApiDefaultReturn(false, null, "This file format is not supported", 0);
                    }

                    UploadPolicy dt = new UploadPolicy();
                    DataTable dt_ = new DataTable();
                    var listPolicyNo = new List<string>();
                    try
                    {
                        dt_ = reader.AsDataSet().Tables[0];
                        for (int i = 0; i < dt_.Columns.Count; i++)
                        {
                            if (i == 0)
                            {
                                dt.Column.Add("Status");
                            }
                            if (dt_.Rows[0][i].ToString().Contains("\n"))
                            {
                                var newColumn = dt_.Rows[0][i].ToString().Replace("\n", "").TrimStart().TrimEnd();
                                dt.Column.Add(newColumn);
                            }
                            else
                            {
                                dt.Column.Add(dt_.Rows[0][i].ToString().TrimStart().TrimEnd());
                            }
                        }

                        var headerExcel = dt.Column.ConvertAll(d => d.ToLower()).Skip(1);
                        var headerTemplate = objAccountBLL.GetColumnUpload(productCode).SequenceEqual(headerExcel);
                        if (!headerTemplate)
                        {
                            return new ApiDefaultReturn(false, null, "Invalid Template", 0);
                        }

                        for (int row_ = 1; row_ < dt_.Rows.Count; row_++)
                        {
                            var message = new List<string>();
                            var RowModel = new List<CertificateRowViewModel>();
                            RowData row = new RowData();
                            row.Value.Add("0");
                            for (int col = 0; col < dt_.Columns.Count; col++)
                            {
                                var dataTypeInput = dt_.Rows[row_][col].GetType().Name;
                                if (dataTypeInput.ToLower() == "datetime")
                                {
                                    row.Value.Add(Convert.ToDateTime(dt_.Rows[row_][col]).ToString("dd-MMM-yyyy"));
                                }
                                else
                                {
                                    row.Value.Add(dt_.Rows[row_][col].ToString());
                                }

                                string column = dt_.Rows[0][col].ToString().TrimStart().TrimEnd();
                                if (column.Contains("\n"))
                                {
                                    column = column.Replace("\n", "");
                                }

                                object value = dt_.Rows[row_][col];
                                var validate = objAccountBLL.ValidatePolicy(productCode, column, value);
                                if (validate != null || validate == "") { message.Add(validate); }

                                if (column.ToLower().TrimStart().TrimEnd() == "policy number")
                                {
                                    if (value.ToString().TrimEnd().TrimStart() != "")
                                    {
                                        if (listPolicyNo.Count() < 1)
                                        {
                                            listPolicyNo.Add(dt_.Rows[row_][col].ToString());
                                        }
                                        else
                                        {
                                            var checkPolicyNo = listPolicyNo.Any(m => m == dt_.Rows[row_][col].ToString().TrimStart().TrimEnd());
                                            if (checkPolicyNo)
                                            {
                                                message.Add("Duplicate PolicyNo");
                                            }
                                        }
                                    }
                                }

                                RowModel.Add(new CertificateRowViewModel { Header = column.ToLower().TrimStart().TrimEnd(), DataType = dataTypeInput.ToLower(), Value = value.ToString().TrimStart().TrimEnd() });
                            }

                            var validate2 = objAccountBLL.ValidatePerRow(RowModel, productCode);
                            if (validate2 != null || validate2 == "") { message.Add(validate2); }

                            if (message.Count() < 1)
                            {
                                row.Value[0] = "Success";
                            }
                            else
                            {
                                row.Value[0] = string.Join(", ", message);
                            }

                            var checkRow = row.Value.Skip(1).All(m => string.IsNullOrWhiteSpace(m.ToString()));
                            if (!checkRow)
                            {
                                dt.AllTable.Add(row);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        return new ApiDefaultReturn(false, null, ex.Message, 0);
                    }

                    reader.Close();
                    reader.Dispose();

                    return new ApiDefaultReturn(true, dt, "Success", 0);
                }
                else
                {
                    return new ApiDefaultReturn(false, null, "Please Upload Your file", 0);
                }
            }

            return new ApiDefaultReturn(false, null, "Error", 0);
        }


        [HttpPost]
        //[Authorize]
        public ApiDefaultReturn PolicyApproved(ParameterPolicyList param)
        {
            int[] listID = param.arrayID.Select(s => Convert.ToInt32(s)).ToArray();
            var model = new ApiDefaultReturn();
            try
            {
                foreach (var ID in listID)
                {
                    var CertificateStatus = objAccountBLL.GetCertificateStatus(ID);
                    var listeCertificate = new List<ECertificateViewModel>();
                    if (CertificateStatus == "WR")
                    {
                        string id =  ID.ToString();

                        listeCertificate = objAccountBLL.GetECertificateData(param.companyID, id);
                        //SubmitDataToKokatto(listeCertificate);
                    }
                    objAccountBLL.PolicyApproved(ID, param.userID);
                    objAccountBLL.CreateActivity("POLICY", HistoryType.ApprovedCertificate, ID, "", param.userID);
                }
                model.IsSuccess = true;
                model.Message = "Success";
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }

        [HttpPost]
        //[Authorize]
        public ApiDefaultReturn PolicyValidated(ParameterPolicyList param)
        {
            int[] listID = param.arrayID.Select(s => Convert.ToInt32(s)).ToArray();
            var model = new ApiDefaultReturn();
            try
            {
                model.IsSuccess = true;
                model.Message = "Success";
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }

        public string LoginKokatto()
        {
            var account = new LoginAnotherApi() { username = "8026api@kokatto.com", password = "8026apiuser" };
            var token = "";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://xapi.kokatto.com/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.PostAsJsonAsync("api/v1/identity/login", account);

            if (response.Result.IsSuccessStatusCode)
            {
                var json = response.Result.Content.ReadAsStringAsync().Result;
                dynamic dynamicObject = JsonConvert.DeserializeObject(json);
                token = dynamicObject["token"];
            }

            return token;
        }

        public void SubmitDataToKokatto(List<ECertificateViewModel> model)
        {
            var token = LoginKokatto();
            var requestID = "";
            var message = "";
            var notifModel = new TNotification();
            try
            {
                foreach (var data in model)
                {
                    var id = data.certificatenumber;
                    var clientNotifRefID = "9999" + id;
                    var edpModel = new EPDViewModel() { appType = "EPD", clientId = 8026, clientNotifRefId = id, appInputMap = data };

                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri("https://clientapi.kokatto.com/");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var response = client.PostAsJsonAsync("api/v2/notifications", edpModel);

                    if (response.Result.IsSuccessStatusCode)
                    {
                        var json = response.Result.Content.ReadAsStringAsync().Result;
                        dynamic dynamicObject = JsonConvert.DeserializeObject(json);
                        requestID = dynamicObject["requestId"];

                        notifModel.CertificateNo = data.certificatenumber;
                        notifModel.ClientNotifRefId = clientNotifRefID;
                        notifModel.RequestID = requestID;
                        objAccountBLL.InsertNotification(notifModel);
                    }
                    else
                    {
                        var json = response.Result.Content.ReadAsStringAsync().Result;
                        dynamic dynamicObject = JsonConvert.DeserializeObject(json);
                        message = dynamicObject["message"];

                        notifModel.CertificateNo = data.certificatenumber;
                        notifModel.ClientNotifRefId = clientNotifRefID;
                        notifModel.Message = message;
                        objAccountBLL.InsertNotification(notifModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        //[Authorize]
        public ApiDefaultReturn CreatePolicy(string userID, string companyID, string productCode, UploadPolicy input)
        {
            var model = new ApiDefaultReturn();
            var listID = new List<int>();
            var listeCertificate = new List<ECertificateViewModel>();
            try
            {
                foreach (RowData rowData in input.AllTable)
                {
                    var ID = objAccountBLL.CreatePolicy(userID, companyID, productCode, rowData);
                    listID.Add(ID);
                }
                model.IsSuccess = true;
                model.Message = "Success";

                string listCerID = "'" + string.Join("','", listID) + "'";

                listeCertificate = objAccountBLL.GetECertificateData(companyID, listCerID);
                //SubmitDataToKokatto(listeCertificate);
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }
            return model;
        }
        #endregion

        #region POLICY MAINTENANCE
        [HttpPost]
        //[Authorize]
        public PolicyMaintenanceViewModel GetPolicyMaintenance(cParams pprm)
        {
            var policyMaintenance = new PolicyMaintenanceViewModel();

            var headerPolicy = objAccountBLL.GetHeaderPolicy(pprm);
            var policyInfo = objAccountBLL.GetPolicyInformation(pprm);
            var ownerInfo = objAccountBLL.GetOwnerInformation(pprm);
            var hasRenewed = objAccountBLL.HasRenewed(pprm);

            policyMaintenance.HeaderPolicy = headerPolicy;
            policyMaintenance.PolicyInfo = policyInfo;
            policyMaintenance.OwnerInfo = ownerInfo;
            policyMaintenance.HasRenewed = hasRenewed;

            return policyMaintenance;
        }

        [HttpPost]
        //[Authorize]
        public dynamic GetInsuredInformationList(cParams pprm)
        {
            return objAccountBLL.GetInsuredInformationList(pprm);
        }

        [HttpPost]
        //[Authorize]
        public dynamic GetBeneficiaryInformationList(cParams pprm)
        {
            return objAccountBLL.GetBeneficiaryInformationList(pprm);
        }

        [HttpGet]
        //[Authorize]
        public dynamic GetCustomerList(string companyID)
        {
            return objAccountBLL.GetCustomerListForPolicyMaintenance(companyID);
        }

        [HttpPost]
        public List<CoverageViewModel> GetCoverageInfoList(cParams pprm)
        {
            return objAccountBLL.GetCoverageInfoList(pprm);
        }

        [HttpPost]
        public dynamic GetBenefitList(cParams pprm)
        {
            return objAccountBLL.GetBenefitList(pprm);
        }

        [HttpPost]
        public dynamic GetActivityHistoryList(cParams pprm)
        {
            return objAccountBLL.GetActivityList(pprm);
        }

        [HttpPost]
        //[Authorize]
        public ApiDefaultReturn PolicyMaintenanceApproved(ParameterPolicyList param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                var CertificateStatus = objAccountBLL.GetCertificateStatus(Convert.ToInt32(param.ID));
                var listeCertificate = new List<ECertificateViewModel>();
                if (CertificateStatus == "WR")
                {
                    string id = param.ID;

                    listeCertificate = objAccountBLL.GetECertificateData(param.companyID, id);
                    //SubmitDataToKokatto(listeCertificate);
                }
                objAccountBLL.PolicyApproved(Convert.ToInt32(param.ID), param.userID);
                objAccountBLL.CreateActivity("POLICY", HistoryType.ApprovedCertificate, Convert.ToInt32(param.ID), "", param.userID);
                model.IsSuccess = true;
                model.Message = "Success";
                
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }

        [HttpPost]
        //[Authorize]
        public ApiDefaultReturn UpdatePolicy(PolicyMaintenanceViewModel param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                objAccountBLL.UpdatePolicy(param);
                model.IsSuccess = true;
                model.Message = "Success";
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }
            return model;
        }

        [HttpPost]
        //[Authorize]
        public dynamic GetRemarksList(cParams pprm) 
        {
            return objAccountBLL.GetRemarksList(pprm);
        }

        [HttpPost]
        //[Authorize]
        public ApiDefaultReturn CancelationPolicy(PolicyCancelationViewModel param)
        {
            var model = new ApiDefaultReturn();
            var pprm = new cParams();
            try
            {
                pprm.companyid = param.CompanyID;
                pprm.ikey1 = param.ID;

                objAccountBLL.CancelationPolicy(param);
                var date = param.CancelDate;
                var header = objAccountBLL.GetHeaderPolicy(pprm);

                objAccountBLL.CreateActivity("POLICY", HistoryType.CancelCertificate, param.ID, "", param.UserID);

                model.IsSuccess = true;
                model.Data = new { CancelDate = date, StatusPolicy = header.PolicyStatus };
                model.Message = "Success";
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }
            return model;
        }

        [HttpPost]
        public ApiDefaultReturn AddRemarksPolicy()
        {
            var model = new ApiDefaultReturn();
            var httpRequest = HttpContext.Current.Request;
            var remarks = new RemarksViewModel();
            remarks.CertificateID = Convert.ToInt32(httpRequest.Params["CertificateID"]);
            remarks.Date = Convert.ToDateTime(httpRequest.Params["Date"]);
            remarks.RemarksNotes = httpRequest.Params["Notes"];
            remarks.UserID = httpRequest.Params["UserID"];
            if (httpRequest.Files.Count > 0)
            {
                try
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        var filePath = HttpContext.Current.Server.MapPath("~/Attachments/PolicyRemarks/" + postedFile.FileName);
                        postedFile.SaveAs(filePath);
                        remarks.RemarksFileName = postedFile.FileName;
                        remarks.RemarksFilePath = "/Attachments/PolicyRemarks/" + postedFile.FileName;
                    }

                    objAccountBLL.AddRemarksPolicy(remarks);
                    objAccountBLL.CreateActivity("POLICY", HistoryType.AddRemarks, remarks.CertificateID, "", remarks.UserID);
                    model.IsSuccess = true;
                    model.Message = "Success";
                }
                catch (Exception ex)
                {
                    return new ApiDefaultReturn(false, null, ex.Message, 0);
                }
            }
            else
            {
                try {
                    objAccountBLL.AddRemarksPolicy(remarks);
                    model.IsSuccess = true;
                    model.Message = "Success";
                } catch (Exception ex)
                {
                    return new ApiDefaultReturn(false, null, ex.Message, 0);
                }
            }

            return model;
        }

        [HttpPost]
        public ApiDefaultReturn Renewal(ParameterPolicyList param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                var certifNo = objAccountBLL.Renewal(Convert.ToInt32(param.ID), param.userID);
                model.IsSuccess = true;
                model.Message = "Success";
                model.Data = certifNo;
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }

        #region Dropdown
        [HttpPost]
        //[Authorize]
        public List<DropDownViewModel> GetBillingMode(cParams pprm)
        {
            return objAccountBLL.GetBillingMode(pprm);
        }

        [HttpPost]
        //[Authorize]
        public List<DropDownViewModel> GetBillingStatus(cParams pprm)
        {
            return objAccountBLL.GetBillingStatus(pprm);
        }

        [HttpPost]
        //[Authorize]
        public List<DropDownViewModel> GetCurrency(cParams pprm)
        {
            return objAccountBLL.GetCurrency(pprm);
        }

        [HttpGet]
        //[Authorize]
        public List<DropDownViewModel> GetSelection()
        {
            var data = new List<DropDownViewModel>();
            data.Add(new DropDownViewModel { Id = "1", Value = "Yes" });
            data.Add(new DropDownViewModel { Id = "0", Value = "No" });

            return data;
        }

        [HttpPost]
        //[Authorize]
        public List<DropDownViewModel> GetGender(cParams pprm)
        {
            return objAccountBLL.GetGender(pprm);
        }

        [HttpGet]
        //[Authorize]
        public dynamic GetCancelReasonList(string companyID)
        {
            var data = objAccountBLL.GetTdCodeByCompanyID(companyID);
            return data.Where(m => m.CodeType.ToLower() == "cnclr").Select(m => new { m.CodeID, m.CodeName });
        }
        #endregion

        #endregion
    }
}