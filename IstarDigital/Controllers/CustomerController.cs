﻿using IstarDigital.BLL;
using IstarDigital.Models;
using IstarDigital.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace IstarDigital.Controllers
{
    public class CustomerController : ApiController
    {
        IstarDigitalbll objAccountBLL = new IstarDigitalbll();

        [HttpGet]
        //[Authorize]
        public dynamic GetCustomerTypeList(string companyID)
        {
            var data = objAccountBLL.GetTdCodeByCompanyID(companyID);
            return data.Where(m => m.CodeType.ToUpper() == "CSTRT").Select(m => new { m.CodeID, m.CodeName });
        }

        [HttpGet]
        //[Authorize]
        public dynamic GetGender(string companyID)
        {
            var data = objAccountBLL.GetTdCodeByCompanyID(companyID);
            return data.Where(m => m.CodeType.ToUpper() == "SEX").Select(m => new { m.CodeID, m.CodeName });
        }

        [HttpGet]
        //[Authorize]
        public dynamic GetCardType(string companyID)
        {
            var data = objAccountBLL.GetTdCodeByCompanyID(companyID);
            return data.Where(m => m.CodeType.ToUpper() == "IDCT").Select(m => new { m.CodeID, m.CodeName });
        }

        [HttpPost]
        //[Authorize]
        public ApiDefaultReturn GetCustomerList(ParameterCustomerListViewModel param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                var data = objAccountBLL.GetCustomerList(param);
                model.IsSuccess = true;
                model.Data = data;
                model.Message = "Success";
                model.Length = data.Count() > 0 ? data.FirstOrDefault().Length : 0;
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }

        [HttpPost]
        public CustomerMaintenanceViewModel GetCustomer(cParams pprm) {
            var customerMaintenance = new CustomerMaintenanceViewModel();
            var headerCustomer = objAccountBLL.GetHeaderCustomer(pprm);
            var addressList = objAccountBLL.GetAddressList(pprm);
            var contactDetail = objAccountBLL.GetContactDetailList(pprm);

            customerMaintenance.HeaderCustomer = headerCustomer;
            customerMaintenance.ContactDetailList = contactDetail;
            customerMaintenance.AddressList = addressList;
            return customerMaintenance;
        }

        [HttpPost]
        //[Authorize]
        public dynamic GetContactDetailList(cParams pprm)
        {
            return objAccountBLL.GetContactDetailList(pprm);
        }

        [HttpPost]
        //[Authorize]
        public dynamic GetAddressList(cParams pprm)
        {
            return objAccountBLL.GetAddressList(pprm);
        }
        [HttpGet]
        //[Authorize]
        public dynamic GetContactTypeList(string companyID, string codeType)
        {
            var data = objAccountBLL.GetTdCodeByCompanyID(companyID);
            return data.Where(m => m.CodeType.ToLower() == codeType).Select(m => new { m.CodeID, m.CodeName });
        }

        [HttpPost]
        //[Authorize]
        public ApiDefaultReturn SaveCustomer(CustomerMaintenanceViewModel param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                objAccountBLL.SaveCustomer(param);
                model.IsSuccess = true;
                model.Message = "Success";
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }
            return model;
        }
    }
}