﻿using System.Web;
using System.Web.Http;
using System.Net.Http;
using IstarDigital.Models;
using IstarDigital.BLL;
using System.Security.Claims;
using System.Net;
using System.Threading.Tasks;
using IstarDigital.Helpers;
using System.Linq;
using System;

namespace IstarDigital.Controllers
{
    public class AccountController : ApiController
    {
        IstarDigitalbll objAccountBLL = new IstarDigitalbll();

        /*
           updated : RS
           date : 31 MAY 2018
           Reason : ADD Date value Token check for Old Token 
       */

        [HttpPost]
        [AllowAnonymous]
        public DefaultReturnRegister Register(RegisterParam oParam)
        {
            var result = objAccountBLL.RegisterUser(oParam);

            DefaultReturnRegister oResult = null;
            oResult = new DefaultReturnRegister();

            if (result.Message == null || result.Message == "")
            {
                var accessTokenResponse = helperCom.GenerateLocalAccessTokenResponse(oParam.UserName, result.OtherValue);
                oResult.access_token = accessTokenResponse;
                oResult.token_type = "bearer";
                oResult.UserName = oParam.UserName;
            }
            else
            {
                oResult.access_token = "";
                oResult.token_type = "";
                oResult.UserName = "";
            }

            oResult.Result = result.Result;
            oResult.Message = result.Message;
            return oResult;

        }

        [HttpPost]
        [AllowAnonymous]
        public DefaultReturn RegisterUserPhase1(RegisterParamV1 oParam)
        {
            return objAccountBLL.RegisterUserPhase1(oParam);

        }

        /*
           updated : RS
           date : 31 MAY 2018
           Reason : ADD Date value Token check for Old Token 
       */

        [HttpPost]
        [AllowAnonymous]
        public DefaultReturnRegister RegisterUserPhase2(RegisterParam oParam)
        {
            var result = objAccountBLL.RegisterUserPhase2(oParam);

            DefaultReturnRegister oResult = null;
            oResult = new DefaultReturnRegister();

            if (result.Result == "SUCCESS")
            {
                var accessTokenResponse = helperCom.GenerateLocalAccessTokenResponse(oParam.UserName, result.OtherValue);
                oResult.access_token = accessTokenResponse;
                oResult.token_type = "bearer";
                oResult.UserName = oParam.UserName;
            }
            else
            {
                oResult.access_token = "";
                oResult.token_type = "";
                oResult.UserName = "";
            }

            oResult.Result = result.Result;
            oResult.Message = result.Message;
            return oResult;

        }

        [HttpPost]
        [Authorize]
        //[AllowAnonymous]
        public DefaultReturn ValidateActivationCode(ActivationParam oParam)
        {
            if (oParam.UserName.ToUpper() != ClaimsPrincipal.Current.Identity.Name.ToUpper())
            {
                DefaultReturn oResult = null;
                oResult = new DefaultReturn();

                oResult.Result = "FAILURE";
                oResult.Message = "UserID tidak valid";
                return oResult;

            }
            else
            {
                return objAccountBLL.ValidateActivationCode(oParam);
            }

        }

        [HttpPost]
        [Authorize]
        //[AllowAnonymous]
        public DefaultReturn ResendActivationCode(paramUserName oParam)
        {
            if (oParam.UserName.ToUpper() != ClaimsPrincipal.Current.Identity.Name.ToUpper())
            {
                DefaultReturn oResult = null;
                oResult = new DefaultReturn();

                oResult.Result = "FAILURE";
                oResult.Message = "UserID tidak valid";
                return oResult;

            }
            else
            {
                return objAccountBLL.ResendActivationCode(oParam.UserName);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public DefaultReturn ValidateUserName(string id)
        {
            return objAccountBLL.ValidateUserName(id);

        }


        [HttpPost]
        [AllowAnonymous]
        public DefaultReturn ForgotPassword(ForgotPassword oParam)
        {

            return objAccountBLL.ForgotPassword(oParam);

        }

        [HttpPost]
        [Authorize]
        //[AllowAnonymous]
        public DefaultReturn ChangePassword(ChangePassword oParam)
        {
            //ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            //oParam.UserID = ClaimsPrincipal.Current.Identity.Name;
            if (oParam.UserName.ToUpper() != ClaimsPrincipal.Current.Identity.Name.ToUpper())
            {
                DefaultReturn oResult = null;
                oResult = new DefaultReturn();

                oResult.Result = "FAILURE";
                oResult.Message = "UserID tidak valid";
                return oResult;

            }
            else
            {
                return objAccountBLL.ChangePassword(oParam);
            }

        }

        [HttpPost]
        [Authorize]
        //[AllowAnonymous]
        public DefaultReturn UpdateUserImage(changeUserImage oParam)
        {
            //ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            //oParam.UserID = ClaimsPrincipal.Current.Identity.Name;
            if (oParam.UserName.ToUpper() != ClaimsPrincipal.Current.Identity.Name.ToUpper())
            {
                DefaultReturn oResult = null;
                oResult = new DefaultReturn();

                oResult.Result = "FAILURE";
                oResult.Message = "UserID tidak valid";
                return oResult;

            }
            else
            {
                return objAccountBLL.changeUserImage(oParam);
            }

        }

        [HttpPost]
        //[AllowAnonymous]
        //[Authorize]
        public dynamic GetMenuList(cParams oParam)
        {
            cMenuList i = objAccountBLL.GetMenuList(oParam);
            var mn = from mns in i.cMenuListDetail
                     where mns.ObjectType == "G"
                     orderby mns.ObjectSequence ascending
                     select new
                     {
                         ObjectId = mns.ObjectId,
                         objectiveMenuDescription = mns.ObjectName,
                         Url = mns.ObjectLink,
                         objectiveIcon = mns.ObjectImage,
                         objectiveMenu = mns.LinkChild,
                         objectShortcut = mns.ObjectShortcut,
                         objectiveChild = from mnss in i.cMenuListDetail
                                          where mnss.ObjectParent == mns.ObjectId
                                          orderby mnss.ObjectSequence ascending
                                          select new
                                          {
                                              ObjectId = mnss.ObjectId,
                                              objectiveMenuDescription = mnss.ObjectName,
                                              Url = mnss.ObjectLink,
                                              objectiveIcon = mnss.ObjectImage,
                                              objectiveMenu = mnss.LinkChild,
                                              objectShortcut = mnss.ObjectShortcut
                                          }
                     };
            return mn;

        }
        [HttpPost]
        //[AllowAnonymous]
        //[Authorize]
        public dynamic GetAllowedAction(cParams oParam)
        {
            try
            {
                var list = objAccountBLL.GetButtonList(oParam);
                return new ApiDefaultReturn(true, list, "Success", 0);
            }
            catch (Exception ex) {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        public dynamic GetCompanyList()
        {
            return objAccountBLL.GetCompanyList();
        }
    }
}