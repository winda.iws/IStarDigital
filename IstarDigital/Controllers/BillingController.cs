﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IstarDigital.BLL;
using IstarDigital.Models.Billing;
using IstarDigital.Models;

namespace IstarDigital.Controllers
{
    public class BillingController : ApiController
    {

        IstarDigitalbll objAccountBLL = new IstarDigitalbll();

        [HttpPost]
        public dynamic GetBillingList(ParameterListBillingViewModel param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                var data = objAccountBLL.GetBillingList(param);
                model.IsSuccess = true;
                model.Data = data;
                model.Message = "Success";
                model.Length = data.Count() > 0 ? data.FirstOrDefault().Length : 0;
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;

        }

        [HttpGet]
        public dynamic GetBillStatusList(string CompanyID)
        {
            return objAccountBLL.GetBillingStatusBill(CompanyID);
        }

        [HttpGet]
        public dynamic GetBillTypeList(string CompanyID)
        {
            return objAccountBLL.GetBillingTypeBill(CompanyID);
        }

        [HttpPost]
        public dynamic GetBilling(cParams pprm)
        {
            var model = new BillingMaintenanceViewModel();
            var headerBilling = objAccountBLL.GetHeaderBilling(pprm);
            var detailBilling = objAccountBLL.GetDetailBilling(pprm);

            model.HeaderBilling = headerBilling;
            model.DetailBilling = detailBilling;

            return model;

        }

        [HttpPost]
        public ApiDefaultReturn SaveBilling(BillingMaintenanceViewModel param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                objAccountBLL.SaveBilling(param);
                model.IsSuccess = true;
                model.Message = "Success";
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }
            return model;

        }
        [HttpPost]
        public ApiDefaultReturn ApprovedBilling(ParameterUpdateStatusBilling param) {
            var model = new ApiDefaultReturn();
            try
            {
                objAccountBLL.BillingApproved(Convert.ToInt32(param.ID), param.UserID);

                model.IsSuccess = true;
                model.Message = "Success";
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }
    }
}
