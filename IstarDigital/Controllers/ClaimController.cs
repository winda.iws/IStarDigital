﻿using IstarDigital.BLL;
using IstarDigital.Models;
using IstarDigital.Models.Claim;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IstarDigital.Controllers
{
    public class ClaimController : ApiController
    {
        IstarDigitalbll objAccountBLL = new IstarDigitalbll();

        [HttpGet]
        public dynamic GetClaimType(string companyID)
        {
            return objAccountBLL.GetClaimTypeList(companyID);
        }

        [HttpGet]
        public dynamic GetAppNoList(string companyID)
        {
            return objAccountBLL.GetAppNoClaimList(companyID);
        }

        [HttpGet]
        public dynamic GetClaimStatus(string companyID)
        {
            return objAccountBLL.GetClaimStatusList(companyID);
        }

        [HttpGet]
        public dynamic GetDiagnosisCodeList(string companyID)
        {
            return objAccountBLL.GetDiagnosisCodeList(companyID);
        }

        [HttpGet]
        public dynamic GetHospitalCodeList(string companyID)
        {
            return objAccountBLL.GetHospitalCodeList(companyID);
        }

        [HttpGet]
        public dynamic GetCuaseOfDeathList(string companyID)
        {
            return objAccountBLL.GetCuaseOfDeathList(companyID);
        }

        //[HttpGet]
        //public dynamic GetPlanType(string companyID)
        //{
        //    return objAccountBLL.GetPlanType(companyID);
        //}

        [HttpGet]
        public dynamic GetPlanType(string companyID, string applicationNo)
        {
            return objAccountBLL.GetPlanTypeByApplicationNo(companyID, applicationNo);
        }

        [HttpPost]
        public ApiDefaultReturn GetClaimList(ParameterClaimListViewModel param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                var data = objAccountBLL.GetClaimList(param);
                model.IsSuccess = true;
                model.Data = data;
                model.Message = "Success";
                model.Length = data.Count() > 0 ? data.FirstOrDefault().Length : 0;
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }

        [HttpPost]
        public ClaimMaintenanceViewModel GetClaim(cParams pprm)
        {
            var claimMaintenance = new ClaimMaintenanceViewModel();
            var headerClaim = objAccountBLL.GetHeaderClaim(pprm);
            var claimDetail = objAccountBLL.GetClaimDetail(pprm);
            var assesment = objAccountBLL.GetAssesment(pprm);

            claimMaintenance.HeaderClaim = headerClaim;
            claimMaintenance.ClaimDetail = claimDetail;
            claimMaintenance.Assesment = assesment;

            return claimMaintenance;
        }

        [HttpPost]
        public dynamic GetBenefitList(cParams pprm)
        {
            return objAccountBLL.GetClaimBenefitList(pprm);
        }

        [HttpPost]
        public dynamic GetActivityList(cParams pprm)
        {
            return objAccountBLL.GetActivityClaimList(pprm);
        }

        [HttpPost]
        public dynamic GetPreviousClaimList(cParams pprm)
        {
            return objAccountBLL.GetPreviousClaimList(pprm);
        }

        [HttpPost]
        public dynamic GetBenefitCodeList(cParams pprm)
        {
            return objAccountBLL.GetBenefitCodeList(pprm);
        }

        [HttpPost]
        public dynamic GetRemainingBenefit(cParams pprm)
        {
            var benefit = new BenefitRemainingViewModel();
            benefit.Amount = objAccountBLL.GetRemainingAmount(pprm);
            benefit.Duration = objAccountBLL.GetRemainingDuration(pprm);
            benefit.AutoCalculate = objAccountBLL.GetAutoCalculate(pprm);
            return benefit;
        }

        [HttpPost]
        public dynamic GetClaimDataByApplicationNo(cParams pprm)
        {
            return objAccountBLL.GetClaimDataByApplicationNo(pprm);
        }

        [HttpPost]
        public ApiDefaultReturn SaveClaim(ClaimMaintenanceViewModel param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                objAccountBLL.SaveClaim(param);
                model.IsSuccess = true;
                model.Message = "Success";
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }
            return model;

        }

        [HttpPost]
        public ApiDefaultReturn ClaimCancel(ParameterClaimStatusViewModel param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                objAccountBLL.UpdateClaimStatus(Convert.ToInt32(param.ID), param.userID, "CANCEL");
                model.IsSuccess = true;
                model.Message = "Success";
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }

        [HttpPost]
        public ApiDefaultReturn ClaimApproved(ParameterClaimStatusViewModel param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                objAccountBLL.UpdateClaimStatus(Convert.ToInt32(param.ID), param.userID, "APPROVED");
                model.IsSuccess = true;
                model.Message = "Success";
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }
    }
}
