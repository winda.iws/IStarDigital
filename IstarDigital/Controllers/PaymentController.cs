﻿using IstarDigital.BLL;
using IstarDigital.Models;
using IstarDigital.Models.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using static IstarDigital.Helpers.helperLib;

namespace IstarDigital.Controllers
{
    public class PaymentController : ApiController
    {
        IstarDigitalbll objAccountBLL = new IstarDigitalbll();

        [HttpGet]
        //[Authorize]
        public dynamic GetPaymentTypeList(string companyID)
        {
            var data = objAccountBLL.GetTdCodeByCompanyID(companyID);
            return data.Where(m => m.CodeType.ToUpper() == "PYTYP").Select(m => new { m.CodeID, m.CodeName });
        }

        [HttpGet]
        //[Authorize]
        public dynamic GetTransTypeList(string companyID)
        {
            var data = objAccountBLL.GetTdCodeByCompanyID(companyID);
            return data.Where(m => m.CodeType.ToUpper() == "TRTYP").Select(m => new { m.CodeID, m.CodeName });
        }

        [HttpGet]
        //[Authorize]
        public dynamic GetPaymentStatusList(string companyID)
        {
            var data = objAccountBLL.GetTdCodeByCompanyID(companyID);
            return data.Where(m => m.CodeType.ToUpper() == "PYSTS").Select(m => new { m.CodeID, m.CodeName });
        }

        [HttpGet]
        //[Authorize]
        public dynamic GetAppNoList(string companyID)
        {
            return objAccountBLL.GetAppNoList(companyID);
        }

        [HttpPost]
        //[Authorize]
        public dynamic GetOwnerNameByAppNo(cParams pprm)
        {
            return objAccountBLL.GetOwnerNameByAppNo(pprm);
        }

        [HttpPost]
        public ApiDefaultReturn GetPaymentList(ParameterPaymentListViewModel param) 
        {

            var model = new ApiDefaultReturn();
            try
            {
                var data = objAccountBLL.GetPaymentList(param);
                model.IsSuccess = true;
                model.Data = data;
                model.Message = "Success";
                model.Length = data.Count() > 0 ? data.FirstOrDefault().Length : 0;
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }

        [HttpPost]
        public PaymentMaintenanceViewModel GetPayment(cParams pprm) {

            var paymentMaintenance = new PaymentMaintenanceViewModel();
            var headerPayment = objAccountBLL.GetHeaderPayment(pprm);
            var detailPayment = objAccountBLL.GetDetailPayment(pprm);
            var historyPayment = objAccountBLL.GetHistoryPayment(pprm);

            paymentMaintenance.HeaderPayment = headerPayment;
            paymentMaintenance.DetailPayment = detailPayment;
            paymentMaintenance.HistoryPaymentList = historyPayment;

            return paymentMaintenance;
        }

        [HttpPost]
        public ApiDefaultReturn SavePayment(PaymentMaintenanceViewModel param) {
            var model = new ApiDefaultReturn();
            try
            {
                objAccountBLL.SavePayment(param);
                model.IsSuccess = true;
                model.Message = "Success";
            }
            catch (Exception ex) {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }
            return model;

        }
        [HttpPost]
        //[Authorize]
        public ApiDefaultReturn PaymentApproved(ParameterPaymentList param)
        {
            var model = new ApiDefaultReturn();
            try
            {
                objAccountBLL.PaymentApproved(Convert.ToInt32(param.PaymentID), param.UserID);
                objAccountBLL.CreateActivity("PAYMENT", PaymentStatus.Approve, Convert.ToInt32(param.PaymentID), "", param.UserID);

                model.IsSuccess = true;
                model.Message = "Success";
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }

            return model;
        }
    }
}