﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using IstarDigital.BLL;
using IstarDigital.Models;
using IstarDigital.Models.Report;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

namespace IstarDigital.Controllers
{
    public class ReportController : ApiController
    {

        IstarDigitalbll objAccountBLL = new IstarDigitalbll();

        [HttpGet]
        public dynamic GetPlanListByProduct(string productID)
        {
            return objAccountBLL.GetPlanListByProduct(productID);
        }

        [HttpGet]
        public dynamic GetProductList()
        {
            var data = objAccountBLL.GetProductsList();
            return data.Select(m => new DropDownViewModel { Id = m.ProductCode, Value = m.ProductName });
        }

        [HttpGet]
        public dynamic GetCertificateStatusList(string companyID)
        {
            var data = objAccountBLL.GetTdCodeByCompanyID(companyID);
            return data.Where(m => m.CodeType.ToUpper() == "STTS").Select(m => new { m.CodeID, m.CodeName });
        }

        [HttpPost]
        public ApiDefaultReturn GetPolicyReport(ParameterPolicyReportViewModel param)
        {
            var result = new ApiDefaultReturn();
            try
            {
                var url = objAccountBLL.GetReportServiceURL().ParameterValue;
                var user = objAccountBLL.GetReportServiceUser().ParameterValue;
                var password = objAccountBLL.GetReportServicePassword().ParameterValue;
                var pathFolder = objAccountBLL.GetFolderReportPolicy().ParameterValue;

                var param1 = param.EffectiveDateFrom == null ? "&effectiveDateFrom:isnull=true" : "&effectiveDateFrom=" + param.EffectiveDateFrom.Value.Date.ToString("yyyy-MM-dd");
                var param2 = param.EffectiveDateTo == null ? "&effectiveDateTo:isnull=true" : "&effectiveDateTo=" + param.EffectiveDateTo.Value.Date.ToString("yyyy-MM-dd");
                var param3 = param.PlanCode == null ? "&planCode:isnull=true" : "&planCode=" + param.PlanCode;
                var param4 = param.ProductCode == null ? "&productCode:isnull=true" : "&productCode=" + param.ProductCode;
                var param5 = param.CertificateStatus == null ? "&certificateStatus:isnull=true" : "&certificateStatus=" + param.CertificateStatus;

                string reportURL = url + param1 + param2 + param3 + param4 + param5 + "&rs:Format=EXCELOPENXML";

                string dirProject = System.AppDomain.CurrentDomain.BaseDirectory;
                string fileName = "ExtractPolicy_" + DateTime.Now.ToString("dd_MMM_yyyy_HHmmss") + ".xlsx";
                var filepath = dirProject + @pathFolder + fileName;

                WebClient myWebClient = new WebClient();
                NetworkCredential netCredential = new NetworkCredential(user, password);
                myWebClient.Credentials = netCredential;

                byte[] myDataBuffer = myWebClient.DownloadData(reportURL);

                if (!System.IO.File.Exists(filepath))
                {
                    System.IO.File.WriteAllBytes(filepath, myDataBuffer);
                    //SAVE FILE HERE
                }

                result.IsSuccess = true;
                result.Message = "Success";
                result.Data = fileName;

                return result;
            }
            catch (Exception ex)
            {
                return new ApiDefaultReturn(false, null, ex.Message, 0);
            }
        }
    }
}