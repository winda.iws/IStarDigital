﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Policy
{
    public class EPDViewModel
    {
        public string appType { get; set; }
        public int clientId { get; set; }
        public string clientNotifRefId { get; set; }
        public ECertificateViewModel appInputMap { get; set; }
    }

    public class ECertificateViewModel
    {
        public string certificatenumber { get; set; }
        public string effectivedate { get; set; }
        public string planname { get; set; }
        public string ownername { get; set; }
        public string ownerdob { get; set; }
        public string ownerage { get; set; }
        public string gender { get; set; }
        public string address { get; set; }
        public string mobilenumber { get; set; }
        public string EMAIL_1 { get; set; }
        public string customername { get; set; }
        public string dateofbirth { get; set; }
        public string insuredage { get; set; }
        public string totalpremium { get; set; }
        public string sumassured { get; set; }
        public string hospital { get; set; }
        public string adb { get; set; }
        public string tpd { get; set; }
        public string adbmotorcycle { get; set; }
        public string deathterrorism { get; set; }
        public string beneficiaryname { get; set; }
        public string maturitydate { get; set; }
        public string partnername { get; set; }
        public string coverageduration { get; set; }
        public string hospitallimit { get; set; }
    }

    public class TNotification
    {
        public string CertificateNo { get; set; }
        public string ClientNotifRefId { get; set; }
        public string Message { get; set; }
        public string RequestID { get; set; }
    }
}