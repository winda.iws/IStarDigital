﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Policy
{
    public class PolicyMaintenanceViewModel
    {
        public string UserID { get; set; }
        public string CompanyID { get; set; }
        public HeaderPolicyMaintenanceViewModel HeaderPolicy { get; set; }
        public PolicyInfoViewModel PolicyInfo { get; set; }
        public OwnerInformationViewModel OwnerInfo { get; set; }
        public List<InsuredORbeneficiaryViewModel> InsuredList { get; set; }
        public List<InsuredORbeneficiaryViewModel> BeneficiaryList { get; set; }
        public List<RemarksViewModel> RemarksList { get; set; }
        public bool HasRenewed { get; set; }
    }

    public class HeaderPolicyMaintenanceViewModel
    {
        public string ID { get; set; }
        public string PolicyNo { get; set; }
        public string SPAJNo { get; set; }
        public string PolicyStatus { get; set; }
        public string UnderwritingStatus { get; set; }
        public string FreeLookPolicy { get; set; }
        public string OwnerName { get; set; }
        public DateTime OwnerBirthday { get; set; }
        public string BusinessPartner { get; set; }
        public string ProductName { get; set; }
        public string PrevCertificateNo { get; set; }
    }

    public class PolicyInfoViewModel
    {
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public DateTime? SignDate { get; set; }
        public string RequiredHC { get; set; }
        public decimal DeliveryFee { get; set; }
        public string Currency { get; set; }
        public string CurrencyName { get; set; }
        public string BillingMode { get; set; }
        public string BillingModeName { get; set; }
        public string BillingStatus { get; set; }
        public string BillingStatusName { get; set; }
        public DateTime? MaturityDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public decimal? PremiumAmount { get; set; }
        public decimal? PremiumNett { get; set; }
        public decimal? PremiumTax { get; set; }
        public float? TaxPercentage{ get; set; }
        public float? ExtraMortalityRating { get; set; }
        public decimal? ExtraPremiumAmount { get; set; }
        public decimal? ExtraNettPremiumAmount { get; set; }
        public decimal? TotalPremiumAmount { get; set; }
        public string Notes { get; set; }
    }

    public class OwnerInformationViewModel
    {
        public string OwnerID { get; set; }
        public string OwnerName { get; set; }
        public DateTime? OwnerDOB { get; set; }
        public string OwnerGender { get; set; }
        public string OwnerContact { get; set; }
        public string OwnerAddress { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
    }

    public class InsuredORbeneficiaryViewModel
    {
        public string ID { get; set; }
        public string CustomerID { get; set; }
        public string Name { get; set; }
        public DateTime? DOB { get; set; }
        public string Gender { get; set; }
        public string Relation { get; set; }
    }

    public class CoverageViewModel
    {
        public int ID { get; set; }
        public string PlanCode { get; set; }
        public string PlanName { get; set; }
        public decimal PremiumAmount { get; set; }
        public int Period { get; set; }
        public string PeriodType { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? Maturity { get; set; }
        public string status { get; set; }
    }

    public class BenefitViewModel
    {
        public string Benefit { get; set; }
        public Decimal SumAssured { get; set; }
        public int LimitClaim { get; set; }
    }

    public class ActivityViewModel
    {
        public DateTime HistoryDate { get; set; }
        public string By { get; set; }
        public string Activity { get; set; }
        public string Notes { get; set; }
    }

    public class CustomerViewModel
    {
        public string CustomerID { get; set; }
        public string Name { get; set; }
        public DateTime? DOB { get; set; }
        public string Gender { get; set; }
        public string MainContact { get; set; }
    }

    public class RemarksViewModel {
        public int ID { get; set; }
        public int CertificateID { get; set; }
        public DateTime Date { get; set; }
        public string By { get; set; }
        public string RemarksNotes { get; set; }
        public string RemarksFileName { get; set; }
        public string RemarksFilePath { get; set; }
        public string UserID { get; set; }
    }
}