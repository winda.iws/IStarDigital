﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Policy
{
    public class ParameterListPolicyViewModel
    {
        public string CompanyID { get; set; }
        public int Page { get; set; }
        public int PerPage { get; set; }
        public string Prop { get; set; }
        public string Sort { get; set; }
        public string PolicyNo { get; set; }
        public string ApplicationNo { get; set; }
        public string PlanCode { get; set; }
        public string Status { get; set; }
        public DateTime? BirthDate { get; set; }
        public string MobileNo { get; set; }
        public DateTime? EffectiveDateFrom { get; set; }
        public DateTime? EffectiveDateTo { get; set; }
        public string CustomerName { get; set; }
    }

    public class ParameterPolicyList
    {
        public string companyID { get; set; }
        public string ID { get; set; }
        public string userID { get; set; }
        public string[] arrayID { get; set; }
    }
}