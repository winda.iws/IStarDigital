﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Policy
{
    public class PolicyCancelationViewModel
    {
        public int ID { get; set; }
        public string UserID { get; set; }
        public string CompanyID { get; set; }
        public DateTime CancelDate { get; set; }
        public string CancelReason { get; set; }
        public string OtherReason { get; set; }
        public string Notes { get; set; }
    }
}