﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Policy
{
    public class ProductViewModel
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string FileType { get; set; }
        public string TemplateFile { get; set; }

    }
}