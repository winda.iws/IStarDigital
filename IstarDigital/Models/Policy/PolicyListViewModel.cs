﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Policy
{
    public class PolicyListViewModel
    {
        public string ID { get; set; }
        public string PolicyNo { get; set; }
        public string ApplicationNo { get; set; }
        public string CertificateNo { get; set; }
        public string CodeID { get; set; }
        public string Status { get; set; }
        public string OwnerName { get; set; }
        public string PlanCode { get; set; }
        public string PlanName { get; set; }
        public Decimal Premium { get; set; }
        public DateTime EffectiveDate { get; set; }
        public int? CoverageDuration { get; set; }
        public string FreeLookPending { get; set; }
        public DateTime BirthDate { get; set; }
        public string MobileNo { get; set; }
        public string CustomerName { get; set; }
        public int Length { get; set; }
    }

    public class TPlanViewModel
    {
        public string PlanCode { get; set; }
        public string PlanName { get; set; }
        public int CoverageDuration { get; set; }
        public string ProductCode { get; set; }

    }

    public class TUploadViewModel
    {
        public string FieldName { get; set; }
        public string DataType { get; set; }
        public int Sequence { get; set; }
        public string Parameter { get; set; }
    }

    public class TdCodeViewModel {
        public string CodeType { get; set; }
        public string CodeID { get; set; }
        public string CodeName { get; set; }
    }

    public class CertificateRowViewModel
    {
        public string Header { get; set; }
        public string DataType { get; set; }
        public object Value { get; set; }
    }
}