﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Policy
{
    public class UploadPolicy
    {
        public List<string> Column { get; set; } = new List<string>();
        public List<RowData> AllTable { get; set; } = new List<RowData>();
    }
    public class RowData
    {
        public List<object> Value { get; set; } = new List<object>();
    }
}