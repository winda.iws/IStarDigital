﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models
{
    public class LandingPage
    {
        public string UserName { get; set; }
        public string UserDescription { get; set; }
        public DateTime LastUpdatedUserImage { get; set; }
        public byte[] UserImage { get; set; }
        public DateTime LastUpdatedBannerImage { get; set; }
        public List<BannerImage> BannerImages { get; set; }
        public int TotalNotification { get; set; }
        public int TotalUnreadNotification { get; set; }
        //public List<NotificationList> Notifications { get; set; }
        //public List<popupNotificationList> popupNotifications { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class LandingPage_Path
    {
        public string UserName { get; set; }
        public string UserDescription { get; set; }
        public DateTime LastUpdatedUserImage { get; set; }
        public byte[] UserImage { get; set; }
        public DateTime LastUpdatedBannerImage { get; set; }
        public List<BannerImage_Path> BannerImages { get; set; }
        public int TotalNotification { get; set; }
        public int TotalUnreadNotification { get; set; }
        //public List<NotificationList> Notifications { get; set; }
        //public List<popupNotificationList_Path> popupNotifications { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class HomePageParam
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public DateTime LastUpdatedBannerImage { get; set; }
    }
    public class LandingPageParam
    {
        public string UserName { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public DateTime LastUpdatedBannerImage { get; set; }
        public DateTime LastUpdatedUserImage { get; set; }
        public string DeviceType { get; set; }
        public string osType { get; set; }
    }

    public class BannerImage
    {
        public int ImageSequence { get; set; }

        public byte[] ImageDetail { get; set; }

        public string ImageURL { get; set; }

        public DateTime LastUpdated { get; set; }

        public string Result { get; set; }
    }
    public class HomePage
    {
        public DateTime LastUpdatedBannerImage { get; set; }
        public List<BannerImage> BannerImages { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class BannerImage_Path
    {
        public int ImageSequence { get; set; }

        public string ImageDetail { get; set; }

        public string ImageURL { get; set; }

        public DateTime LastUpdated { get; set; }

        public string Result { get; set; }
    }

    public class HomePage_Path
    {
        public DateTime LastUpdatedBannerImage { get; set; }
        public List<BannerImage_Path> BannerImages { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }
}