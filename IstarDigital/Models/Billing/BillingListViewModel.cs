﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Billing
{
    public class BillingListViewModel
    {
        public int BillID { get; set; }
        public string BillNo { get; set; }
        public DateTime Duedate { get; set; }
        public string PolicyNo { get; set; }
        public string ApplicationNo { get; set; }
        public string StatusCode { get; set; }
        public string Status { get; set; }
        public string TypeCode { get; set; }
        public string Type { get; set; }
        public string OwnerName { get; set; }
        public string Source { get; set; }
        public int Length { get; set; }
    }
}