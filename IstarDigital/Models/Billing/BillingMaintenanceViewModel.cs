﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Billing
{
    public class BillingMaintenanceViewModel
    {
        public string UserID { get; set; }
        public HeaderBillingMaintenanceViewModel HeaderBilling { get; set; }
        public DetailBillingMaintenanceViewModel DetailBilling { get; set; }

    }
    public class HeaderBillingMaintenanceViewModel
    {
        public string BillingID { get; set; }
        public string BillingNo { get; set; }
        public string PolicyNo { get; set; }
        public string BillingType { get; set; }
        public decimal BillingAmount { get; set; }
        public string ApplicationNo { get; set; }
        public string BillingMode { get; set; }
        public string BillingStatus { get; set; }
        public string OwnerName { get; set; }
    }
    public class DetailBillingMaintenanceViewModel
    {
        public DateTime BillingDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime LastBillingDate { get; set; }
        public DateTime LastPaymentDate { get; set; }
        public string CreditCardType { get; set; }
        public string CreditCardNumber { get; set; }
        public string CreditCardHolderName { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountHolderName { get; set; }
        public string BankCode { get; set; }
        public string BankBranchCode { get; set; }
        public string Currency { get; set; }

    }
}