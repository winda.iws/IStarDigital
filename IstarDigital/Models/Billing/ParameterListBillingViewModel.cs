﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Billing
{
    public class ParameterListBillingViewModel
    {
        public string CompanyID { get; set; }
        public int Page { get; set; }
        public int PerPage { get; set; }
        public string Prop { get; set; }
        public string Sort { get; set; }
        public string PolicyNo { get; set; }
        public string ApplicationNo { get; set; }
        public string BillingType { get; set; }
        public DateTime? DueDate { get; set; }
        public string Status { get; set; }
    }

    public class ParameterUpdateStatusBilling
    {
        public int ID { get; set; }
        public string UserID { get; set; }
    }
}