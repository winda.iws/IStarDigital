﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Report
{
    public class PolicyReportViewModel
    {
        public string CIFNumber { get; set; }
        public string CertificateNumber { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string ProductName { get; set; }
        public string PlanCode { get; set; }
        public string PlanName { get; set; }
        public string OwnerName { get; set; }
        public DateTime OwnerDOB { get; set; }
        public string OwnerAge { get; set; }
        public string NIK { get; set; }
        public string Gender { get; set; }
        public string Occupation { get; set; }
        public string Religion { get; set; }
        public string Address { get; set; }
        public string KotaDomisili { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string CustomerName { get; set; }
        public string PlaceOfBirth { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string InsuredAge { get; set; }
        public string InsuredRelation { get; set; }
        public string HardCopyPolicy { get; set; }
        public decimal? DeliveryFee { get; set; }
        public decimal? Premium { get; set; }
        public decimal? TotalPremium { get; set; }
        public decimal? OriginalPartnerComission { get; set; }
        public decimal? ActualPartnerComission { get; set; }
        public decimal? PartnerComission { get; set; }
        public string PaymentSource { get; set; }
        public decimal SumAssured { get; set; }
        public decimal Hospital { get; set; }
        public decimal ADB { get; set; }
        public decimal TPD { get; set; }
        public decimal ADBMotorcycle { get; set; }
        public decimal DeathTerrorism { get; set; }
        public decimal SumAssuredAdditional { get; set; }
        public string HospitalLimit { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryRelationship { get; set; }
        public DateTime? BeneficiaryDOB { get; set; }
        public string CoverageDuration { get; set; }
        public DateTime MaturityDate { get; set; }
        public string CertificateStatus { get; set; }
        public DateTime? CancelDate { get; set; }
        public string CancelRemark { get; set; }
        public string PartnerName { get; set; }
        public string DistributionChannel { get; set; }
    }

    public class ParameterPolicyReportViewModel
    {
        public DateTime? EffectiveDateFrom { get; set; }
        public DateTime? EffectiveDateTo { get; set; }
        public string PlanCode { get; set; }
        public string ProductCode { get; set; }
        public string CertificateStatus { get; set; }
    }

    public class TparameterViewModel
    {
        public string ParameterID { get; set; }
        public string ParameterValue { get; set; }
        public string ParameterType { get; set; }
    }
}