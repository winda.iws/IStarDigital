﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Payment
{
    public class PaymentMaintenanceViewModel
    {
        public string UserID { get; set; }
        public HeaderPaymentMaintenanceViewModel HeaderPayment { get; set; }
        public DetailPaymentMaintenanceViewModel DetailPayment { get; set; }
        public List<HistoryPaymentMaintenanceViewModel> HistoryPaymentList { get; set; }
    }

    public class HeaderPaymentMaintenanceViewModel {
        public string PaymentID { get; set; }
        public string PaymentNo { get; set; }
        public string PolicyNo { get; set; }
        public string PaymentSource { get; set; }
        public string PaymentType { get; set; }
        public string ApplicationNo { get; set; }
        public string TransType { get; set; }
        public string OwnerName { get; set; }
        public string PaymentStatus { get; set; }
        public string ProgressStatus { get; set; }

    }

    public class DetailPaymentMaintenanceViewModel {
        public DateTime PaymentDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string UploadedBy { get; set; }
        public DateTime? UploadDate { get; set; }
        public string Currency { get; set; }
        public Decimal PaymentAmount { get; set; }
        public string RefferenceNo { get; set; }
        public string TrxRefno { get; set; }
        public string TrxDate { get; set; }
        public string TrxTime { get; set; }
        public string Merchant { get; set; }
    }

    public class HistoryPaymentMaintenanceViewModel {
        public DateTime HistoryDate { get; set; }
        public string HistoryType { get; set; }
        public string HistoryBy { get; set; }
        public string RefferenceNo { get; set; }
        public string PaymentInfo { get; set; }
        public string Notes { get; set; }
    }

    public class DataPaymentByAppNoViewModel {
        public string OwnerName { get; set; }
        public DateTime EffectiveDate { get; set; }
        public Decimal PaymentAmount { get; set; }
        public string Currency { get; set; }
    }
}