﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Payment
{
    public class ParameterPaymentListViewModel
    {
        public string CompanyID { get; set; }
        public int Page { get; set; }
        public int PerPage { get; set; }
        public string Prop { get; set; }
        public string Sort { get; set; }
        public string PolicyNo { get; set; }
        public string ApplicationNo { get; set; }
        public string PaymentType { get; set; }
        public string ProgressStatus { get; set; }
        public DateTime? PaymentDate { get; set; }
    }

    public class ParameterPaymentList
    {
        public string UserID { get; set; }
        public string PaymentID { get; set; }
    }
}