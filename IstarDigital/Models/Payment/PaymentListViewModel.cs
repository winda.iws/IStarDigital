﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Payment
{
    public class PaymentListViewModel
    {
        public Int64 ID { get; set; }
        public string PaymentNo { get; set; }
        public DateTime PaymentDate { get; set; }
        public string PolicyNo { get; set; }
        public string ApplicationNo { get; set; }
        public string OwnerName { get; set; }
        public string PaymentType { get; set; }
        public string ProgressStatus { get; set; }
        public string PaymentStatus { get; set; }
        public string Source { get; set; }
        public int Length { get; set; }
    }
}