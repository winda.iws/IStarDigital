﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models
{
    public class UserLogin
    {
        public string UserID { get; set; }
        public string UserPassword { get; set; }
        public string userType { get; set; }
        public string UserStatus { get; set; }
        public string KeyUpdate { get; set; }
        public string CodeStatus { get; set; }
        public string CodeMessage { get; set; }

    }

    public class ChangePassword
    {
        public string UserName { get; set; }
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }

    }

    public class ActivationParam
    {
        public string UserName { get; set; }
        public string ActivationCode { get; set; }
        public string ActivationType { get; set; }
        public string ActivationInfo { get; set; }
    }

    public class RegisterParam
    {
        public string PolicyID { get; set; }
        public string BirthDate { get; set; }
        public string ActivationType { get; set; }
        public string ActivationInfo { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
    }

    public class RegisterParamV1
    {
        public string PolicyID { get; set; }
        public string BirthDate { get; set; }
        public string ActivationType { get; set; }
        public string ActivationInfo { get; set; }
    }

    public class ForgotPassword
    {
        public string PolicyID { get; set; }
        public string UserName { get; set; }
        public string SendingType { get; set; }
        public string SendingInfo { get; set; }
        public string Result { get; set; }
        public string Result2 { get; set; }
        public string Message { get; set; }
        public bool ResultFlag { get; set; }
    }

    public class changeUserImage
    {
        public string UserName { get; set; }
        public string UserImage { get; set; }

    }


    /*Release1*/
    public class ConfirmRegistrationParam
    {
        public string BirthDate { get; set; }
        public string ActivationType { get; set; }
        public string ActivationInfo { get; set; }
        public string PolicyID { get; set; }
    }

    public class ForgotPassword2Param
    {
        public string BirthDate { get; set; }
        public string ForgetType { get; set; }
        public string ActivationType { get; set; }
        public string ActivationInfo { get; set; }
        public string PolicyID { get; set; }
    }


    public class cParams
    {
        public string userid { get; set; }
        public string companyid { get; set; }
        public string paramtype { get; set; }
        public int pagesize { get; set; }
        public int pageno { get; set; }
        public string skey1 { get; set; }
        public string skey2 { get; set; }
        public string skey3 { get; set; }
        public string skey4 { get; set; }
        public string skey5 { get; set; }
        public string skey6 { get; set; }
        public string skey7 { get; set; }
        public string skey8 { get; set; }
        public string skey9 { get; set; }
        public string skey10 { get; set; }
        public int ikey1 { get; set; }
        public int ikey2 { get; set; }
        public int ikey3 { get; set; }
        public int ikey4 { get; set; }
        public int ikey5 { get; set; }
        public double fkey1 { get; set; }
        public double fkey2 { get; set; }
        public double fkey3 { get; set; }
        public double fkey4 { get; set; }
        public double fkey5 { get; set; }
        public bool bkey1 { get; set; }
        public bool bkey2 { get; set; }
        public bool bkey3 { get; set; }
        public bool bkey4 { get; set; }
        public bool bkey5 { get; set; }
        public DateTime dkey1 { get; set; } = new DateTime(1753, 1, 1, 0, 0, 0);
        public DateTime dkey2 { get; set; } = new DateTime(1753, 1, 1, 0, 0, 0);
        public DateTime dkey3 { get; set; } = new DateTime(1753, 1, 1, 0, 0, 0);
        public DateTime dkey4 { get; set; } = new DateTime(1753, 1, 1, 0, 0, 0);
        public DateTime dkey5 { get; set; } = new DateTime(1753, 1, 1, 0, 0, 0);
        public string Notes { get; set; }
        public string ProcessBy { get; set; }

        public ICollection<string> Details;
    }


    public class cMenuList
    {
        public string CompanyId { get; set; }
        public string ObjectId { get; set; }
        public string ObjectName { get; set; }
        public string ObjectTitle { get; set; }
        public string ObjectType { get; set; }
        public string ObjectParent { get; set; }
        public string ObjectSequence { get; set; }
        public string ObjectLink { get; set; }

        public List<cMenuListDetail> cMenuListDetail { get; set; }
        public List<cMenuListDetail2> cMenuListDetail2 { get; set; }
        public List<cMenuListDetail> Child { get; set; }


        public string result { get; set; }
        public string message { get; set; }
        public string totalRows { get; set; }
    }
    public class cButtonList
    {
        public string CompanyId { get; set; }
        public string ObjectId { get; set; }
        public string ObjectName { get; set; }
        public string ObjectTitle { get; set; }
        public string ObjectType { get; set; }
        public string ObjectParent { get; set; }
        public string ObjectSequence { get; set; }
        public string ObjectLink { get; set; }
        public string ObjectImage { get; set; }
        public string LinkChild { get; set; }
        public bool ObjectShortcut { get; set; }
    }

    public class loginResult
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string DeptID { get; set; }
        public string DomainName { get; set; }
        public string Roles { get; set; }
        public string Result { get; set; }
        public string RolesList { get; set; }
    }

    public class cMenuListDetail
    {
        public string CompanyId { get; set; }
        public string ObjectId { get; set; }
        public string ObjectName { get; set; }
        public string ObjectTitle { get; set; }
        public string ObjectType { get; set; }
        public string ObjectParent { get; set; }
        public string ObjectSequence { get; set; }
        public string ObjectLink { get; set; }
        public string ObjectImage { get; set; }
        public string LinkChild { get; set; }
        public bool ObjectShortcut { get; set; }
        public List<cMenuListDetail> Child { get; set; }
    }

    public class cMenuListDetail2
    {
        public string CompanyId { get; set; }
        public string ObjectId { get; set; }
        public string ObjectName { get; set; }
        public string ObjectTitle { get; set; }
        public string ObjectType { get; set; }
        public string ObjectParent { get; set; }
        public string ObjectSequence { get; set; }
        public string ObjectLink { get; set; }
        public List<cMenuListDetail2> Child { get; set; }
    }

    public class DropDownViewModel
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }

    public class StatusListParam
    {
        public string CompanyId { get; set; }
        public string CodeType { get; set; }
    }

    public  class LoginAnotherApi
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}