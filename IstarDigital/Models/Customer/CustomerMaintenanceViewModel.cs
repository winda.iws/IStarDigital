﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Customer
{
    public class CustomerMaintenanceViewModel
    {
        public string UserID { get; set; }
        public HeaderCustomerMaintenanceViewModel HeaderCustomer { get; set; }
        public List<ContactAddress> ContactDetailList { get; set; }
        public List<ContactAddress> AddressList { get; set; }
    }

    public class HeaderCustomerMaintenanceViewModel { 
        public string CustomerID { get; set; }
        public string CustomerType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Nationality { get; set; }
        public string Occupation { get; set; }
        public string IDType { get; set; }
        public string IDNumber { get; set; }
        public string IDTypeName { get; set; }
        public DateTime DOB { get; set; }
    }

    public class ContactAddress
    {
        public string ID { get; set; }
        public string ContactTypeID { get; set; }
        public string ContactType { get; set; }
        public string ContactDetail { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
    }
}