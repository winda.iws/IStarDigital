﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Customer
{
    public class CustomerListViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string GenderID { get; set; }
        public string Gender { get; set; }
        public DateTime? DOB { get; set; }
        public string TypeID { get; set; }
        public string Type { get; set; }
        public int Length { get; set; }
    }
}