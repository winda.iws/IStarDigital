﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Customer
{
    public class ParameterCustomerListViewModel
    {
        public int Page { get; set; }
        public int PerPage { get; set; }
        public string Prop { get; set; }
        public string Sort { get; set; }
        public string CompanyID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerType { get; set; }
        public DateTime? DOB { get; set; }
        public string Gender { get; set; }
    }
}