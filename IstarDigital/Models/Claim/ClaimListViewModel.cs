﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Claim
{
    public class ClaimListViewModel
    {
        public string ID { get; set; }
        public string ClaimNo { get; set; }
        public string PolicyNo { get; set; }
        public string InsuredName { get; set; }
        public string ClaimType { get; set; }
        public DateTime ClaimDate { get; set; }
        public string Status { get; set; }
        public int Length { get; set; }
    }
}