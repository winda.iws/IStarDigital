﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Claim
{
    public class ParameterClaimListViewModel
    {
        public int Page { get; set; }
        public int PerPage { get; set; }
        public string Prop { get; set; }
        public string Sort { get; set; }
        public string CompanyID { get; set; }
        public string PolicyNo { get; set; }
        public string ClaimNo { get; set; }
        public string ClaimType { get; set; }
        public string InsuredName { get; set; }
        public string ClaimStatus { get; set; }
    }

    public class ParameterClaimStatusViewModel
    {
        public string ID { get; set; }
        public string userID { get; set; }
    }
}