﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models.Claim
{
    public class ClaimMaintenanceViewModel
    {
        public string UserID { get; set; }
        public HeaderClaimMaintenanceViewModel HeaderClaim { get; set; }
        public ClaimDetailViewModel ClaimDetail { get; set; }
        public AssesmentViewModel Assesment { get; set; }
        public List<ClaimBenefitViewModel> ClaimBenefitList { get; set; }
    }

    public class HeaderClaimMaintenanceViewModel
    {
        public string ID { get; set; }
        public string ClaimNo { get; set; }
        public string ApplicationNo { get; set; }
        public DateTime? InsuredDate { get; set; }
        public string ClaimStatus { get; set; }
        public string ClaimStatusName { get; set; }
        public DateTime? ClaimDate { get; set; }
        public string CreatedBy { get; set; }
        public string ClaimType { get; set; }
        public string ClaimTypeName { get; set; }
        public string ApprovedBy { get; set; }
        public string Notes { get; set; }
    }

    public class ClaimDetailViewModel
    {
        public string PolicyNo { get; set; }
        public string PolicyOwner { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string CertificateNo { get; set; }
        public DateTime MaturityDate { get; set; }
        public string InsuredName { get; set; }
        public DateTime InsuredDOB { get; set; }
        public string InsuredSex { get; set; }
        public string InsuredAge { get; set; }

        public string BankAccountName { get; set; }
        public string BankAccountNo { get; set; }
        public string BankName { get; set; }
        public string BeneficiaryAccountName { get; set; }
        public string BeneficiaryAccountNo { get; set; }
        public string BeneficiaryBankName { get; set; }

        public string PlanType { get; set; }
    }

    public class AssesmentViewModel
    {
        public string DiagnosisCode { get; set; }
        public string DiagnosisCodeName { get; set; }
        public string OtherDiagnosis { get; set; }
        public string Physician { get; set; }
        public string HospitalCode { get; set; }
        public string HospitalCodeName { get; set; }
        public string HospitalName { get; set; }
        public DateTime? HospitalizationStartDate { get; set; }
        public DateTime? HospitalizationEndDate { get; set; }
        public string HospitalChargeCurrency { get; set; }
        public string HospitalChargeCurrencyName { get; set; }
        public decimal HospitalCharge { get; set; }
        public DateTime? DateOfDeath { get; set; }
        public string PlaceOfDeath { get; set; }
        public string CauseOfDeath { get; set; }
        public string CauseOfDeathName { get; set; }
        public string OtherDeath { get; set; }

        public string ClaimCurrency { get; set; }
        public string ClaimCurrencyName { get; set; }
        public decimal? ClaimAmount { get; set; }
        public string PlanType { get; set; }
        public string PlanTypeName { get; set; }
    }

    public class ClaimActivityViewModel
    {
        public DateTime ActivityDate { get; set; }
        public string ActivityBy { get; set; }
        public string ActivityHistory { get; set; }
    }

    public class PreviousClaimViewModel
    {
        public string ID { get; set; }
        public string ClaimNo { get; set; }
        public DateTime ClaimDate { get; set; }
        public string ClaimantName { get; set; }
        public Decimal ClaimAmount { get; set; }
        public string ClaimStatus { get; set; }
    }

    public class ClaimBenefitViewModel
    {
        public string ID { get; set; }
        public string BenefitCode { get; set; }
        public string BenefitCodeName { get; set; }
        public decimal? ClaimAmount { get; set; }
        public int NoOfDays { get; set; }
        public decimal? TotalClaimAmount { get; set; }
        public string Notes { get; set; }
    }

    public class BenefitCodeViewModel
    {
        public string BenefitCode { get; set; }
        public string BenefitName { get; set; }
        public Decimal LimitAmount { get; set; }
        public int LimitDuration { get; set; }

    }

    public class BenefitRemainingViewModel
    {
        public decimal Amount { get; set; }
        public decimal Duration { get; set; }
        public string AutoCalculate { get; set; }
    }
}