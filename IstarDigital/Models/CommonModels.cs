﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models
{
    public class DefaultReturn
    {
        public string Result { get; set; }
        public string Message { get; set; }
        public string OtherValue { get; set; }

    }

    public class ApiDefaultReturn
    {
        public ApiDefaultReturn()
        {
        }

        public ApiDefaultReturn(bool isSuccess, object data, string message, int length)
        {
            IsSuccess = isSuccess;
            Data = data;
            Message = message;
            Length = length;
        }

        public bool IsSuccess { get; set; }
        public object Data { get; set; }
        public string Message { get; set; }
        public int Length { get; set; }
    }

    public class paramUserName
    {
        public string UserName { get; set; }

    }

    public class DefaultReturnRegister
    {
        public string Result { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string UserName { get; set; }

    }

    public class smsParam
    {
        public string serviceLink { get; set; }

        public string userID { get; set; }

        public string userPassword { get; set; }

        public string phoneNo { get; set; }

        public string SMSContent { get; set; }

        public string senderID { get; set; }

        public string divisionID { get; set; }

        public string batchname { get; set; }

        public string uploadBy { get; set; }

        public string channel { get; set; }

    }

    public class emailParam
    {
        public string fromMail { get; set; }

        public string sendemail { get; set; }

        public string subjectMsg { get; set; }

        public string bodyMsg { get; set; }

        public string SMTPServer { get; set; }

        public int PortServer { get; set; }

        public string SMTPId { get; set; }

        public string SMTPPassword { get; set; }

        public bool SMTPSSL { get; set; }

        public bool SMTPbodyHTML { get; set; }


    }

    public class MsgError
    {
        public string error { get; set; }
        public string error_description { get; set; }
    }

    public class DefaultReturnHTTP
    {
        public string Message { get; set; }
        public string Result { get; set; }

    }

    public class otpRequestParameter
    {
        public string userName { get; set; }
        public string policyID { get; set; }
        public string transType { get; set; }

    }

    public class otpRequest
    {
        public string result { get; set; }
        public string message { get; set; }
        public string userName { get; set; }
        public otpRequestDetail otpData { get; set; }

    }

    public class otpRequestDetail
    {
        public string policyID { get; set; }
        public string transType { get; set; }
        public string otpCode { get; set; }
        public string otpCount { get; set; }
        public string expired { get; set; }

    }

    public class otpValidateParameter
    {
        public string userName { get; set; }
        public string policyID { get; set; }
        public string transType { get; set; }
        public string otpCode { get; set; }

    }

    public class otpValidate
    {
        public string result { get; set; }
        public string message { get; set; }
        public string userName { get; set; }
        public otpValidateDetail otpData { get; set; }

    }

    public class otpValidateDetail
    {
        public string policyID { get; set; }
        public string transType { get; set; }
        public string otpCode { get; set; }
        public string otpStatus { get; set; }

    }
}