﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace IstarDigital.Helpers
{
    public class helperLib
    {
        #region "Public Enum"
        public enum InputType
        {
            Datetime,
            Numeric,
            Text
        }
        public enum TaskStatus
        {
            Handle,
            DirectAssign,
            Pending,
            ReturnTask,
            Reject,
            Complete,
            Expired
        }

        public class StatusPolicy
        {
            public static readonly string New = "N";
            public static readonly string NewValidate = "NV";
            public static readonly string WaitingApproval = "WA";
            public static readonly string Approve = "A";
            public static readonly string Cancel = "C";
        }

        public class BillingMode
        {
            public static readonly string Single = "S";
            public static readonly string Monthly = "M";
        }

        public class BillingStatus
        {
            public static readonly string Paid = "PD";
            public static readonly string OverPayment = "PO";
            public static readonly string PartialyPaid = "PP";
            public static readonly string Unpaid = "UP";
        }

        public class UnderwritingStatus
        {
            public static readonly string UnderwritingComplete = "CU";
            public static readonly string DecissionUnderwritting = "DU";
            public static readonly string InProgressUnderwritting = "IU";
            public static readonly string Cancelled = "LU";
            public static readonly string NotAvailable = "NA";
            public static readonly string PendingUnderwritting = "PU";
            public static readonly string Expired = "XU";
        }

        public class HistoryType
        {
            public static readonly string ApprovedCertificate = "AC";
            public static readonly string CancelCertificate = "CC";
            public static readonly string IssuedCertificate = "IC";
            public static readonly string ModifyCertificate = "MC";
            public static readonly string UploadCertificate = "UC";
            public static readonly string ValidateCertificate = "VC";
            public static readonly string AddRemarks = "AR";
        }

        public class CustomerType
        {
            public static readonly string Owner = "O";
            public static readonly string Insured = "I";
            public static readonly string Beneficiary = "B";
        }

        public class PaymentStatus
        {
            public static readonly string New = "N";
            public static readonly string Panding = "P";
            public static readonly string Approve = "A";
            public static readonly string Cancel = "C";
            public static readonly string Modify = "M";
        }
        #endregion

        #region "Constants"
        public static System.DateTime NullDateTime = System.DateTime.MinValue;
        public static decimal NullDecimal = decimal.MinValue;
        public static double NullSingle = float.MinValue;
        public static double NullDouble = double.MinValue;
        public static int NullInt = int.MinValue;
        public static long NullLong = long.MinValue;
        public static string NullString = string.Empty;
        public static System.DateTime SqlMaxDate = new System.DateTime(9999, 1, 3, 23, 59, 59);
        public static System.DateTime SqlMinDate = new System.DateTime(1753, 1, 1, 0, 0, 0);
        #endregion
        public static bool NullBool = false;

        #region "Functions"

        //public FileResults getFile(FileParam oParam)
        //{
        //    FileResults oResult = null;

        //    if (!string.IsNullOrEmpty(oParam.Path))
        //    {
        //        if (!File.Exists(oParam.Path))
        //        {
        //            oResult = new FileResults();
        //            oResult.Result = "Failure";
        //            oResult.Message = "Tidak ditemukan File/FilePath";
        //        }
        //        else
        //        {
        //            oResult = new FileResults();
        //            FileStream fs = new FileStream(oParam.Path,
        //                                                     FileMode.Open,
        //                                                     FileAccess.Read);
        //            byte[] filebytes = new byte[fs.Length];
        //            fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
        //            oResult.FileString =
        //                 Convert.ToBase64String(filebytes,
        //                                                Base64FormattingOptions.InsertLineBreaks);
        //            //oResult.FileString = filebytes;
        //            oResult.Result = "Success";
        //            oResult.Message = "";
        //        }

        //    }
        //    else
        //    {
        //        oResult = new FileResults();
        //        oResult.Result = "Failure";
        //        oResult.Message = "Tidak ditemukan file";

        //    }

        //    return oResult;

        //}

        public static bool writeLogBLL(string val1, string val2, string val3, string val4, string val5)
        {
            Logger logger = LogManager.GetLogger("rsExceptionBLL");
            LogEventInfo customLog = new LogEventInfo(LogLevel.Trace, "", "Log Exception BLL Data");

            customLog.Properties["Controller"] = val1;
            customLog.Properties["Type"] = val2;
            customLog.Properties["Detail_1"] = val3;
            customLog.Properties["Detail_2"] = val4;
            customLog.Properties["Status"] = "400";
            customLog.Properties["ErrorMessage"] = val5;

            logger.Log(customLog);


            return true;
        }

        public static bool writeLogMedian(string val1, string val2, string val3, string val4, string val5)
        {
            Logger logger = LogManager.GetLogger("rsMedian");
            LogEventInfo customLog = new LogEventInfo(LogLevel.Trace, "", "Log Median Data");

            customLog.Properties["Type"] = val1;
            customLog.Properties["Send"] = val2;
            customLog.Properties["Detail"] = val3;
            customLog.Properties["Status"] = val4;
            customLog.Properties["ErrorMessage"] = val5;

            logger.Log(customLog);


            return true;
        }

        public string getComString(string stype)
        {
            string m_file_name = null;
            StreamReader m_stream_reader = default(StreamReader);
            string getStr = "";
            string[] arrstring = null;
            bool bout = false;
            m_file_name = "comstring.ini";
            try
            {
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + m_file_name))
                {
                    m_stream_reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + m_file_name);
                    while (!bout)
                    {
                        getStr = m_stream_reader.ReadLine();
                        arrstring = getStr.Split('|');
                        if (arrstring[0].ToLower() == stype.ToLower())
                        {
                            getStr = arrstring[1];
                            bout = true;
                        }
                        else if (getStr == null)
                        {
                            bout = true;
                        }

                    }
                    m_stream_reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                m_stream_reader = null;
            }
            return getStr;
        }

        public static string CleanFileName(string sFileName)
        {
            return sFileName.Replace("_", "").Replace("/", "").Replace(":", "").Replace(" ", "").Replace("PM", "").Replace("AM", "");
        }
        public static bool CheckValidation(string TextInput, InputType TextType)
        {
            bool result = true;

            try
            {
                if (TextType == InputType.Datetime)
                {
                    DateTime outputDate;

                    //Check Date
                    if (!DateTime.TryParse(TextInput, out outputDate))
                    {
                        result = false;
                    }
                }
                else if (TextType == InputType.Numeric)
                {
                    double cResult = 0;

                    //Check Numeric
                    if (!Double.TryParse(TextInput, out cResult))
                    {
                        result = false;
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        //Get last day of the month
        public static System.DateTime LastDateofMonth(System.DateTime dateInput)
        {

            return dateInput.AddMonths(1).AddDays((dateInput.AddMonths(1).Day) * -1);

        }
        public static string CleanTextBox(string SourceStr)
        {

            string result = SourceStr.Replace("&nbsp;", "").Replace("&amp;", "");

            return result;
        }

        //Convert blank value to any return value
        public static object Nz(object value, object returnvalue)
        {
            return (IsTextBlank(value) ? returnvalue : value);
        }

        public static bool GetBool(object pObj)
        {
            if ((pObj == null))
            {
                return NullBool;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return NullBool;
            }
            else
            {
                return Convert.ToBoolean(pObj);
            }
        }
        public static double GetDouble(object pObj)
        {
            if ((pObj == null))
            {
                return 0;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(pObj);
            }
        }
        public static float GetSingle(object pObj)
        {
            if ((pObj == null))
            {
                return 0;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return 0;
            }
            else
            {
                return float.Parse(pObj.ToString());
            }
        }
        public static int GetInteger(object pObj)
        {
            if ((pObj == null))
            {
                return 0;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return 0;
            }
            else if (string.IsNullOrEmpty(pObj.ToString()))
            {
                return 0;
            }
            else
            {
                return int.Parse(pObj.ToString());
            }
        }
        public static string GetText(object pObj)
        {
            if ((pObj == null))
            {
                return NullString;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return NullString;
            }
            else if (pObj.ToString() == "&nbsp;")
            {
                return "";
            }
            else
            {
                return pObj.ToString();
            }
        }

        public static DateTime GetDate(object pObj)
        {
            if ((pObj == null))
            {
                return DateTime.MinValue;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return DateTime.MinValue;
            }
            else if (pObj.ToString() == "&nbsp;")
            {
                return DateTime.MinValue;
            }
            else
            {
                return Convert.ToDateTime(pObj);
            }
        }

        public static DateTime GetDateFormat(object pObj,string format)
        {
            if ((pObj == null))
            {
                return DateTime.MinValue;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return DateTime.MinValue;
            }
            else if (pObj.ToString() == "&nbsp;")
            {
                return DateTime.MinValue;
            }
            else
            {
                DateTime date;
                DateTime.TryParseExact(pObj.ToString(), format, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out date);
                return date;
            }
        }

        public static DateTime GetDateFormatClaim(object pObj, string format)
        {
            if ((pObj == null))
            {
                return Convert.ToDateTime("1/1/1753");
            }
            else if (Convert.IsDBNull(pObj))
            {
                return Convert.ToDateTime("1/1/1753");
            }
            else if (pObj.ToString() == "&nbsp;")
            {
                return Convert.ToDateTime("1/1/1753");
            }
            else if (pObj.ToString() == "")
            {
                return Convert.ToDateTime("1/1/1753");
            }
            else
            {
                DateTime date;
                DateTime.TryParseExact(pObj.ToString(), format, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out date);
                return date;
            }
        }

        public static System.DateTime CToDate(object pObj, bool pShort = false)
        {
            if ((pObj == null))
            {
                return SqlMinDate;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return SqlMinDate;
            }
            else if (IsTextBlank(pObj))
            {
                return SqlMinDate;
            }
            else
            {
                if (pShort)
                {
                    return Convert.ToDateTime(Convert.ToDateTime(pObj).ToShortDateString());
                }
                else
                {
                    return Convert.ToDateTime(pObj);
                }
            }
        }
        public static object MinDateToNull(object pobj)
        {
            if (DateTime.ParseExact(pobj.ToString(), "ddd MMM dd yyyy HH:mm:ss", CultureInfo.InvariantCulture) == DateTime.MinValue)//pobj == SqlMinDate
            {
                return DBNull.Value;
            }
            else if (pobj.ToString() == "&nbsp;")
            {
                return DBNull.Value;
            }
            else
            {
                return pobj;
            }
        }
        public static object BlankDateToNull(System.DateTime psource)
        {
            if (psource == DateTime.MinValue)
            {
                return null;
            }
            else
            {
                return psource;
            }
        }

        public static object BlankDateToMinValue(System.DateTime psource)
        {
            if (psource == DateTime.MinValue)
            {
                return SqlMinDate;
            }
            else
            {
                return psource;
            }
        }

        public static bool IsTextBlank(object pobj)
        {
            if ((pobj == null))
            {
                return true;
            }
            else
            {
                return Convert.IsDBNull(pobj) | string.IsNullOrEmpty(pobj.ToString());
            }
        }
        public static bool IsNumericBlank(object pobj)
        {
            if (IsTextBlank(pobj))
            {
                return true;
            }
            else if (Convert.ToInt16(pobj) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static string CBlank(string pobj)
        {
            if (IsTextBlank(pobj))
            {
                return "";
            }
            else
            {
                return pobj;
            }
        }

        public static double CNumeric(string pobj)
        {
            if (IsTextBlank(pobj))
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(pobj);
            }
        }

        public static string cToString64(byte[] pObj)
        {

            if ((pObj == null))
            {
                return NullString;
            }
            else if (Convert.IsDBNull(pObj))
            {
                return NullString;
            }
            else
            {
                return Convert.ToBase64String(pObj);
            }

        }

        public static byte[] Cbytefrom64(string pobj)
        {
            if (IsTextBlank(pobj))
            {
                return null;
            }
            else
            {
                return Convert.FromBase64String(pobj);
            }
        }

        public static byte[] Getbyte(object pobj)
        {
            if (IsTextBlank(pobj))
            {
                return null;
            }
            else
            {
                return (byte[])pobj;
            }
        }

        public static System.DateTime SystemDate()
        {
            try
            {
                return System.DateTime.Today;
            }
            catch (Exception ex)
            {
                return SqlMinDate;
            }
        }

        public static SqlDbType GetSQLDataType(string pdatatype)
        {
            try
            {
                switch (pdatatype)
                {
                    case "I":
                        return SqlDbType.Int;
                    case "S":
                        return SqlDbType.VarChar;
                    case "D":
                        return SqlDbType.DateTime;
                    default:
                        return SqlDbType.VarChar;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static object GetSystemDataType(string pdatatype)
        {
            try
            {
                switch (pdatatype)
                {
                    case "I":
                        return typeof(int);
                    case "S":
                        return typeof(string);
                    case "D":
                        return typeof(System.DateTime);
                    default:
                        return typeof(string);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static object CastValue(string ptype, string pvalue)
        {
            try
            {
                switch (ptype)
                {
                    case "I":
                        return Convert.ToInt32(pvalue);
                    case "S":
                        return pvalue;
                    case "D":
                        return Convert.ToDateTime(pvalue);
                    default:
                        return pvalue;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static DateTime GetFirstDayOfMonth(DateTime dtDate)
        {
            DateTime dtFrom = dtDate;
            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));
            return dtFrom;
        }

        public static DateTime GetLastDayOfMonth(DateTime dtDate)
        {
            DateTime dtTo = new DateTime(dtDate.Year, dtDate.Month, 1);
            dtTo = dtTo.AddMonths(1);
            dtTo = dtTo.AddDays(-(dtTo.Day));
            return dtTo;
        }

        public static string CheckInputValidation(string inputName, string TextInput, InputType TextType, bool AllowNull = true)
        {
            string result = "";
            if (TextType == InputType.Datetime)
            {
                DateTime outputDate;

                //Check Date
                if (!DateTime.TryParse(TextInput, out outputDate))
                {
                    result = "*Please enter a valid Date value for " + inputName;
                }
            }
            else if (TextType == InputType.Numeric)
            {

                double cResult = 0;

                //Check Numeric
                if (!Double.TryParse(TextInput, out cResult))
                {
                    result = "*Please enter a valid numeric value for " + inputName;
                }
            }
            else
            {
            }
            return result;
        }

        //public static cAnyParams ConvertStringToAnyParams(string pString)
        //{
        //    string[] arrfield = null;
        //    cAnyParams oany = new cAnyParams();
        //    try
        //    {
        //        arrfield = Strings.Split(pString, "|");
        //        for (i = 0; i <= Information.UBound(arrfield); i += 2)
        //        {
        //            if (arrfield[i] == "CompanyID")
        //            {
        //                oany.CompanyID = helperLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "PageNumber")
        //            {
        //                oany.PageNumber = helperLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "PageSize")
        //            {
        //                oany.PageSize = helperLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey1")
        //            {
        //                oany.skey1 = helperLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey2")
        //            {
        //                oany.skey2 = helperLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey3")
        //            {
        //                oany.skey3 = helperLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey4")
        //            {
        //                oany.skey4 = helperLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey5")
        //            {
        //                oany.skey5 = helperLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey6")
        //            {
        //                oany.skey6 = helperLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey7")
        //            {
        //                oany.skey7 = helperLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey8")
        //            {
        //                oany.skey8 = helperLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey9")
        //            {
        //                oany.skey9 = helperLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "skey10")
        //            {
        //                oany.skey10 = helperLib.GetText(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "ikey1")
        //            {
        //                oany.ikey1 = helperLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "ikey2")
        //            {
        //                oany.ikey2 = helperLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "ikey3")
        //            {
        //                oany.ikey3 = helperLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "ikey4")
        //            {
        //                oany.ikey4 = helperLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "ikey5")
        //            {
        //                oany.ikey5 = helperLib.GetInteger(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "fkey1")
        //            {
        //                oany.fkey1 = helperLib.GetDouble(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "fkey2")
        //            {
        //                oany.fkey2 = helperLib.GetDouble(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "fkey3")
        //            {
        //                oany.fkey3 = helperLib.GetDouble(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "fkey4")
        //            {
        //                oany.fkey4 = helperLib.GetDouble(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "fkey5")
        //            {
        //                oany.fkey5 = helperLib.GetDouble(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "bkey1")
        //            {
        //                oany.bkey1 = helperLib.GetBool(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "bkey2")
        //            {
        //                oany.bkey2 = helperLib.GetBool(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "bkey3")
        //            {
        //                oany.bkey3 = helperLib.GetBool(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "bkey4")
        //            {
        //                oany.bkey4 = helperLib.GetBool(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "bkey5")
        //            {
        //                oany.bkey5 = helperLib.GetBool(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "dkey1")
        //            {
        //                oany.dkey1 = helperLib.CToDate(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "dkey2")
        //            {
        //                oany.dkey2 = helperLib.CToDate(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "dkey3")
        //            {
        //                oany.dkey3 = helperLib.CToDate(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "dkey4")
        //            {
        //                oany.dkey4 = helperLib.CToDate(arrfield[i + 1]);
        //            }
        //            else if (arrfield[i] == "dkey5")
        //            {
        //                oany.dkey5 = helperLib.CToDate(arrfield[i + 1]);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return oany;
        //}

        public static bool IsTextBlankV(object pobj)
        {
            if ((pobj == null))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //#Region "JSON Conversion"
        //    Public Shared Function DataTableToJSON(dt As DataTable) As String
        //        Dim serializer As New JavaScriptSerializer()
        //        Dim rows As New List(Of Dictionary(Of String, Object))()
        //        Dim row As Dictionary(Of String, Object) = Nothing

        //        For Each dr As DataRow In dt.Rows
        //            row = New Dictionary(Of String, Object)()
        //            For Each col As DataColumn In dt.Columns
        //                row.Add(col.ColumnName, dr(col))
        //            Next
        //            rows.Add(row)
        //        Next
        //        Return serializer.Serialize(rows)
        //    End Function
        //    Public Shared Function DataSetToJSON(ds As DataSet) As String
        //        Dim dict As New Dictionary(Of String, Object)()

        //        For Each dt As DataTable In ds.Tables
        //            Dim arr As Object() = New Object(dt.Rows.Count) {}

        //            For i As Integer = 0 To dt.Rows.Count - 1
        //                arr(i) = dt.Rows(i).ItemArray
        //            Next

        //            dict.Add(dt.TableName, arr)
        //        Next

        //        Dim json As New JavaScriptSerializer()
        //        Return json.Serialize(dict)
        //    End Function
        //#End Region
        //#endregion

        #endregion

        #region "PDF Utility"
        public static string CreatePDFReferral(string clientId, List<IstarDigital.Models.MyReferral> myReferrals)
        {

            string folderPath = @"c:\pdf-folder\";
            if(!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            string fileName = String.Format("MyReferrals-{0}-{1}.pdf", clientId, DateTime.Now.ToString("ddmmyyyyhhmmss"));

            using (var stream = new MemoryStream())
            {
                Document pdfDoc = new Document(PageSize.A4);
                FileStream fs = new FileStream(String.Format("{0}{1}", folderPath, fileName), FileMode.Create, FileAccess.Write, FileShare.None);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, fs);

                pdfDoc.Open();
                pdfDoc.AddAuthor("Sun Life Insurance");
                pdfDoc.AddTitle("List of Referral");
                pdfDoc.AddSubject(String.Format("List of Referral for client ID {0}", clientId));

                Paragraph paragraph = new Paragraph();
                paragraph.Add(new Chunk("Daftar Referensi Saya", FontFactory.GetFont(FontFactory.HELVETICA_BOLD)));
                pdfDoc.Add(paragraph);

                paragraph = new Paragraph();
                paragraph.Add(new Phrase("\n"));
                pdfDoc.Add(paragraph);

                PdfPTable table = setupTableHeader();


                BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                Font font = new Font(baseFont, 8, Font.NORMAL);

                int recordNumber = 1;
                int i = 1;
                foreach (IstarDigital.Models.MyReferral myReferral in myReferrals)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(recordNumber.ToString(), font));
                    ApplyPrintSettingsToCell(cell);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase((!String.IsNullOrEmpty(myReferral.referralDate))? myReferral.referralDate : " ", font));
                    ApplyPrintSettingsToCell(cell);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase((!String.IsNullOrEmpty(myReferral.referralName)) ? myReferral.referralName : " ", font));
                    ApplyPrintSettingsToCell(cell);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase((!String.IsNullOrEmpty(myReferral.referralPhone)) ? myReferral.referralPhone : " ", font));
                    ApplyPrintSettingsToCell(cell);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase((!String.IsNullOrEmpty(myReferral.referralCity)) ? myReferral.referralCity : " ", font));
                    ApplyPrintSettingsToCell(cell);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase((!String.IsNullOrEmpty(myReferral.recommendedAgent)) ? myReferral.recommendedAgent : " ", font));
                    ApplyPrintSettingsToCell(cell);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase((!String.IsNullOrEmpty(myReferral.agentPhone)) ? myReferral.agentPhone : " ", font));
                    ApplyPrintSettingsToCell(cell);
                    table.AddCell(cell);

                    recordNumber++;

                    if ((i / 20) == 0)
                    {
                        i = 1;
                    }
                    else
                    {
                        i++;
                    }
                        
                }

                pdfDoc.Add(table);


                pdfDoc.Close();
            }

            return String.Format("{0}{1}", folderPath, fileName);

        }

        private static PdfPTable setupTableHeader()
        {
            BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font font = new Font(baseFont, 9, Font.NORMAL);

            PdfPTable table = new PdfPTable(7);
            table.SetWidths(new[] { 10, 35, 30, 25, 25, 35, 35 });
            table.WidthPercentage = 100.0f;

            PdfPCell cell = new PdfPCell(new Phrase("No", font));
            ApplyPrintSettingsToCellHeader(cell);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Tanggal Pemberian Referensi", font));
            ApplyPrintSettingsToCellHeader(cell);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nama Referensi", font));
            ApplyPrintSettingsToCellHeader(cell);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nomor Ponsel", font));
            ApplyPrintSettingsToCellHeader(cell);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Kota Domisili", font));
            ApplyPrintSettingsToCellHeader(cell);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Rekomendasi Nama Agen", font));
            ApplyPrintSettingsToCellHeader(cell);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nomor Ponsel Agen", font));
            ApplyPrintSettingsToCellHeader(cell);
            table.AddCell(cell);

            return table;
        }

        private static void ApplyPrintSettingsToCellHeader(PdfPCell pCell)
        {
            pCell.PaddingLeft = 5.0f;
            pCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            pCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
            pCell.UseAscender = true; //Setting [UseAscender] property to "true" along with setting [VerticalAlignment] to "PdfPCell.ALIGN_MIDDLE" helps to align the text in the cell at vertically middle position.
        }
        private static void ApplyPrintSettingsToCell(PdfPCell pCell)
        {
            pCell.PaddingLeft = 5.0f;
            pCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            pCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
            pCell.UseAscender = true; //Setting [UseAscender] property to "true" along with setting [VerticalAlignment] to "PdfPCell.ALIGN_MIDDLE" helps to align the text in the cell at vertically middle position.
        }
        #endregion

    }
}