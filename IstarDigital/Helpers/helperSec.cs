﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace IstarDigital.Helpers
{
    public class helperSec
    {
        public static string GetHash(string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();

            byte[] byteValue = System.Text.Encoding.UTF8.GetBytes(input);

            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);

            return Convert.ToBase64String(byteHash);
        }

        public string GetMD5(string input)
        {

            string strPlainText = input.ToString();
            string result = null;


            try
            {
                //Create an instance of the MD5CryptoServiceProvider class
                MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

                //The array of bytes that will contain the encrypted value of strPlainText
                byte[] hashedDataBytes = md5Hasher.ComputeHash(Encoding.Default.GetBytes(strPlainText));

                //The encoder class used to convert strPlainText to an array of bytes

                //Call ComputeHash, passing in the plain-text string as an array of bytes
                //The return value is the encrypted value, as an array of bytes

                StringBuilder strPass = new StringBuilder();

                for (int i = 0; i <= hashedDataBytes.Length - 1; i++)
                {
                    strPass.AppendFormat("{0:x2}", hashedDataBytes[i]);
                }

                result = strPass.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }
    }
}