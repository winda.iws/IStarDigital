﻿using IstarDigital.Entities;
using IstarDigital.Helpers;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using IstarDigital.Models;
using System.Net;
using IstarDigital.BLL;
using Newtonsoft.Json.Linq;
using NLog;

namespace IstarDigital.Providers
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {

            string clientId = string.Empty;
            string clientSecret = string.Empty;
            Client client = null;

            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            if (context.ClientId == null)
            {
                //Remove the comments from the below line context.SetError, and invalidate context 
                //if you want to force sending clientId/secrects once obtain access tokens. 
                context.Validated();
                //context.SetError("invalid_clientId", "ClientId should be sent.");
                return Task.FromResult<object>(null);
            }

            if (client == null)
            {
                context.SetError("invalid_clientId", string.Format("Client '{0}' is not registered in the system.", context.ClientId));
                return Task.FromResult<object>(null);
            }

            if (client.ApplicationType == Models.ApplicationTypes.NativeConfidential)
            {
                if (string.IsNullOrWhiteSpace(clientSecret))
                {
                    context.SetError("invalid_clientId", "Client secret should be sent.");
                    return Task.FromResult<object>(null);
                }
                else
                {
                    if (client.Secret != helperSec.GetHash(clientSecret))
                    {
                        context.SetError("invalid_clientId", "Client secret is invalid.");
                        return Task.FromResult<object>(null);
                    }
                }
            }

            if (!client.Active)
            {
                context.SetError("invalid_clientId", "Client is inactive.");
                return Task.FromResult<object>(null);
            }

            context.OwinContext.Set<string>("as:clientAllowedOrigin", client.AllowedOrigin);
            context.OwinContext.Set<string>("as:clientRefreshTokenLifeTime", client.RefreshTokenLifeTime.ToString());

            context.Validated();
            return Task.FromResult<object>(null);
        }

        //public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        //{

        //    var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
        //    UserLogin oparamLog = null;
        //    IstarDigitalbll objLogin = new IstarDigitalbll();

        //    Logger logger = LogManager.GetLogger("rsLogin");
        //    LogEventInfo customLog = new LogEventInfo(LogLevel.Trace, "", "Log Login Data");

        //    if (allowedOrigin == null) allowedOrigin = "*";

        //    context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

        //    var identity = new ClaimsIdentity(context.Options.AuthenticationType);

        //    oparamLog = objLogin.GetLoginValue(context.UserName, context.Password);

        //    customLog.Properties["Request"] = "Login";
        //    customLog.Properties["URL"] = context.Request.Uri.ToString();
        //    customLog.Properties["IPAddress"] = context.Request.RemoteIpAddress.ToString();
        //    customLog.Properties["UserName"] = context.UserName;

        //    if (oparamLog != null)
        //    {
        //        if(oparamLog.CodeStatus == "79")
        //        {
        //            context.SetError("Otorisasi", oparamLog.CodeMessage);

        //            context.Response.Headers.Add(ServerGlobalModels.OwinFlag,
        //            new[] { ((int)HttpStatusCode.Unauthorized).ToString() });

        //            customLog.Properties["Status"] = oparamLog.CodeStatus;
        //            customLog.Properties["Message"] = oparamLog.CodeMessage;
        //            logger.Log(customLog);

        //            return;
        //        }

        //        if (oparamLog.UserStatus == "09")
        //        {
        //            context.SetError("Otorisasi", "Nama Pengguna Tidak Aktif");

        //            context.Response.Headers.Add(ServerGlobalModels.OwinFlag,
        //            new[] { ((int)HttpStatusCode.Unauthorized).ToString() });

        //            customLog.Properties["Status"] = oparamLog.UserStatus;
        //            customLog.Properties["Message"] = "Nama Pengguna Tidak Aktif";
        //            logger.Log(customLog);

        //            return;
        //        }
        //        else
        //        {
        //            identity.AddClaim(new Claim(ClaimTypes.Name, oparamLog.UserID));
        //            identity.AddClaim(new Claim(ClaimTypes.Role, oparamLog.userType));
        //            identity.AddClaim(new Claim(ClaimTypes.Role, "CLIENT_PRTL_USER"));
        //            identity.AddClaim(new Claim("LastKeyUpdate", oparamLog.KeyUpdate));

        //            customLog.Properties["Status"] = oparamLog.CodeStatus;
        //            customLog.Properties["Message"] = oparamLog.CodeMessage;
        //            logger.Log(customLog);
        //        }
        //    }
        //    else
        //    {

        //        context.SetError("Otorisasi", "Informasi login Anda tidak sesuai.");

        //        context.Response.Headers.Add(ServerGlobalModels.OwinFlag,
        //        new[] { ((int)HttpStatusCode.Unauthorized).ToString() });

        //        customLog.Properties["Status"] = "99";
        //        customLog.Properties["Message"] = "Informasi login Anda tidak sesuai";
        //        logger.Log(customLog);

        //        return;
        //    }


        //    var props = new AuthenticationProperties(new Dictionary<string, string>
        //        {
        //            {
        //                "codeStatus", oparamLog.UserStatus
        //            },
        //            {
        //                "message", ""
        //            },
        //            {
        //                "result", "SUCCESS"
        //            },
        //            {
        //                "userName", context.UserName
        //            }
        //        });

        //    var ticket = new AuthenticationTicket(identity, props);
        //    context.Validated(ticket);

        //}

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            //    var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            //UserLogin oparamLog = null;
            IstarDigitalbll objLogin = new IstarDigitalbll();

            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

            Logger logger = LogManager.GetLogger("rsLogin");
            LogEventInfo customLog = new LogEventInfo(LogLevel.Trace, "", "Log Login Data");


            if (allowedOrigin == null) allowedOrigin = "*";

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            loginResult oResult = null;
            oResult = new loginResult();

            oResult = objLogin.IsValidAD("CP", context.UserName, context.Password);

            if (oResult.Result == "")
            {

                identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));

            }
            else
            {

                context.SetError("Otorisasi", "Informasi login Anda tidak sesuai.");

                context.Response.Headers.Add(ServerGlobalModels.OwinFlag,
                new[] { ((int)HttpStatusCode.Unauthorized).ToString() });

                return;
            }


            var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {
                        "message", ""
                    },
                    {
                        "result", "SUCCESS"
                    },
                    {
                        "userID", oResult.UserID
                    },
                    {
                        "userName", oResult.UserName
                    },
                    {
                        "deptID", oResult.DeptID
                    },
                    {
                        "roles", oResult.Roles
                    }
                ,
                    {
                        "rolesList", oResult.RolesList
                    }
                });

            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);

        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

    }
}