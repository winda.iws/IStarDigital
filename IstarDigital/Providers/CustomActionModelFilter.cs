﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Linq;
using IstarDigital.Models;
using IstarDigital.Helpers;
using System.Web.Http.Tracing;
using System.Web.Http;
using System;
using NLog;
using System.Web;
using Microsoft.Owin;

namespace IstarDigital.Providers
{
    public class CustomActionModelFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var modelState = actionContext.ModelState;

            Logger logger = LogManager.GetLogger("rsRequest");
            LogEventInfo customLog = new LogEventInfo(LogLevel.Trace, "", "Log Request Data");
            var context = actionContext.RequestContext;

            customLog.Properties["Request"] = actionContext.ActionDescriptor.ActionName;
            customLog.Properties["URL"] = actionContext.Request.RequestUri.ToString();
            customLog.Properties["IPAddress"] = GetClientIpAddress(actionContext.Request);
            customLog.Properties["UserName"] = context.Principal.Identity.IsAuthenticated ? context.Principal.Identity.Name : string.Empty;

            if (!modelState.IsValid)
            {

                customLog.Properties["Status"] = "500";
                customLog.Properties["Message"] = "Model Not Valid";

                var response = actionContext.Request.CreateResponse<MsgError>
                                   (new MsgError() { error = "Error", error_description = "Input Parameter tidak valid atau ada kesalahan System" });
                response.StatusCode = HttpStatusCode.BadRequest;

                actionContext.Response = response;

                //actionContext.Response = actionContext.Request
                //     .CreateErrorResponse(HttpStatusCode.BadRequest, modelState);
            }
            else
            {
                customLog.Properties["Status"] = "200";
                customLog.Properties["Message"] = "SUCCESS";
            }

            logger.Log(customLog);

            GlobalConfiguration.Configuration.Services.Replace(typeof(ITraceWriter), new NLogger());
            var trace = GlobalConfiguration.Configuration.Services.GetTraceWriter();
            trace.Info(actionContext.Request, "Controller : " + actionContext.ControllerContext.ControllerDescriptor.ControllerType.FullName + Environment.NewLine + "Action : " + actionContext.ActionDescriptor.ActionName, "JSON", actionContext.ActionArguments.ToJSONv2s());

            //if (actionContext.ActionArguments.Any(v => v.Value == null))
            //{
            //    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest);
            //}
        }

        private string GetClientIpAddress(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return IPAddress.Parse(((HttpContextBase)request.Properties["MS_HttpContext"]).Request.UserHostAddress).ToString();
            }
            if (request.Properties.ContainsKey("MS_OwinContext"))
            {
                return IPAddress.Parse(((OwinContext)request.Properties["MS_OwinContext"]).Request.RemoteIpAddress).ToString();
            }
            return String.Empty;
        }

    }
}