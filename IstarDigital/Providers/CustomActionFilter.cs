﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using IstarDigital.Models;
using System.Security.Claims;
using IstarDigital.BLL;

namespace IstarDigital.Providers
{
    public class CustomActionFilter : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            var response = actionContext.Request.CreateResponse<MsgError>
                                    (new MsgError() { error = "Otorisasi", error_description = "Otorisasi telah ditolak untuk permintaan ini" });
            response.StatusCode = HttpStatusCode.Unauthorized;

            actionContext.Response = response;
        }

        
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            //if (actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
            //   || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any())

            if (ClaimsPrincipal.Current.Claims.ToList().Count != 0)
            {
                //IstarDigitalbll objAccountBLL = new IstarDigitalbll();

                //ClaimsPrincipal principal = actionContext.RequestContext.Principal as ClaimsPrincipal;

                //if(principal.Claims.SingleOrDefault(c => c.Type == "LastKeyUpdate") == null)
                //{
                //    var response = actionContext.Request.CreateResponse<MsgError>
                //                               (new MsgError() { error = "Otorisasi", error_description = "Otorisasi telah ditolak untuk permintaan ini" });
                //    response.StatusCode = HttpStatusCode.Unauthorized;

                //    actionContext.Response = response;
                //}
                //else
                //{

                //    var valueLastKey = principal.Claims.Where(c => c.Type == "LastKeyUpdate").SingleOrDefault().Value;
                //    string UserID = ClaimsPrincipal.Current.Identity.Name.ToUpper();

                //    if (objAccountBLL.GetLastKeyPwd(UserID) != valueLastKey)
                //    {
                //        var response = actionContext.Request.CreateResponse<MsgError>
                //                                   (new MsgError() { error = "Otorisasi", error_description = "Otorisasi telah ditolak untuk permintaan ini" });
                //        response.StatusCode = HttpStatusCode.Unauthorized;

                //        actionContext.Response = response;
                //    }

                //}

                
            }

        }

        
    }
}