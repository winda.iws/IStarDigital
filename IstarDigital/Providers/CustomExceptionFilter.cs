﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Linq;
using IstarDigital.Models;
using System.Web.Http.Tracing;
using IstarDigital.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Web.Http;
using System;
using NLog;
using System.Web;
using Microsoft.Owin;

namespace IstarDigital.Providers
{
    public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            Logger logger = LogManager.GetLogger("rsException");
            LogEventInfo customLog = new LogEventInfo(LogLevel.Trace, "", "Log Exception Data");

            customLog.Properties["Request"] = context.ActionContext.ActionDescriptor.ActionName;
            customLog.Properties["URL"] = context.Request.RequestUri.ToString();
            customLog.Properties["IPAddress"] = GetClientIpAddress(context.Request);
            customLog.Properties["UserName"] = HttpContext.Current.User.Identity.Name;
            customLog.Properties["Status"] = "500";
            
            var response = context.Request.CreateResponse<MsgError>
                                   (new MsgError() { error = "Error", error_description = "Input Parameter tidak valid atau ada kesalahan System" });
            response.StatusCode = HttpStatusCode.InternalServerError;

            context.Response = response;

            GlobalConfiguration.Configuration.Services.Replace(typeof(ITraceWriter), new NLogger());
            var trace = GlobalConfiguration.Configuration.Services.GetTraceWriter();
            trace.Error(context.Request, "Controller : " + context.ActionContext.ControllerContext.ControllerDescriptor.ControllerType.FullName + Environment.NewLine + "Action : " + context.ActionContext.ActionDescriptor.ActionName, context.Exception);

            var exceptionType = context.Exception.GetType();

            if (exceptionType == typeof(ValidationException))
            {

                customLog.Properties["Message"] = context.Exception.Message;
                logger.Log(customLog);

                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(context.Exception.Message), ReasonPhrase = "ValidationException", };
                throw new HttpResponseException(resp);

            }
            else if (exceptionType == typeof(UnauthorizedAccessException))
            {
                customLog.Properties["Message"] = "UnAuthorized";
                logger.Log(customLog);

                throw new HttpResponseException(context.Request.CreateResponse(HttpStatusCode.Unauthorized, new ServiceStatus() { StatusCode = (int)HttpStatusCode.Unauthorized, StatusMessage = "UnAuthorized", ReasonPhrase = "UnAuthorized Access" }));
            }
            else if (exceptionType == typeof(ApiException))
            {
                var webapiException = context.Exception as ApiException;

                customLog.Properties["Message"] = webapiException.ErrorDescription;
                logger.Log(customLog);

                if (webapiException != null)
                    throw new HttpResponseException(context.Request.CreateResponse(webapiException.HttpStatus, new ServiceStatus() { StatusCode = webapiException.ErrorCode, StatusMessage = webapiException.ErrorDescription, ReasonPhrase = webapiException.ReasonPhrase }));
            }
            else if (exceptionType == typeof(ApiBusinessException))
            {
                var businessException = context.Exception as ApiBusinessException;

                customLog.Properties["Message"] = businessException.ErrorDescription;
                logger.Log(customLog);

                if (businessException != null)
                    throw new HttpResponseException(context.Request.CreateResponse(businessException.HttpStatus, new ServiceStatus() { StatusCode = businessException.ErrorCode, StatusMessage = businessException.ErrorDescription, ReasonPhrase = businessException.ReasonPhrase }));
            }
            else if (exceptionType == typeof(ApiDataException))
            {
                var dataException = context.Exception as ApiDataException;

                customLog.Properties["Message"] = dataException.ErrorDescription;
                logger.Log(customLog);

                if (dataException != null)
                    throw new HttpResponseException(context.Request.CreateResponse(dataException.HttpStatus, new ServiceStatus() { StatusCode = dataException.ErrorCode, StatusMessage = dataException.ErrorDescription, ReasonPhrase = dataException.ReasonPhrase }));
            }
            else
            {
                customLog.Properties["Message"] = context.Exception.Message;
                logger.Log(customLog);

                throw new HttpResponseException(context.Request.CreateResponse(HttpStatusCode.InternalServerError));
            }

        }

        private string GetClientIpAddress(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return IPAddress.Parse(((HttpContextBase)request.Properties["MS_HttpContext"]).Request.UserHostAddress).ToString();
            }
            if (request.Properties.ContainsKey("MS_OwinContext"))
            {
                return IPAddress.Parse(((OwinContext)request.Properties["MS_OwinContext"]).Request.RemoteIpAddress).ToString();
            }
            return String.Empty;
        }

    }
}